﻿namespace TUMaster.Core.Constants
{
	public static class RequestMessages
	{
		public const string Achievement = "Complete achievement";
		public const string Salvage = "Salvage card";
		public const string FightRaid = "Fight raid battle";
		public const string KillRaidLevel = "Kill raid level";
		public const string DealRaidDamage = "Deal raid damage";

		public const string GetRaidInfoMessage = "getRaidInfo";
		public const string GetBattleResult = "getBattleResults";
		public const string BuyEnergyRefill = "buyBattleEnergyRefillTokens";
		public const string GetProfileData = "getUserAccount";
		public const string BuyStaminaRefill = "buyStaminaRefillTokens";
		public const string GetMissionCompletions = "getMissionCompletions";
		public const string UseDailyMessageBonus = "useDailyBonus";

		public const string ClaimConquestRewardsMessage = "claimConquestReward";
		public const string ClaimBrawlRewardMessage = "claimBrawlRewards";
		public const string ClaimWarRewardMessage = "claimFactionWarRewards";
		public const string ClaimRaidRewardMessage = "claimRaidReward&raid_id={0}";
		public const string BuyPromoTokenMessageFormat = "buyStorePromoTokens&item_id={0}&item_type={1}&expected_cost={2}";
		public const string BuyPromoGoldMessageFormat = "buyStorePromoGold&item_id={0}&item_type={1}&expected_cost={2}";
		public const string BuyPackTokensMessageFormat = "buyPackTokens&pack_id={0}";
		public const string BuyPackGoldMessageFormat = "buyPackGold&pack_id={0}";
		public const string AchievementMessageFormat = "completeAchievement&achievement_id={0}";
		public const string SalvageMessageFormat = "salvageCard&card_id={0}";
		public const string StartRaidBattleMessageFormat = "fightRaidBattle&raid_id={0}&raid_level={1}";
		public const string PlayCardMessageFormat = "playCard&card_uid={0}";
		public const string PlayCardSkipMessage = "playCard&skip=true";
		public const string GetInventoryMessage = "getUserCards";
		public const string GetBattlesMessage = "startHuntingBattle";
		public const string GetBrawlsMessage = "getHuntingTargets";
		public const string GetStoreInfoMessage = "getStoreData";
		public const string GetUserDeckMessage = "getUserDecks";
		public const string SetUserDeckMessageFormat = "setDeckCards&cards={{{0}}}&commander_id={1}";
		public const string UpdateFactionWarMessage = "updateFactionWar";
		public const string UpdateFactionMessage = "updateFaction";
		public const string StartHuntingMessageFormat = "startHuntingBattle&target_user_id={0}";
		public const string StartWarBattleMessageFormat = "startFactionWarBattle&slot_id={0}";
		public const string FightBrawlBattleMessage = "fightBrawlBattle";
		public const string UpgrageCardMessageFormat = "upgradeCard&card_id={0}";
		public const string SalvageCardMessageFormat = "salvageCard&card_id={0}";
		public const string SalvageAllCommonsMessage = "salvageL1CommonCards";
		public const string SalvageAllRaresMessage = "salvageL1RareCards";
		public const string FightMissionMessageFormat = "startMission&mission_id={0}";
		public const string FlagSettingMessageFormat = "setUserFlag&flag={0}&value={1}";
		public const string SetCardLockMessageFormat = "setCardLock&card_id={0}&locked={1}";
		public const string StartCampaignMessage = "startCampaign";
		public const string InitMessage = "init";
		public const string GetConquestDataMessage = "getConquestUpdate";
		public const string FightConquestBattleMessageFormat = "fightConquestBattle&zone_id={0}";
		public const string FightFactionQuestMessageFormat = "fightFactionQuest&quest_id={0}";
	}
}
