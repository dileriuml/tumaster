﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;
using TUMaster.Core.Data.Quests;
using TUMaster.Core.Data.Brawls;
using TUMaster.Core.Data.Faction;

namespace TUMaster.Core.Data
{
	public class APIResponse : GameServiceResponse
	{
		public const string CardFilesInfoKey = "card_files";

		public long ServerTime { get; set; }

		public long DailyBonusTime { get; set; }

		public bool IsDailyBonusReady => ServerTime > DailyBonusTime;

		public CardInfos CardInfos { get; private set; }

		public ServiceUserData UserData { get; set; }

		public ServiceWarData WarData { get; set; }

		public ServiceWarEventInfo WarEventInfo { get; set; }

		public ServiceStoreData StoreData { get; set; }

		public List<ServiceCardData> Cards { get; set; }

		public List<ServiceHuntingTarget> HuntingTargets { get; set; }

		public List<ServiceMissionCompletionData> MissionCompletions { get; set; }

		public ServiceDeckData Deck { get; set; }
		
		public ServiceProfileData ProfileData { get; set; }

		public ServiceGuildData MyGuildData { get; set; }

		public ServiceCombatData BattleData { get; set; }

		public ServiceRaidStatusData CurrentRaidStatus { get; set; }

		public ServiceCampaignData CampaignData { get; set; }

		public ServiceQuestInfo[] GuildQuests { get; set; }

		public ServiceBrawlsData UserBrawlData { get; set; }

		public FactionData Faction { get; set; }

		public bool ShowCombatK { get; set; }

		public bool Banned { get; set; }

		public int MaxEnergy { get; set; }

		public APIResponse()
		{
		}

		public APIResponse(JsonObject json)
			: base(json)
		{
			this.ServerTime = json.GetOrDefault("time", 0L);
			this.Banned = json.ContainsKey("is_banned") && json.Get<bool>("is_banned");
			this.ShowCombatK = !json.ContainsKey("combat_k_toggle") || json.Get<int>("combat_k_toggle") == 1;

			if (json.ContainsKey(ServiceUserData.UserDataJsonKey))
			{
				this.UserData = json.GetOrDefault<JsonObject>(ServiceUserData.UserDataJsonKey, null).With(x => new ServiceUserData(x));
				this.UserData.UserKey = json.GetOrDefault("new_password", string.Empty);
				this.UserData.SyncodeKey = json.GetOrDefault("new_syncode", string.Empty);
				this.UserData.MaxEnergy = json.GetOrDefault<int>("max_energy");
				this.UserData.MaxStamina = json.GetOrDefault<int>("max_stamina");
			}

			if (json.ContainsKey("daily_bonus_time"))
				this.DailyBonusTime = json.GetOrDefault("daily_bonus_time", 0L);

			if (json.ContainsKey("battle_data"))
				this.BattleData = json.GetOrDefault<JsonObject>("battle_data", null).With(x => new ServiceCombatData(x));

			if (json.ContainsKey("user_cards"))
			{
				this.Cards = new List<ServiceCardData>();
				JsonObject cardsField = json.Get<JsonObject>("user_cards");
				this.Cards.AddRange(cardsField.Keys.Select(key => new ServiceCardData(Convert.ToInt32(key), cardsField.Get<JsonObject>(key))));
			}

			if (json.ContainsKey("hunting_targets"))
			{
				this.HuntingTargets = new List<ServiceHuntingTarget>();
				JsonObject targetsField = json.Get<JsonObject>("hunting_targets");
				this.HuntingTargets.AddRange(targetsField.Values.Select(hTarget => new ServiceHuntingTarget(JsonObject.Parse(hTarget))));
			}

			if (json.ContainsKey("faction_war"))
			{
				WarData = new ServiceWarData(json.GetOrDefault<JsonObject>("faction_war", null));
			}

			if (json.ContainsKey("faction_war_event_info"))
			{
				WarEventInfo = new ServiceWarEventInfo(json.GetOrDefault<JsonObject>("faction_war_event_info", null));
			}

			if (json.ContainsKey("store_packs") && json.ContainsKey("store_promos"))
				StoreData = new ServiceStoreData(json);

			if (json.ContainsKey("raid_info"))
			{
				json.Get<JsonObject>("raid_info")?
					.Values
					.FirstOrDefault()
					.With(ri => 
					{
						this.CurrentRaidStatus = 
							new ServiceRaidStatusData(JsonObject.Parse(ri));
					});
			}

			if (json.ContainsKey("user_decks"))
			{
				this.Deck = json.Get<JsonObject>("user_decks")
					.Values
					.FirstOrDefault()
					.With(x => new ServiceDeckData(JsonObject.Parse(x)));
			}

			if (json.ContainsKey("mission_completions"))
				this.MissionCompletions = json.Get<JsonObject>("mission_completions")
					.Select(kv => new ServiceMissionCompletionData(
						kv.Key.ToInt(), 
						JsonObject.Parse(kv.Value)))
					.ToList();

			if (json.ContainsKey("player_info"))
				this.ProfileData = json.GetOrDefault<JsonObject>("player_info", null).With(x => new ServiceProfileData(x));

			if (json.ContainsKey(CardFilesInfoKey))
			{
				var cardsJson = json.GetOrDefault<JsonObject>(CardFilesInfoKey, null);
				CardInfos = new CardInfos(cardsJson);
			}

			if (json.ContainsKey("faction"))
			{
				Faction = json.GetOrDefault<FactionData>("faction");
			}

			//if (json.ContainsKey("current_campaigns"))
			//{
			//	CampaignData = json.GetOrDefault<JsonObject>("current_campaigns", null)?
			//		.With(x => x.Keys.FirstOrDefault()
			//			.With(k => x.GetOrDefault<JsonObject>(k, null)))
			//		.With(x => new ServiceCampaignData(x));
			//}

			if (json.ContainsKey("faction_quests"))
			{
				json.GetOrDefault<JsonObject>("faction_quests").
					With(fq =>
					{
						GuildQuests = fq.Keys
							.Select(gqId => fq.GetOrDefault<JsonObject>(gqId))
							.Select(gqJson => new ServiceQuestInfo(gqJson))
							.ToArray();
					});
			}

			if (json.ContainsKey("player_brawl_data"))
			{
				json.GetOrDefault<JsonObject>("player_brawl_data")
					.With(brawlJson => 
						UserBrawlData = brawlJson.Values.Any(value => value != null) ? new ServiceBrawlsData(brawlJson) : null);
			}
		}
	}
}
