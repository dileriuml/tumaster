﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data.Brawls
{
	public class ServiceBrawlsData : ServiceData
	{
		public int CurrentRank { get; set; }

		public int BattleEnergy { get; set; }

		public int MaxEnergy { get; set; }

		public int Wins { get; set; }

		public int Losses { get; set; }

		public int WinStreak { get; set; }

		public int Seeding { get; set; }

		public int Points { get; set; }

		public bool IsRewardsNotClaimed { get; set; }

		public ServiceBrawlsData(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			CurrentRank = json.GetOrDefault<int>("current_rank");
			Wins = json.GetOrDefault<int>("wins");
			Losses = json.GetOrDefault<int>("losses");
			WinStreak = json.GetOrDefault<int>("win_streak");
			Seeding = json.GetOrDefault<int>("seeding");
			IsRewardsNotClaimed = json.GetOrDefault<int>("claimed_rewards") == 0;
			Points = json.GetOrDefault<int>("points");
			json.GetOrDefault<JsonObject>("energy")
				.With(energyJson =>
				{
					BattleEnergy = energyJson.GetOrDefault<int>("battle_energy");
					MaxEnergy = energyJson.GetOrDefault<int>("max_battle_energy");
				});
		}
	}
}
