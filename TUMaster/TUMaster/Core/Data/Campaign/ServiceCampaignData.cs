﻿using ServiceStack.Text;
using System;

namespace TUMaster.Core.Data
{
	public class ServiceCampaignData : ServiceData
	{
		public string Name { get; set; }

		public int Id { get; set; }

		public int CurrentLevel { get; set; }

		public int EnergyCost { get; set; }

		public int LevelCompleted { get; set; }

		public int MaxProgress { get; set; }

		public int MaxReserves { get; set; }

		public ServiceCampaignRewardData RewardsInfo { get; set; }

		public ServiceCampaignData(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			Name = json.GetOrDefault("name", string.Empty);
			Id = json.GetOrDefault("id", 0);
			MaxProgress = json.GetOrDefault("max_progress", 0);
			LevelCompleted = json.GetOrDefault("level_completed", 0);
			MaxReserves = json.GetOrDefault("max_reserves", 0);
			EnergyCost = json.GetOrDefault("energy", 0);
			CurrentLevel = json.GetOrDefault("level", 0);

			json.GetOrDefault<JsonObject>("rewards", null).With(
				rewardsJson =>
					RewardsInfo = new ServiceCampaignRewardData(rewardsJson));
		}
	}
}