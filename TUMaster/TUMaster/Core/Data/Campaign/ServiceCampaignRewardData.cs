﻿using System;
using ServiceStack.Text;
using System.Collections.Generic;
using TUMaster.Core.Data.Campaign;
using System.Linq;

namespace TUMaster.Core.Data
{
	public class ServiceCampaignRewardData : ServiceData
	{
		public List<ServiceCampaignRewardPart> Rewards { get; }
		
		public ServiceCampaignRewardData(JsonObject obj) : base(obj)
		{
			Rewards = new List<ServiceCampaignRewardPart>();
		}

		protected override void Map(JsonObject json)
		{
			var rewardParts = json.Keys.SelectMany(level =>
				{
					var levelRewards = json.GetOrDefault<JsonObject>(level, null);
					return levelRewards.Keys.SelectMany(rewardType =>
					{
						var rewards = levelRewards.GetOrDefault<JsonObject>(rewardType, null);

						return rewards.Keys.Select(rewardId =>
						{
							var rewardInfo = rewards.GetOrDefault<JsonObject>(rewardId, null);

							return new ServiceCampaignRewardPart
							{
								Id = rewardId.ToInt(),
								Amount = rewardInfo.GetOrDefault("amount", 0),
								CollectedAmount = rewardInfo.GetOrDefault("collected", 0),
								ForLevel = level.ToInt(),
								Type = rewardType
							};
						});
					});
				});

			Rewards.AddRange(rewardParts);
		}
	}
}
