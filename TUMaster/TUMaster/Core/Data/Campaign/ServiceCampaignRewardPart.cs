﻿namespace TUMaster.Core.Data.Campaign
{
	public class ServiceCampaignRewardPart
	{
		public int ForLevel { get; set; }

		public string Type { get; set; }

		public int Id { get; set; }

		public int Amount { get; set; }

		public int CollectedAmount { get; set; }
	}
}
