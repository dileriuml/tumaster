﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TUMaster.Core.Data
{
	public class CardInfos
	{
		public const string FileNameKey = "file";

		public string[] CardFileNames { get; }

		public CardInfos(JsonObject fromObj)
		{
			var valuesCount = fromObj.Values.Count;
			CardFileNames = new string[valuesCount];
			var keys = fromObj.Keys.ToArray();

			for (int i = 0; i < valuesCount; i++)
			{
				CardFileNames[i] = fromObj.Get<JsonObject>(keys[i], null).Get(FileNameKey);
			}
		}
	}
}
