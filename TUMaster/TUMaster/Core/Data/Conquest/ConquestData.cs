﻿using System.Collections.Generic;
using ServiceStack.Text;
using System;
using System.Linq;

namespace TUMaster.Core.Data.Conquest
{
	public class ServiceConquestData : GameServiceResponse
	{
		public string Name { get; set; }

		public long StartTime { get; set; }

		public long EndTime { get; set; }

		public int Rank { get; set; }

		public int GuildPoints { get; set; }

		public int UserInfluence { get; set; }

		public bool IsRewardsNotClaimed { get; set; }

		public int Energy { get; set; }

		public int MaxEnergy { get; set; }

		public List<ZoneData> Zones { get; set; }

		public override void Map(JsonObject json)
		{
			base.Map(json);
			json.GetOrDefault<JsonObject>("conquest_data").With(MapConquestData);
		}

		private void MapConquestData(JsonObject json)
		{
			Name = json.GetOrDefault<string>("name");
			StartTime = json.GetOrDefault<long>("start_time");
			EndTime = json.GetOrDefault<long>("end_time");
			Rank = json.GetOrDefault<int>("conquest_rank");
			GuildPoints = json.GetOrDefault<int>("conquest_points");

			json.GetOrDefault<JsonObject>("user_conquest_data").With(
				userDataJson =>
				{
					IsRewardsNotClaimed = userDataJson.GetOrDefault<int>("claimed_reward") == 0;
					UserInfluence = userDataJson.GetOrDefault<int>("influence");

					userDataJson.GetOrDefault<JsonObject>("energy").With(energyJson =>
					{
						Energy = energyJson.GetOrDefault<int>("battle_energy");
						MaxEnergy = energyJson.GetOrDefault<int>("max_battle_energy");
					});
				});

			json.GetOrDefault<JsonObject>("zone_data").With(zonesJson =>
			{
				var zoneInfosJsons = from zoneId in zonesJson.Keys
														 select zonesJson.GetOrDefault<JsonObject>(zoneId);

				Zones = zoneInfosJsons
					.Select(zoneJson => new ZoneData(zoneJson))
					.OrderBy(x => x.Order)
					.ToList();
			});
		}
	}
}
