﻿using ServiceStack.Text;
using System;

namespace TUMaster.Core.Data.Conquest
{
	public class ZoneData
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public int Order { get; set; }

		public string BgEffectInfo { get; set; }

		public int GuildPoints { get; set; }

		public int GuildRank { get; set; }

		public ZoneData(JsonObject zoneJson)
		{
			Id = zoneJson.GetOrDefault<int>("id");
			Name = zoneJson.GetOrDefault<string>("name");
			Order = zoneJson.GetOrDefault<int>("order");
			BgEffectInfo = zoneJson.GetOrDefault<JsonObject>("battleground_effect_data")?
				.GetOrDefault<string>("desc");

			zoneJson
				.GetOrDefault<JsonObject>("rankings")
				.With(MapRanks);
		}

		private void MapRanks(JsonObject ranksJson)
		{
			GuildPoints = ranksJson.GetOrDefault<int>("my_guild_points");
			GuildRank = ranksJson.GetOrDefault<int>("my_guild_rank");
		}
	}
}