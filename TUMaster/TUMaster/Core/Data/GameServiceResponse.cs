﻿using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public abstract class GameServiceResponse
	{
		public ServiceBasicData Response { get; set; }

		public ServiceRequestData Request { get; set; }

		protected GameServiceResponse() { }

		protected GameServiceResponse(JsonObject json) => Map(json);

		public virtual void Map(JsonObject json)
		{
			this.Response = new ServiceBasicData(json);
			this.Request = new ServiceRequestData(json.Get<JsonObject>("request"));
		}
	}
}
