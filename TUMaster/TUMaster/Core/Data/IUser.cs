﻿namespace TUMaster.Core.Data
{
	public interface IUser
	{
		string UserId { get; set; }

		string Password { get; set; }

		string Syncode { get; set; }
	}
}