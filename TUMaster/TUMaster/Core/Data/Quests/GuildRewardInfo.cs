﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data.Quests
{
	public class GuildRewardInfo : ServiceData
	{
		public int Id { get; set; }

		public int Number { get; set; }

		public int GuildPoints { get; set; }

		public GuildRewardInfo(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			var guildItem = json.GetOrDefault<JsonObject>("guild_item", null);
			Id = guildItem.GetOrDefault("id", 0);
			Number = guildItem.GetOrDefault("number", 0);
			GuildPoints = json.GetOrDefault("guild_points", 0);
		}
	}
}
