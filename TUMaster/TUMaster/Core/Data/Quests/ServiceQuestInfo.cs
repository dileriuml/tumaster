﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data.Quests
{
	public class ServiceQuestInfo : ServiceData
	{
		public int Id { get; set; }

		public int CommanderId { get; set; }

		public bool Completed { get; set; }

		public string Description { get; set; }

		public int Duration { get; set; }

		public int Energy { get; set; }

		public int GoldPerWin { get; set; }

		public int Mission { get; set; }

		public string Name { get; set; }

		public string Title { get; set; }

		public int MaxProgress { get; set; }

		public int Progress { get; set; }

		public GuildRewardInfo RewardInfo { get; set; }

		public ServiceQuestInfo(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			Id = json.GetOrDefault<int>("id");
			CommanderId = json.GetOrDefault<int>("commander_id");
			Description = json.GetOrDefault<string>("desc");
			Completed = json.GetOrDefault<int>("completed") == 1;
			Duration = json.GetOrDefault<int>("duration");
			Energy = json.GetOrDefault<int>("energy");
			GoldPerWin = json.GetOrDefault<int>("gold_per_win");
			Mission = json.GetOrDefault<int>("mission");
			Name = json.GetOrDefault<string>("name");
			Title = json.GetOrDefault<string>("short_desc");
			Progress = json.GetOrDefault<int>("progress");
			MaxProgress = json.GetOrDefault<JsonObject>("req")?.GetOrDefault<int>("num_to_complete") ?? 0;
			json.GetOrDefault<JsonObject>("guild_reward").With(r =>
			{
				RewardInfo = new GuildRewardInfo(r);
			});
		}
	}
}
