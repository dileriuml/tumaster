﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceBasicData : ServiceData
	{
		public int CurrentTime { get; set; }
		
		public bool Result { get; set; }

		public List<string> ResultMessages { get; set; }

		public string ResultMessage
			=> ResultMessages.WithEmpty(xCol => string.Join(string.Empty, ResultMessages));

		public ServiceBasicData()
		{
		}

		public ServiceBasicData(JsonObject json) : base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			CurrentTime = json.GetOrDefault("time", 0);
			Result = json.GetOrDefault("result", false);
			json.Get<JsonArrayObjects>("result_message").WithEmpty(xCol => ResultMessages = xCol.Select(x => x.Keys.First()).ToList());
		}
	}
}
