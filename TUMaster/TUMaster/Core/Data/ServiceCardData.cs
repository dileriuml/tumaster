﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceCardData : ServiceData
	{
		public int CardID { get; set; }

		public int Quantity { get; set; }

		public bool isLocked { get; set; }

		public bool IsCommander
		{
			get
			{
				return CardID >= 1000 && CardID < 2000;
			}
		}

		public ServiceCardData()
		{
		}

		public ServiceCardData(int cardID, JsonObject json) : base(json)
		{
			this.CardID = cardID;
		}

		protected override void Map(JsonObject json)
		{
			Quantity = json.GetOrDefault("num_owned", 0);
			isLocked = json.GetOrDefault("is_locked", false);
		}

		public ServiceCardData Take(int count)
		{
			Quantity -= count;

			return new ServiceCardData()
			{
				CardID = CardID,
				Quantity = count,
				isLocked = isLocked
			};
		}
	}
}
