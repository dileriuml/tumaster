﻿using System;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceCombatCardMapData : ServiceData
	{
		public int CardUID { get; set; }

		public int CardID { get; set; }

		public ServiceCombatCardMapData()
		{
		}

		public ServiceCombatCardMapData(JsonObject json)
		{
			this.Map(json);
		}

		protected override void Map(JsonObject json)
		{
			try
			{
				var jsonKey = json.Keys.FirstOrDefault();
				jsonKey.With(key =>
				{
					CardUID = key.ToInt();
					CardID = json.GetOrDefault(key, 0);
				});
			}
			catch
			{
			}
		}
	}
}
