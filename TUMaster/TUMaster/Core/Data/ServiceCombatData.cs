﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;

namespace TUMaster.Core.Data
{
	public class ServiceCombatData : ServiceData
	{
		public long BattleID { get; set; }

		public int HostID { get; set; }

		public bool HostIsAttacker { get; set; }

		public int AttackCommanderID { get; set; }

		public int DefendCommanderID { get; set; }

		public int PlayerSize { get; set; }

		public int EnemySize { get; set; }

		public int EnemyID { get; set; }

		public string EnemyName { get; set; }

		public int MissionID { get; set; }

		//public string BattlegroundIcon { get; set; }

		//public string BattlegroundDesc { get; set; }

		public string EndTime { get; set; }

		public bool IsWinner { get; set; }

		public bool IsLoser { get { return !IsWinner; } }

		public bool HasOutcome { get; set; }

		public bool HasRewards { get; set; }

		public List<ServiceRewardData> Rewards { get; set; }

		public string Message { get; set; }

		public bool MissionExists
		{
			get
			{
				return MissionID > 0;
			}
		}

		public ServiceCombatData()
		{
			//this.Turns = new List<ServiceCombatTurnData>();      
			//this.DefendDeck = new List<ServiceCombatCardMapData>();
			//this.CardMap = new List<ServiceCombatCardMapData>();
			this.Rewards = new List<ServiceRewardData>();
		}

		public ServiceCombatData(JsonObject json)
		{
			this.Map(json);
		}

		protected override void Map(JsonObject json)
		{
			//Debug.LogWarning((object)("parsing combat data: " + json.ToString()));
			BattleID = json.GetOrDefault("battle_id", 0L);
			HostID = json.GetOrDefault("host_id", 0);
			HostIsAttacker = json.GetOrDefault("host_is_attacker", false);
			AttackCommanderID = json.GetOrDefault("attack_commander", 0);
			//DefendDeck = !json.ContainsKey("defend_deck") ? new List<ServiceCombatCardMapData>() : JSONUtils.ToJSONList<ServiceCombatCardMapData>(json, "defend_deck");
			DefendCommanderID = json.GetOrDefault("defend_commander", 0);
			EnemySize = json.GetOrDefault("enemy_size", 0);
			PlayerSize = json.GetOrDefault("player_size", 0);
			EnemyName = json.GetOrDefault("enemy_name", string.Empty);
			EnemyID = json.GetOrDefault("enemy_id", 0);

			//      if (json.ContainsKey("battleground_effect")) {
			//        JsonObject field = json.GetField("battleground_effect");
			//        this.BattlegroundIcon = JSONUtils.ToJSONString(field, "web_picture", string.Empty);
			//        this.BattlegroundDesc = JSONUtils.ToJSONString(field, "name", string.Empty) + "\n" + JSONUtils.ToJSONString(field, "desc", string.Empty);
			//      }
			//this.Turns = JSONUtils.ToJSONList<ServiceCombatTurnData>(json, "turn");
			//this.CardMap = JSONUtils.ToJSONList<ServiceCombatCardMapData>(json, "card_map");

			if (json.ContainsKey("winner"))
			{
				HasOutcome = true;
				IsWinner = json.GetOrDefault("winner", false);
			}

			HasRewards = json.ContainsKey("rewards");
			Rewards = HasRewards ? Rewards = json.Get<JsonArrayObjects>("rewards").ConvertAll(x => new ServiceRewardData(x)) : new List<ServiceRewardData>();
		}
	}
}
