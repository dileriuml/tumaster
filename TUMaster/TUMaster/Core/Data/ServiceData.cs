﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public abstract class ServiceData
	{
		#region Fields

		public string RawData { get; set; }

		#endregion

		#region Constructors

		protected ServiceData()
		{
		}

		protected ServiceData(JsonObject json) => this.Map(json);

		#endregion

		#region Abstract methods

		protected abstract void Map(JsonObject json);

		#endregion
	}
}