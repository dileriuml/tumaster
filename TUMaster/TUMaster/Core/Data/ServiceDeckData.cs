﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	/// <summary>
	/// Description of ServiceDeckData.
	/// </summary>
	public class ServiceDeckData : ServiceData
	{
		public int CommanderId { get; set; }
		public int DominionId { get; set; }
		public Dictionary<int, int> Cards { get; set; }

		public ServiceDeckData() { }

		public ServiceDeckData(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			CommanderId = json.GetOrDefault<int>("commander_id");
			DominionId = json.GetOrDefault<int>("dominion_id");
			Cards = new Dictionary<int, int>();
			json.GetOrDefault<JsonObject>("cards", null).With(x => x.ToList().ForEach(kp => Cards.Add(kp.Key.ToInt(), kp.Value.ToInt())));
		}

		public ServiceDeckData GetCopy()
		{
			return new ServiceDeckData
			{
				CommanderId = CommanderId,
				//Cards = Cards.ToDictionary(x => new KeyValuePair<int, int>(x.Key, x.Value))
			};
		}
	}
}
