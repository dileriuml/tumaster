﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceGuildData : ServiceData
	{
		public string GuildId { get; private set; }

		public string name { get; private set; }

		public string LeaderId { get; private set; }

		public string LeaderName { get; private set; }

		public int NumMembers { get; private set; }

		public int MaxMembers { get; private set; }

		public int Rank { get; private set; }

		public int TotalRating { get; private set; }

		public int GuildPoints { get; private set; }

		public int FortressSlotHealth { get; private set; }

		public int RecruitmentStatus { get; set; }

		public int MinRating { get; set; }

		public string Message { get; private set; }

		public List<ServiceGuildMemberData> Members { get; private set; }

		public Dictionary<int, int> FactionCards { get; set; }

		protected override void Map(JsonObject json)
		{
			GuildId = json.GetOrDefault("faction_id", string.Empty);
			name = json.GetOrDefault("name", string.Empty);
			LeaderId = json.GetOrDefault("leader_id", string.Empty);
			LeaderName = json.GetOrDefault("leader_name", string.Empty);
			NumMembers = json.GetOrDefault("num_members", 0);
			MaxMembers = json.GetOrDefault("max_members", 50);
			RecruitmentStatus = json.GetOrDefault("recruitment_status", 0);
			MinRating = json.GetOrDefault("min_rating", 0);
			Rank = json.GetOrDefault("rank", 0);
			TotalRating = json.GetOrDefault("total_rating", 0);
			GuildPoints = json.GetOrDefault("guild_points", 0);
			FortressSlotHealth = json.GetOrDefault("max_slot_health", 100);

			if (json.ContainsKey("members"))
				Members = json.Get<JsonArrayObjects>("members").ConvertAll(x => new ServiceGuildMemberData(x));

			if (json.ContainsKey("message"))
				Message = json.GetOrDefault("message", string.Empty);

			FactionCards = new Dictionary<int, int>();

			var field = json.GetOrDefault("faction_cards", (JsonObject)(null));

			if (field == null)
				return;

			foreach (JsonObject value in field.Values.Select(JsonObject.Parse))
				value.Keys.FirstOrDefault().With(key => this.FactionCards.Add(key.ToInt(), json.GetOrDefault("num_owned", 0)));
		}

		public ServiceGuildMemberData getMemberData(string user_id)
		{
			return Members.FirstOrDefault(member => member.UserID == user_id);
		}
	}
}
