﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceGuildMemberData : ServiceData
	{
		public string GuildID { get; private set; }

		public string UserID { get; private set; }

		public string Name { get; private set; }

		public int level { get; private set; }

		public int Rating { get; private set; }

		public int CommanderID { get; private set; }

		public string message { get; private set; }

		public int GpEarned { get; private set; }

		public int Rank { get; private set; }

		public long LastTime { get; private set; }

		protected override void Map(JsonObject json)
		{
			GuildID = json.GetOrDefault("faction_id", string.Empty);
			UserID = json.GetOrDefault("user_id", string.Empty);
			Name = json.GetOrDefault("name", string.Empty);
			Rating = json.GetOrDefault("rating", 0);
			CommanderID = json.GetOrDefault("commander_id", 1000);
			GpEarned = json.GetOrDefault("gp_earned", 0);
			Rank = json.GetOrDefault("member_role", 0);
			LastTime = json.GetOrDefault("last_update_time", 0L);
		}

		public ServiceGuildMemberData()
		{
		}

		public ServiceGuildMemberData(JsonObject json)
		{
			Map(json);
		}
	}
}
