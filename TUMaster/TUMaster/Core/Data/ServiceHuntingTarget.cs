﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceHuntingTarget : ServiceData
	{
		public string UserName { get; set; }

		public int UserId { get; set; }

		public int BattleRate { get; set; }

		public int CommanderId { get; set; }

		public int GoldReward { get; set; }

		public int SpReward { get; set; }

		public ServiceHuntingTarget(JsonObject obj) : base(obj)
		{

		}

		protected override void Map(JsonObject json)
		{
			UserName = json.GetOrDefault("name", string.Empty);
			UserId = json.GetOrDefault("user_id", 0);
			BattleRate = json.GetOrDefault("hunting_elo", 0);
			CommanderId = json.GetOrDefault("commander", 0);
			GoldReward = json.GetOrDefault("gold", 0);
			SpReward = json.GetOrDefault("sp", 0);
		}
	}
}
