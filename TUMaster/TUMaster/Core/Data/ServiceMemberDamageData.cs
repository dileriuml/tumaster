﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceMemberDamageData : ServiceData
	{
		public int MemberId { get; set; }

		public string MemberName { get; set; }

		public int MemberDamage { get; set; }

		public ServiceMemberDamageData(JsonObject obj) : base(obj)
		{

		}

		protected override void Map(JsonObject json)
		{
			MemberId = json.GetOrDefault("member_id", 0);
			MemberName = json.GetOrDefault("member_name", string.Empty);
			MemberDamage = json.GetOrDefault("damage", 0);
		}
	}
}
