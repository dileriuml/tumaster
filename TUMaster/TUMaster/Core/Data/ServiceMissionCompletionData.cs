﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceMissionCompletionData : ServiceData
	{
		public int Id { get; set; }

		public int CompleteCount { get; set; }

		public ServiceMissionCompletionData()
		{
		}

		public ServiceMissionCompletionData(int id, JsonObject obj) : base(obj)
		{
			Id = id;
		}

		protected override void Map(JsonObject json)
		{
			CompleteCount = json.GetOrDefault("number", 0);
		}
	}
}
