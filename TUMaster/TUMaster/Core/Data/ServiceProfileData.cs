﻿using System;
using System.Collections.Generic;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceProfileData : ServiceData
	{
		public string user_id { get; private set; }

		public string name { get; private set; }

		public int starCount { get; private set; }

		public int warPoints { get; private set; }

		public int level { get; private set; }

		public string level_name { get; private set; }

		public long lastTime { get; private set; }

		public int commander { get; private set; }

		public List<int> cards { get; private set; }

		public ServiceProfileData(JsonObject json) : base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			user_id = json.GetOrDefault("user_id", string.Empty);
			name = json.GetOrDefault("name", string.Empty);
			starCount = json.GetOrDefault("stars_earned", 0);
			warPoints = json.GetOrDefault("faction_war_points", -1);
			level = json.GetOrDefault("level", 0);
			level_name = json.GetOrDefault("level_name", string.Empty);
			lastTime = json.GetOrDefault("last_update_time", 0L);

			var deckField = json.Get<JsonObject>("deck");
			commander = deckField.GetOrDefault("commander_id", 0);
			cards = deckField.ArrayObjects("cards").ConvertAll(x => x.Get<int>(string.Empty));
			cards.Add(commander);
		}
	}
}
