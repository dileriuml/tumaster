﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceRaidStatusData : GameServiceResponse
	{
		public int RaidId { get; private set; }

		public int Level { get; private set; }

		public int Health { get; private set; }

		public int MaxHealth { get; private set; }

		public long BattleStartTime { get; private set; }

		public long BattleEndTime { get; private set; }

		public bool IsNotClaimedReward { get; set; }

		public int Energy { get; set; }

		public int MaxEnergy { get; set; }

		public List<ServiceMemberDamageData> Members { get; private set; }

		public ServiceRaidStatusData()
		{
		}

		public ServiceRaidStatusData(JsonObject json) : base(json) { }

		public override void Map(JsonObject json)
		{
			json.GetOrDefault<JsonObject>("raid_info")
				.With(raidJson => raidJson.Values?.FirstOrDefault())
				.With(JsonObject.Parse)
				.With(MapRaidData);
		}

		private void MapRaidData(JsonObject json)
		{
			RaidId = json.GetOrDefault("raid_id", 0);
			Level = json.GetOrDefault("raid_level", 0);
			Health = json.GetOrDefault("health", 0);
			MaxHealth = json.GetOrDefault("max_health", 0);
			BattleStartTime = json.GetOrDefault("raid_level_start", 0L);
			BattleEndTime = json.GetOrDefault("raid_level_end", 0L);
			IsNotClaimedReward = json.GetOrDefault("claimed_rewards", 0) == 0;

			json.GetOrDefault<JsonObject>("energy")
				.With(energyJson =>
				{
					Energy = energyJson.GetOrDefault<int>("battle_energy");
					MaxEnergy = energyJson.GetOrDefault<int>("max_battle_energy");
				});

			if (!json.ContainsKey("members"))
				return;

			Members = json.GetOrDefault<JsonObject>("members", null)
				.With(x => x.Select(y => new ServiceMemberDamageData(JsonObject.Parse(y.Value)))
										.OrderByDescending(z => z.MemberDamage)
										.ToList());
		}

		public int GetDamageForUser(string userId)
		{
			return Members.FirstOrDefault(x => x.MemberId == userId.ToInt()).With(x => x.MemberDamage);
		}
	}
}
