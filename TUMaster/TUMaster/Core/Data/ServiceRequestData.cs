﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceRequestData : ServiceData
	{
		public string Message { get; private set; }

		public string UserID { get; private set; }

		public string Password { get; private set; }

		public int ProductID { get; private set; }

		public ServiceRequestData()
		{
		}

		public ServiceRequestData(JsonObject json) : base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			Message = json.GetOrDefault("message", string.Empty);
			UserID = json.GetOrDefault("user_id", string.Empty);
			Password = json.GetOrDefault("password", string.Empty);
			ProductID = json.GetOrDefault("item_id", 0);

			if (!json.ContainsKey("pack_id"))
				return;

			ProductID = json.GetOrDefault("pack_id", 0);
		}
	}
}
