﻿using System;
using System.Collections.Generic;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceRewardData : ServiceData
	{
		public string UserID { get; set; }

		public int Gold { get; set; }

		public int Warbonds { get; set; }

		public List<int> Cards { get; set; }

		public int ArenaRating { get; set; }

		public int TrophyRating { get; set; }

		public int SP { get; set; }

		public int WarPoints { get; set; }

		public int SlotDamage { get; set; }

		// public List<ServiceStarData> Stars { get; set; }

		public string Message { get; set; }

		public ServiceRewardData()
		{
			this.Cards = new List<int>();
			//this.Stars = new List<ServiceStarData>();
		}

		public ServiceRewardData(JsonObject json) : base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			UserID = json.GetOrDefault("user_id", "0");
			Gold = json.GetOrDefault("gold", 0);
			Warbonds = json.GetOrDefault("wb", 0);
			//Cards = json.Get<JsonArrayObjects>("card").ConvertAll(x => x.Keys.First().ToInt());
			ArenaRating = json.GetOrDefault("rating", 0);
			TrophyRating = json.GetOrDefault("rating_change", 0);
			SP = json.GetOrDefault("sp", 0);
			//this.Stars = JSONUtils.ToJSONList<ServiceStarData>(json, "star");
			Message = json.GetOrDefault("message", string.Empty);
			WarPoints = !json.ContainsKey("pvp_points") ? json.GetOrDefault("war_points", 0) : json.GetOrDefault("pvp_points", 0);
			SlotDamage = !json.ContainsKey("raid_damage") ? json.GetOrDefault("slot_damage", 0) : json.GetOrDefault("raid_damage", 0);
		}
	}
}
