﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public enum Slots
	{
		Core = 1,
		TopDefence = 11,
		BottomDefence = 12,
		TopSiege = 21,
		BottomSiege = 22
	}

	/// <summary>
	/// Description of ServiceSlotInfo.
	/// </summary>
	public class ServiceSlotInfo : ServiceData
	{
		public int SlotId { get; set; }

		public int FortressId { get; set; }

		public int Health { get; set; }

		public Slots SlotName
		{
			get
			{
				return (Slots)SlotId;
			}
		}

		public ServiceSlotInfo(JsonObject json) : base(json)
		{

		}

		protected override void Map(JsonObject json)
		{
			SlotId = json.GetOrDefault("slot_id", 1);
			FortressId = json.GetOrDefault("card_id", 0);
			Health = json.GetOrDefault("health", 0);
		}
	}
}
