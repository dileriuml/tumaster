﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceUserData : ServiceData
	{
		public const string UserDataJsonKey = "user_data";

		public string UserID { get; set; }

		public string Name { get; set; }

		public string FactionName { get; set; }

		public string UserKey { get; set; }

		public string SyncodeKey { get; set; }

		public DateTime InstallDate { get; set; }

		public int ActiveDeckID { get; set; }

		public int DefenseDeckID { get; set; }

		public int LocationID { get; set; }

		public int LastUpdateTime { get; set; }

		public int Tokens { get; set; }

		public int Money { get; set; }

		public int TokensBought { get; set; }

		public int TokensSpent { get; set; }

		public int Energy { get; set; }

		public int EnergyRechargeTime { get; set; }

		public int BattleEnergy { get; set; }

		public int BattleRechargeTime { get; set; }

		public int LastBattleTime { get; set; }

		public int Stamina { get; set; }

		public int StaminaRechargeTime { get; set; }

		public int LastStaminaTime { get; set; }

		public long ShieldEndTime { get; set; }

		public int SalvagePoints { get; set; }

		public int MaxSalvage { get; set; }

		public int MaxCards { get; set; }

		public int NameChanges { get; set; }

		public int Stars { get; set; }

		public int HuntingLevel { get; set; }

		public int TrophyRating { get; set; }

		public int ArenaRating { get; set; }

		public int ArenaWins { get; set; }

		public int ArenaLosses { get; set; }

		public int HuntingWins { get; set; }

		public int CommanderUnlockRarity { get; set; }

		public int PromotionPoints { get; set; }

		public int BaseXP { get; set; }

		public int NextXP { get; set; }

		public int Level { get; set; }

		public int MissionWins { get; set; }

		public string LevelName { get; set; }

		public int CardsEarned { get; set; }

		public int GoldEarned { get; set; }

		public bool IsBanned { get; set; }

		public int DeckPower { get; set; }

		public int MaxEnergy { get; set; }

		public int MaxStamina { get; set; }

		public ServiceUserFlagData Flags { get; set; }

		public ServiceUserData()
		{
		}

		public ServiceUserData(JsonObject json) : base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			if (json == null)
				return;

			UserID = json.GetOrDefault("user_id", string.Empty);
			Name = json.GetOrDefault("name", "Player" + UserID);
			InstallDate = json.GetOrDefault("install_date", default(DateTime));
			Flags = !json.ContainsKey("flags") ? new ServiceUserFlagData() : new ServiceUserFlagData(json.Get<JsonObject>("flags"));
			LocationID = json.GetOrDefault("location_id", 0);
			LastUpdateTime = json.GetOrDefault("last_update_time", 0);
			Tokens = json.GetOrDefault("tokens", 0);
			Money = json.GetOrDefault("money", 0);
			Energy = json.GetOrDefault("energy", 0);
			BattleEnergy = json.GetOrDefault("battle_energy", 0);
			Stamina = json.GetOrDefault("stamina", 0);
			EnergyRechargeTime = json.GetOrDefault("energy_recharge_time", 60);
			BattleRechargeTime = json.GetOrDefault("battle_energy_recharge_time", 900);
			StaminaRechargeTime = json.GetOrDefault("stamina_recharge_time", 300);
			LastBattleTime = json.GetOrDefault("battle_energy_update_time", 0);
			LastStaminaTime = json.GetOrDefault("last_stam_update_time", 0);
			SalvagePoints = json.GetOrDefault("salvage", 0);
			ActiveDeckID = json.GetOrDefault("active_deck", 0);
			NameChanges = json.GetOrDefault("name_changes", 0);
			Stars = json.GetOrDefault("stars", 0);
			HuntingLevel = json.GetOrDefault("hunting_level", 0);
			TrophyRating = json.GetOrDefault("hunting_elo", 0);
			ArenaRating = json.GetOrDefault("arena_rating", 0);
			ArenaWins = json.GetOrDefault("arena_wins", 0);
			ArenaLosses = json.GetOrDefault("arena_losses", 0);
			TokensBought = json.GetOrDefault("tokens_bought", 0);
			TokensSpent = json.GetOrDefault("tokens_spent", 0);
			ShieldEndTime = json.GetOrDefault("hunting_protection", 0);
			HuntingWins = json.GetOrDefault("hunting_wins", 0);
			CommanderUnlockRarity = json.GetOrDefault("commander_unlock_rarity", 0);
			PromotionPoints = json.GetOrDefault("promotion_points", 0);
			MissionWins = json.GetOrDefault("mission_wins", 0);
			DeckPower = json.GetOrDefault("deck_power", 0);
			BaseXP = json.GetOrDefault("base_xp", 0);
			NextXP = json.GetOrDefault("next_xp", 0);
			Level = json.GetOrDefault("level", 1);
			LevelName = json.GetOrDefault("level_name", "General");
			CardsEarned = json.GetOrDefault("cards_earned", 0);
			GoldEarned = json.GetOrDefault("gold_earned", 0);

			if (json.ContainsKey("caps"))
			{
				var field = json.Get<JsonObject>("caps");
				MaxSalvage = field.GetOrDefault("max_salvage", 150);
				MaxCards = field.GetOrDefault("max_cards", 50);
			}
		}

		public void Remap(JsonObject json)
		{
			Map(json);
		}

		public static ServiceUserData DesignTimeModel
		{
			get
			{
				return new ServiceUserData()
				{
					Name = "MAXIMUS",
					UserID = "100000",
					Energy = 200,
					BattleEnergy = 400,
					Stamina = 1000,
					Money = 1000,
					SalvagePoints = 200,
					MaxCards = 400,
					Tokens = 1000,
					DeckPower = 10000,
					GoldEarned = 2000000,
					TrophyRating = 1000000
				};
			}
		}
	}
}
