﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public static class Flags
	{
		public const string MUSIC = "music";
		public const string SOUND = "sound";
		public const string AUTOPILOT = "autopilot";
		public const string SPEED = "speed";
		public const string SHOW_FILTERS = "show_filters";
	}

	public class ServiceUserFlagData : ServiceData
	{
		public bool MusicOn { get; set; }

		public bool SoundOn { get; set; }

		public int Tutorial { get; set; }

		public bool AutoPilotOn { get; set; }

		public int Speed { get; set; }

		public bool ShowFilters { get; set; }

		public bool ShowMissions { get; set; }

		public ServiceUserFlagData()
		{
		}

		public ServiceUserFlagData(JsonObject json) : base(json) { }

		protected override void Map(JsonObject json)
		{
			MusicOn = json.GetOrDefault("music", false);
			SoundOn = json.GetOrDefault("sound", false);
			Tutorial = json.GetOrDefault("tutorial", 0);
			AutoPilotOn = json.GetOrDefault("autopilot", false);
			Speed = json.GetOrDefault("speed", 0);
			ShowFilters = json.GetOrDefault("show_filters", false);
			ShowMissions = json.GetOrDefault("show_missions", false);
		}
	}
}
