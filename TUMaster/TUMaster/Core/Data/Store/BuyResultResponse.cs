﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;

namespace TUMaster.Core.Data.Store
{
	public class BuyResultResponse : GameServiceResponse
	{
		public bool IsTooManyCards { get; set; }
		
		public Dictionary<int, int> NewCards { get; set; } = new Dictionary<int, int>();

		public override void Map(JsonObject json)
		{
			base.Map(json);
			IsTooManyCards = json.GetOrDefault("too_many_cards", 0) == 1;

			if (IsTooManyCards)
			{
				return;
			}

			json.GetOrDefault<JsonArrayObjects>("new_cards_data")?.ForEach(x =>
			{
				var id = x.GetOrDefault<int>("id");
				var num = x.GetOrDefault<int>("number");
				NewCards[id] = num;
			});
		}
	}
}
