﻿using ServiceStack.Text;
using System;

namespace TUMaster.Core.Data.Store
{
	public class ServiceItemToBuy : ServiceData
	{
		public int Id { get; set; }
		public int Count { get; set; }

		public ServiceItemToBuy(JsonObject obj) : base(obj)
		{

		}

		protected override void Map(JsonObject json)
		{
			Id = json.GetOrDefault("id", 0);
			Count = json.GetOrDefault("count", 0);
		}
	}
}
