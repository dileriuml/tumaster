﻿using ServiceStack.Text;
using System;

namespace TUMaster.Core.Data.Store
{
	public class ServicePack : ServiceStoreItem
	{
		public int PackId { get; set; }

		public ServicePack(JsonObject obj)
			: base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			base.Map(json);
			PackId = json.GetOrDefault("pack_id", 0);
		}
	}
}
