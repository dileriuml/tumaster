﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;

namespace TUMaster.Core.Data.Store
{
	public class ServicePromo : ServiceStoreItem
	{
		public int Id { get; set; }

		public int ItemType { get; set; }

		public List<ServiceItemToBuy> Items = new List<ServiceItemToBuy>();

		public ServicePromo(JsonObject obj) : base(obj)
		{
		}

		protected override void Map(JsonObject json)
		{
			base.Map(json);
			Id = json.GetOrDefault("item_id", 0);
			Items = json.GetOrDefault<JsonArrayObjects>("card", null).With(x => x.ConvertAll(item => new ServiceItemToBuy(item)));
			ItemType = json.GetOrDefault("item_type", 0);
		}
	}
}
