﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;
using TUMaster.Core.Data.Store;

namespace TUMaster.Core.Data
{
	public class ServiceStoreData : ServiceData
	{
		public List<ServiceStoreItem> Items { get; set; }

		public ServiceStoreData(JsonObject obj) : base(obj)
		{

		}

		protected override void Map(JsonObject json)
		{
			Items = new List<ServiceStoreItem>();
			json.GetOrDefault<JsonObject>("store_packs", null).With(x => x.Values.ForEach(packStr => Items.Add(new ServicePack(JsonObject.Parse(packStr)))));
			json.GetOrDefault<JsonArrayObjects>("store_promos", null).With(x => x.ConvertAll(promo => new ServicePromo(promo)).Where(promo => promo.CurrencyType != PackCurrency.None).ForEach(Items.Add));
		}
	}
}
