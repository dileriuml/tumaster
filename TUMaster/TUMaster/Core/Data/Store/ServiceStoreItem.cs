﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public enum PackCurrency
	{
		None,
		Gold,
		WB
	}

	/// <summary>
	/// Description of ServiceStoreItem.
	/// </summary>
	public abstract class ServiceStoreItem : ServiceData
	{
		public ServiceStoreItem(JsonObject json)
			: base(json)
		{
		}

		public string Name { get; set; }

		public string Description { get; set; }

		public int GoldCost { get; set; }

		public int WbCost { get; set; }

		public PackCurrency CurrencyType { get; set; }

		protected override void Map(JsonObject json)
		{
			Name = json.GetOrDefault("name", string.Empty);
			Description = json.GetOrDefault("desc", string.Empty);
			GoldCost = json.GetOrDefault("gold_cost", 0);
			WbCost = json.GetOrDefault("wb_cost", 0);

			if (GoldCost != 0)
				CurrencyType = PackCurrency.Gold;
			else if (WbCost != 0)
				CurrencyType = PackCurrency.WB;
		}
	}
}
