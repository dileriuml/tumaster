﻿namespace TUMaster.Core.Data
{
	public class TimeData
	{
		public long ServerTime { get; set; }
		public long DailyBonusTime { get; set; }

		public TimeData(APIResponse userData)
		{
			DailyBonusTime = userData.DailyBonusTime;
			ServerTime = userData.ServerTime;
		}
	}
}
