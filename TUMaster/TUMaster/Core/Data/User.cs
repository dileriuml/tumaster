﻿using TUMaster.Core.Services.Botting;

namespace TUMaster.Core.Data
{
	public class User : IUser
	{
		public User() { }

		public User(IUser fromAnother)
		{
			UserId = fromAnother.UserId;
			Password = fromAnother.Password;
			Syncode = fromAnother.Syncode;
		}

		#region IUser implementation

		public string UserId { get; set; }

		public string Password { get; set; }

		public string InGameName { get; set; }

		public bool IsBanned { get; set; }

		public string Syncode { get; set; }

		public BotConfiguration BotConfiguration { get; set; }

		#endregion

		public static User Empty => new User
		{
			UserId = string.Empty,
			Password = string.Empty,
			Syncode = string.Empty
		};
	}
}
