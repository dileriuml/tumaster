﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceActiveWarEventInfo : ServiceData
	{
		public int DecayTime { get; set; }

		public int StartTime { get; set; }

		public int EndTime { get; set; }

		public ServiceActiveWarEventInfo(JsonObject json)
			: base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			DecayTime = json.GetOrDefault("decay_time", 0);
			StartTime = json.GetOrDefault("start_time", 0);
			EndTime = json.GetOrDefault("end_time", 0);
		}
	}
}
