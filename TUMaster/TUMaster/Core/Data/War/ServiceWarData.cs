﻿using System;
using System.Collections.Generic;
using System.Linq;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceWarData : ServiceData
	{
		public int WarId { get; set; }

		public string AttackerFaction { get; set; }

		public string DefenderFaction { get; set; }

		public int WarPoints { get; set; }

		public int Wins { get; set; }

		public int Loses { get; set; }

		public int MaxEnergy { get; set; }

		public int AttackerPoints { get; set; }

		public int DefenderPoints { get; set; }

		public bool IsWarRewardNotClaimed { get; set; }

		public bool IsRewardsAvailable { get; set; }

		public List<ServiceSlotInfo> AttackerSlots { get; set; }

		public List<ServiceSlotInfo> DefenderSlots { get; set; }

		public List<ServiceWarFactionMember> AttackerMembers { get; set; }

		public List<ServiceWarFactionMember> DefenderMembers { get; set; }

		public int Energy { get; set; }

		public long EndTime { get; set; }
		
		public ServiceWarData(JsonObject json)
			: base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			WarId = json.GetOrDefault("faction_war_id", 0);
			AttackerFaction = json.GetOrDefault("attacker_faction_name", string.Empty);
			DefenderFaction = json.GetOrDefault("defender_faction_name", string.Empty);
			AttackerPoints = json.GetOrDefault("attacker_faction_points", 0);
			WarPoints = json.GetOrDefault("faction_war_points", 0);
			Wins = json.GetOrDefault("wins", 0);
			Loses = json.GetOrDefault("losses", 0);
			MaxEnergy = json.GetOrDefault("max_battle_energy", 0);
			Energy = json.GetOrDefault("battle_energy", 0);

			DefenderPoints = json.GetOrDefault("defender_faction_points", 0);
			AttackerSlots = json.GetOrDefault<JsonObject>("attacker_faction_fortress_slots", null)
				.With(x => x.Values.Select(val => new ServiceSlotInfo(JsonObject.Parse(val))).ToList());
			DefenderSlots = json.GetOrDefault<JsonObject>("defender_faction_fortress_slots", null)
				.With(x => x.Values.Select(val => new ServiceSlotInfo(JsonObject.Parse(val))).ToList());

			AttackerMembers = (json.GetOrDefault<JsonArrayObjects>("attacker_faction_members", null) ??
			json.GetOrDefault<JsonArrayObjects>("members", null))
				.With(c => c.ConvertAll(x => x.With(member => new ServiceWarFactionMember(member))));

			DefenderMembers = json.GetOrDefault<JsonArrayObjects>("defender_faction_members", null)
				.With(c => c.ConvertAll(x => x.With(member => new ServiceWarFactionMember(member))));

			EndTime = json.GetOrDefault("end_time", 0L);
		}
	}
}
