﻿using ServiceStack.Text;
using System;
using System.Collections.Generic;
using System.Linq;

namespace TUMaster.Core.Data
{
	public class ServiceWarEventInfo : ServiceData
	{
		public int FactionId { get; set; }
		public int WarEventId { get; set; }
		public int WarPoints { get; set; }
		public int Wins { get; set; }
		public int Loses { get; set; }
		public int WinStreak { get; set; }
		public int Rank { get; set; }
		
		public ServiceWarFactionMember CurrentMember { get; set; }

		public List<ServiceWarFactionMember> FactionMembers { get; set; }

		public ServiceWarEventInfo(JsonObject json)
			: base(json)
		{
		}

		protected override void Map(JsonObject json)
		{
			WarEventId = json.GetOrDefault("war_event_id", 0);
			FactionId = json.GetOrDefault("faction_id", 0);
			WarPoints = json.GetOrDefault("faction_war_points", 0);
			Loses = json.GetOrDefault("losses", 0);
			Wins = json.GetOrDefault("wins", 0);
			WinStreak = json.GetOrDefault("win_streak", 0);
			Rank = json.GetOrDefault("our_rank", 0);
			
			FactionMembers = json.GetOrDefault<JsonArrayObjects>("members", null)?
				.ToList()?
				.ConvertAll(x => new ServiceWarFactionMember(x));
			
			CurrentMember = json.GetOrDefault<JsonObject>("user_info", null).With(x => new ServiceWarFactionMember(x));
		}
	}
}
