﻿using System;
using ServiceStack.Text;

namespace TUMaster.Core.Data
{
	public class ServiceWarFactionMember : ServiceData
	{
		public ServiceWarFactionMember(JsonObject obj) : base(obj)
		{
		}

		public int MemberId { get; set; }

		public string MemberName { get; set; }

		public int Wins { get; set; }

		public int Losses { get; set; }

		public int CurrentWarPoints { get; set; }

		public int FactionWarPoints { get; set; }

		public int WinStreak { get; set; }

		public int BattleEnergy { get; set; }

		public bool IsRewardsNotClaimed { get; set; }

		protected override void Map(JsonObject json)
		{
			MemberId = json.GetOrDefault("member_id", 0);
			MemberName = json.GetOrDefault("member_name", string.Empty);
			Wins = json.GetOrDefault("wins", 0);
			WinStreak = json.GetOrDefault("win_streak", 0);
			Losses = json.GetOrDefault("losses", 0);
			CurrentWarPoints = json.GetOrDefault("current_war_points", 0);
			FactionWarPoints = json.GetOrDefault("faction_war_points", 0);
			BattleEnergy = json.GetOrDefault("battle_energy", 0);
			IsRewardsNotClaimed = json.GetOrDefault("claimed_rewards", -1) == 0;
		}
	}
}
