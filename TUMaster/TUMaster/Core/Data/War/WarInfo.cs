﻿using ServiceStack.Text;
using System;

namespace TUMaster.Core.Data
{
	public class WarInfo : GameServiceResponse
	{
		public ServiceActiveWarEventInfo ActiveEventInfo { get; private set; }

		public ServiceWarData WarData { get; private set; }

		public ServiceWarEventInfo WarEventInfo { get; private set; }

		public bool IsRewardsTime { get; private set; }

		public override void Map(JsonObject json)
		{
			base.Map(json);
			MapData(json);
		}

		private void MapData(JsonObject json)
		{
			if (json.ContainsKey("faction_war"))
			{
				WarData = new ServiceWarData(json.GetOrDefault<JsonObject>("faction_war", null));
			}

			if (json.ContainsKey("faction_war_event_info"))
			{
				WarEventInfo = new ServiceWarEventInfo(json.GetOrDefault<JsonObject>("faction_war_event_info", null));
			}

			if (json.ContainsKey("active_war_event"))
			{
				ActiveEventInfo = new ServiceActiveWarEventInfo(json.GetOrDefault<JsonObject>("active_war_event", null));
				ActiveEventInfo.With(x => IsRewardsTime = Response.CurrentTime < x.DecayTime && Response.CurrentTime > x.EndTime);
			}
		}
	}
}
