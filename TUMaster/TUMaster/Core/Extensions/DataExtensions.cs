﻿using ServiceStack.Text;

namespace System
{
	public static class DataExtensions
  {
		#region CastExtensions

		public static int ToInt(this string val) => int.TryParse(val, out var res) ? res : 0;

		#endregion

		#region JsonObject extensions
		
		public static T GetOrDefault<T>(this JsonObject obj, string key, T defaultValue = default(T))
    {
			return (obj != null && obj.ContainsKey(key)) ? obj.Get<T>(key) : defaultValue;
		}

		#endregion

		#region String extensions

		public static bool Contains(this string source, string target, StringComparison comp) => source.IndexOf(target, comp) >= 0;
    
    #endregion
  }
}
