﻿using System.Collections.Generic;
using System.Linq;

namespace System.Reflection
{
	public static class ReflectionExtensions
	{
		#region Constants reflection

		public static IEnumerable<TResult> GetAllConstants<TResult>(this Type reflectType)
		{
			var constants = reflectType.GetFields(BindingFlags.Public | BindingFlags.Static | BindingFlags.FlattenHierarchy);
			return from fi in constants
						 where fi.IsLiteral && !fi.IsInitOnly && fi.FieldType == typeof(TResult)
						 select (TResult)fi.GetRawConstantValue();
		}

		public static IEnumerable<TResult> GetAllConstants<TResult>(this Type reflectType, Func<TResult, bool> predicate)
		{
			return reflectType.GetAllConstants<TResult>().Where(predicate);
		}

		#endregion
	}
}
