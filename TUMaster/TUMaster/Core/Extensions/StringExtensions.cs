﻿using System.Globalization;

namespace System
{
	public static class StringExtensions
	{
		public static string UpperFirst(this string str)
			=> CultureInfo.CurrentCulture.TextInfo.ToTitleCase(str.ToLower());
	}
}
