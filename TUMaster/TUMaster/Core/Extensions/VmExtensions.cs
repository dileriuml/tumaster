﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Threading;
using TUMaster.VM;

namespace System
{
	public static class VmExtensions
	{
		#region Property Changed extensions

		public static void OnPropertyChangedWithDispatcher<T, TProperty>(this T observableBase, Expression<Func<T, TProperty>> expression)
				where T : VmBase
		{
			Dispatcher.CurrentDispatcher.Invoke(() => observableBase.RaisePropertyChanged(GetPropertyName(expression)), DispatcherPriority.Input);
		}

		public static string GetPropertyName<T, TProperty>(Expression<Func<T, TProperty>> expression)
			where T : INotifyPropertyChanged
		{
			if (expression == null)
				throw new ArgumentNullException("expression");

			var memberExpression = expression.Body as MemberExpression;

			if (memberExpression == null)
				throw new ArgumentNullException("expression");

			return memberExpression.Member.Name;
		}

		#endregion

		#region Observable collection extensions

		public static void AddUsingDispatcher<T>(this ObservableCollection<T> collection, IEnumerable<T> newItems) where T : class
		{
			Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => newItems.ForEach(item => collection.Add(item))));
		}

		public static void AddUsingDispatcher<T>(this ObservableCollection<T> collection, T newItem)
		{
			Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => collection.Add(newItem)));
		}

		public static async Task RemoveUsingDispatcherAsync<T>(this ObservableCollection<T> collection, T newItem)
		{
			await Application.Current.Dispatcher.BeginInvoke(DispatcherPriority.Input, new Action(() => collection.Remove(newItem)));
		}

		public static void ClearUsingDispatcher<T>(this ObservableCollection<T> collection)
		{
			Application.Current.Dispatcher.Invoke(DispatcherPriority.Input, new Action(collection.Clear));
		}

		#endregion
	}
}
