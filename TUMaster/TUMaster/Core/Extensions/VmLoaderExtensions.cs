﻿using MvvmCross.ViewModels;

namespace TUMaster.Core.Extensions
{
	public static class VmLoaderExtensions
	{
		public static TVm LoadVm<TVm, TParameter>(this IMvxViewModelLoader vmLoader, TParameter parameter)
			where TVm : IMvxViewModel =>
			(TVm)vmLoader.LoadViewModel(new MvxViewModelRequest(typeof(TVm)), parameter, null);

		public static TVm LoadVm<TVm>(this IMvxViewModelLoader vmLoader)
			where TVm : IMvxViewModel =>
			(TVm)vmLoader.LoadViewModel(new MvxViewModelRequest(typeof(TVm)), null);
	}
}
