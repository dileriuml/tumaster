﻿namespace TUMaster.Core
{
	/// <summary>
	/// Description of ICancelationHost.
	/// </summary>
	public interface ICancelationHost
	{
		bool CheckActionIsCanceled();
	}
}
