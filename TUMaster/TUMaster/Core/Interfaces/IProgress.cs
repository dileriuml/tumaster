﻿namespace TUMaster.Core
{
	public interface ILongProgress : ICancelationHost
	{
		int Progress { get; set; }
		int MaxProgress { get; set; }
		string Message { get; set; }
		bool IsStopped { get; }
	}
}
