﻿using System.Threading.Tasks;
using TUMaster.Core.Services;

namespace TUMaster.Core.Network
{
	public enum ConnectionStatus
	{
		None,
		Offline,
		Online
	}

	public delegate void ConnectionStatusChangedHandler(ConnectionStatus status);

	public class ConnectionManager
	{
		bool isCanceled;

		public ConnectionManager(SynapseServer synapseServer)
		{
			ConnectionStatus = ConnectionStatus.Offline;
			LaunchConnectionCheckingAsync();
		}

		#region IConnectionManager implementation

		public event ConnectionStatusChangedHandler ConnectionStatusChanged;

		public ConnectionStatus ConnectionStatus { get; set; }

		#endregion

		async void LaunchConnectionCheckingAsync()
		{
			while (!isCanceled)
			{
				var newStatus = ConvertToStatus(await SynapseServer.CheckServerIsOnlineAsync());

				if (ConnectionStatus != newStatus)
				{
					ConnectionStatus = newStatus;

					ConnectionStatusChanged?.Invoke(newStatus);
				}

				await Task.Delay(10000);
			}
		}

		ConnectionStatus ConvertToStatus(int statusCode)
		{
			switch (statusCode)
			{
				case 0:
					return ConnectionStatus.Offline;
				case 1:
					return ConnectionStatus.Online;
				default:
					return ConnectionStatus.None;
			}
		}

		public void Cancel()
		{
			isCanceled = true;
		}
	}
}
