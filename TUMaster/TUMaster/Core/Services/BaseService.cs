﻿using System.Threading.Tasks;
using MvvmCross;
using ServiceStack.Text;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class BaseService
	{
		readonly SynapseServer server;
		protected readonly UsersCredsManager usersManager;

		public BaseService()
		{
			this.server = Mvx.Resolve<SynapseServer>();
			this.usersManager = Mvx.Resolve<UsersCredsManager>();
		}

		protected async Task<APIResponse> ExecuteEmptyRequestAsync(User user)
			=> await server.CallAPIWithEmptyMessageAsync(user);

		protected async Task<APIResponse> ExecuteEmptyRequestAsync(string userId)
			=> await server.CallAPIWithEmptyMessageAsync(usersManager.GetUserById(userId));

		protected async Task<APIResponse> ExecuteRequestAsync(string request, User user)
			=> await server.CallAPIWithResponseAsync(request, user);

		protected async Task<T> ExecuteGenericRequestAsync<T>(string request, string userId)
			where T : GameServiceResponse, new()
			=> await server.CallAPIWithGenericResponseAsync<T>(request, usersManager.GetUserById(userId));

		protected async Task<APIResponse> ExecuteRequestAsync(string request, string userId)
			=> await server.CallAPIWithResponseAsync(request, usersManager.GetUserById(userId));
		
		protected async Task<string> ExecuteRawRequestAsync(string request, string userId)
			=> await server.CallAPIAsync(request, usersManager.GetUserById(userId));

		protected async Task<JsonObject> ExecuteRequestToJsonAsync(string request, string userId)
			=> await server.CallAPIWithResponseToJsonAsync(request, usersManager.GetUserById(userId));

		protected async Task<APIResponse> GetEmptyResponseAsync(string userId)
		{
			return await ExecuteRequestAsync(string.Empty, userId);
		}

		public async Task<long> GetServerTimeAsync(string userId)
		{
			var emptyResponse = await GetEmptyResponseAsync(userId);
			return emptyResponse.ServerTime;
		}
	}
}
