﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class BattleServiceBase : BaseService
	{
		protected async Task<APIResponse> SkipBattleAsync(string userId)
		{
			var request = string.Format(RequestMessages.PlayCardSkipMessage);
			return await ExecuteRequestAsync(request, userId);
		}

		protected async Task<ServiceCombatData> GetBattleResultAsync(string userId, int tries = 10)
		{
			for (int i = 0; i < tries; i++)
			{
				var result = await ExecuteRequestAsync(RequestMessages.GetBattleResult, userId);
				var battleData = result.BattleData;

				if (battleData != null)
					return battleData;
			}

			return null;
		}
	}
}
