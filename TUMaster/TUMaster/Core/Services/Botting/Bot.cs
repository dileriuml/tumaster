﻿using System;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Brawls;
using TUMaster.Core.Data.Quests;
using TUMaster.Core.Network;
using TUMaster.Core.Services.Botting;
using TUMaster.Core.Xml;
using TUMaster.Utils;
using TUMaster.VM;

namespace TUMaster.Core.Services
{
	public class Bot : ICancelationHost
	{
		#region Constants

		public const double DowntimeIterationsCount = 10;

		#endregion

		#region Fields

		private bool isNoConnection;
		private ServiceUserData userAccountData;
		private ServiceBrawlsData brawlsData;
		private ServiceQuestInfo[] guildQuests;

		#endregion

		#region Services

		private readonly ProfileService profileService;
		private readonly UsersCache usersCache;
		private readonly PvpBattleService battleService;
		private readonly FactionService factionService;
		private readonly MissionsService missionsService;
		private readonly XmlGameDb xmlGameDb;
		private readonly UsersCredsManager credsManager;

		#endregion

		#region Properties

		public string UserId { get; }
		public BotStatus BotStatus { get; private set; } = BotStatus.Stopped;
		public BotBlockingStatus BlockingStatus { get; private set; }
		public double DowntimeLeft { get; private set; }
		public long LoadedTimeLeftForDaily { get; private set; }
		public bool HuntingHardMode { get; set; }
		public bool IsHuntingEnabled { get; set; }
		public bool IsMissionsEnabled { get; set; }
		public bool IsGuildQuestsEnabled { get; set; }
		public bool IsDailyBonusEnabled { get; set; }
		public bool IsBrawlsSmartCapIsEnabled { get; set; }
		public bool IsEventMissionsEnabled { get; set; }
		public bool IsUsesRefills { get; set; }
		public int BrawlsCustomCap { get; set; }
		public EnergyConfiguration EnergyConfig { get; private set; }

		public bool IsCancelledOrStopped => BotStatus != BotStatus.Working || BlockingStatus != BotBlockingStatus.NotBlocked;

		public event EventHandler<string[]> BotInfoUpdated;

		#endregion

		#region Constructor

		public Bot(string userId,
			UsersCache usersCache,
			ProfileService profileService,
			PvpBattleService battleService,
			MissionsService missionsService,
			FactionService factionService,
			UsersCredsManager credsManager,
			XmlGameDb xmlGameDb,
			ConnectionManager connectionManager)
		{
			UserId = userId;
			this.profileService = profileService;
			this.usersCache = usersCache;
			this.battleService = battleService;
			this.missionsService = missionsService;
			this.factionService = factionService;
			this.xmlGameDb = xmlGameDb;
			this.credsManager = credsManager;
			connectionManager.ConnectionStatusChanged += ConnectionStatusChanged;
			usersCache.AddUpdateHandler(userId, OnUserDataUpdated);
			userAccountData = usersCache.GetCurrentData(userId);
			ConfigureBot();
		}

		private async void OnUserDataUpdated(AccountResponse response)
		{
			if (response == AccountResponse.Success)
			{
				userAccountData = usersCache.GetCurrentData(UserId);
				brawlsData = usersCache.GetCurrentBrawlsData(UserId);
				guildQuests = usersCache.GetCurrentGuildQuests(UserId);
				BlockingStatus = userAccountData.Flags.SoundOn ? BotBlockingStatus.NotBlocked : BotBlockingStatus.Blocked;
				RaiseBotInfoUpdated(nameof(BlockingStatus));
				await UpdateTimeAsync();
			}
			else
			{

			}
		}

		#endregion

		private async Task UpdateTimeAsync()
		{
			await profileService.GetServerTimeAsync(UserId);
			LoadedTimeLeftForDaily = await profileService.GetLeftTimeForDailyBonus(UserId);
			RaiseBotInfoUpdated(nameof(LoadedTimeLeftForDaily));
		}

		private async Task RunBotAsync()
		{
			while (true)
			{
				if (BlockingStatus != BotBlockingStatus.Blocked)
					await BotOneTimeAsync();

				do
				{
					for (DowntimeLeft = DowntimeIterationsCount; DowntimeLeft > 0; DowntimeLeft--)
					{
						RaiseBotInfoUpdated(nameof(DowntimeLeft));

						if (BotStatus == BotStatus.Stopping)
						{
							BotStatus = BotStatus.Stopped;
							DowntimeLeft = 0;
							RaiseBotInfoUpdated(nameof(DowntimeLeft), nameof(BotStatus));
							return;
						}

						if (BlockingStatus == BotBlockingStatus.Blocked)
						{
							await Task.Delay(2000);

							try
							{
								await ReloadUserDataAsync();
							}
							catch (Exception er)
							{
								Log.Message(er.Message);
							}
						}

						await Task.Delay(1000);
					}
				} while (isNoConnection);
			}
		}

		private async Task BotOneTimeAsync()
		{
			try
			{
				if (IsCancelledOrStopped)
					return;

				if (IsDailyBonusEnabled)
				{
					await profileService.UseDailyBonusAsync(UserId);
				}

				if (IsHuntingEnabled)
				{
					await battleService.HuntTillStaminaAsync(userAccountData, true, this, ReloadUserDataAsync);
				}

				var anySpecialMissionsPlayed = false;

				if (IsGuildQuestsEnabled)
				{
					anySpecialMissionsPlayed = await PlayGuildQuest();
				}

				if (IsMissionsEnabled && !IsGuildQuestsEnabled && !anySpecialMissionsPlayed)
				{
					await PlayMissionSmartAsync();
				}

				while (IsBrawlsSmartCapIsEnabled && brawlsData.BattleEnergy >= BrawlsCustomCap)
				{
					await battleService.FightBrawlBattleAsync(userAccountData.UserID);
					await ReloadUserDataAsync();
					await Task.Delay(3000);
				}

				await ReloadUserDataAsync();
			}
			catch (OperationCanceledException)
			{
				return;
			}
			catch (Exception e)
			{
				Log.Message($"User : {userAccountData.Name}\n Message : {e.Message}");
			}
		}

		private async Task<bool> PlayGuildQuest()
		{
			if (CheckActionIsCanceled())
				return false;

			var availableQuest = guildQuests.FirstOrDefault(x => !x.Completed && userAccountData.Energy > x.Energy);

			if (availableQuest == null)
				return false;
			
			return await factionService.DoGuildQuest(UserId, availableQuest);
		}

		private async Task PlayMissionSmartAsync()
		{
			if (CheckActionIsCanceled())
				return;

			int currentMissionId = 0;
			int highestNumber = 10;

			var allPlayableMissions = xmlGameDb.MissionsDb.Missions.Where(x => x.Require != -1);
			var statMissions = (await missionsService.GetMissionCompletionsAsync(userAccountData.UserID)).MissionCompletions;

			var completeAllMissions = allPlayableMissions.Select(x => new
			{
				Mission = x,
				MissionStat = statMissions.FirstOrDefault(y => y.Id == x.Id) ?? new ServiceMissionCompletionData { Id = x.Id }
			}).ToList();

			var mutantMissions = completeAllMissions.Where(x => x.Mission.Name.Contains("mutant", StringComparison.InvariantCultureIgnoreCase));
			var simpleMissions = completeAllMissions.Except(mutantMissions).Where(x => x.Mission.EventReq == 0 && x.Mission.Id != 107).ToArray();
			var eventMissions = completeAllMissions.Where(x => x.Mission.EventReq == completeAllMissions.Max(y => y.Mission.EventReq)).ToArray();

			if (IsEventMissionsEnabled && eventMissions.Any())
			{
				var noncompletedmission = eventMissions.FirstOrDefault(x => x.MissionStat.CompleteCount == 0);

				if (noncompletedmission != null)
				{
					currentMissionId = noncompletedmission.Mission.Id;
				}
				else
				{
					for (int i = 6; i <= eventMissions.Count(); i += 6)
					{
						eventMissions.ElementAtOrDefault(i - 1).With(x =>
						{
							if (x.MissionStat.CompleteCount < 50)
							{
								currentMissionId = x.Mission.Id;
								highestNumber = 50;
							}
						});
					}
				}

				if (currentMissionId == 0)
				{
					eventMissions.FirstOrDefault(x => x.MissionStat.CompleteCount < 10).With(x => currentMissionId = x.Mission.Id);
				}
			}

			if (currentMissionId == 0)
			{
				var notCompletedBaseMission = simpleMissions.FirstOrDefault(x => x.MissionStat.CompleteCount < 10);
				var bestProfitMission = completeAllMissions.OrderByDescending(x => x.Mission.Profit).FirstOrDefault();
				currentMissionId = notCompletedBaseMission != null ? notCompletedBaseMission.Mission.Id : bestProfitMission.Mission.Id;
			}

			while (true)
			{
				if (CheckActionIsCanceled())
					return;

				var res = await missionsService.FightMissionAsync(currentMissionId, userAccountData.UserID);
				await ReloadUserDataAsync();

				if (!res.Response.Result
						|| res.MissionCompletions == null
						|| res.MissionCompletions.FirstOrDefault(x => x.Id == currentMissionId).CompleteCount >= highestNumber)
				{
					break;
				}

				await Task.Delay(2000);
			}
		}

		private async Task ReloadUserDataAsync() => userAccountData = await usersCache.LoadUser(UserId);

		private void RaiseBotInfoUpdated(params string[] props) => BotInfoUpdated?.Invoke(this, props);

		private void ConfigureBot()
		{
			var savedConfig = 
				credsManager.GetUserById(UserId).BotConfiguration
				?? new BotConfiguration();

			ApplyConfiguration(savedConfig);
		}

		private void ApplyConfiguration(BotConfiguration config)
		{
			HuntingHardMode = config.HuntingHardMode;
			IsHuntingEnabled = config.IsHuntingEnabled;
			IsMissionsEnabled = config.IsMissionsEnabled;
			IsGuildQuestsEnabled = config.IsGuildQuestsEnabled;
			IsDailyBonusEnabled = config.IsDailyBonusEnabled;
			IsBrawlsSmartCapIsEnabled = config.IsBrawlsSmartCapIsEnabled;
			IsEventMissionsEnabled = config.IsEventMissionsPlayable;
			IsUsesRefills = config.IsUsesRefills;
			BrawlsCustomCap = config.BrawlsCustomCap;
			EnergyConfig = config.EnergyConfiguration;
		}

		#region Connection handler

		private void ConnectionStatusChanged(ConnectionStatus status)
		{
			switch (status)
			{
				case ConnectionStatus.Online:
					isNoConnection = false;
					break;
				case ConnectionStatus.Offline:
					isNoConnection = true;
					break;
			}
		}

		#endregion

		#region Cancellation host implementation

		public bool CheckActionIsCanceled() => IsCancelledOrStopped;

		#endregion

		#region Public methods

		public async Task UnblockAsync()
		{
			if (BlockingStatus == BotBlockingStatus.Blocked)
			{
				BlockingStatus = BotBlockingStatus.Unblocking;
				RaiseBotInfoUpdated(nameof(BlockingStatus));
				await profileService.SetUserFlag(UserId, Flags.SOUND, 1);
				await ReloadUserDataAsync();
			}
		}

		public async Task BlockAsync()
		{
			if (BlockingStatus == BotBlockingStatus.NotBlocked)
			{
				BlockingStatus = BotBlockingStatus.Blocked;
				RaiseBotInfoUpdated(nameof(BlockingStatus));
				await profileService.SetUserFlag(UserId, Flags.SOUND, 0);
				await ReloadUserDataAsync();
			}
		}

		public void ForceStopBot()
		{
			BotStatus = BotStatus.Stopping;
			RaiseBotInfoUpdated(nameof(BotStatus));
		}

		public async Task TryRunBot()
		{
			if (BotStatus == BotStatus.Stopped)
			{
				BotStatus = BotStatus.Working;
				RaiseBotInfoUpdated(nameof(BotStatus));
				await RunBotAsync();
			}
		}

		public async void RunPause()
		{
			switch (BotStatus)
			{
				case BotStatus.Stopped:
					await TryRunBot();
					break;
				case BotStatus.Working:
					ForceStopBot();
					break;
			}
		}

		public void SetConfigurationToUser()
		{
			
		}

		#endregion
	}
}
