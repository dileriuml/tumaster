﻿namespace TUMaster.Core.Services.Botting
{
	public class BotConfiguration
	{
		public bool HuntingHardMode { get; set; } = true;
		public bool IsHuntingEnabled { get; set; } = true;
		public bool IsMissionsEnabled { get; set; } = true;
		public bool IsDailyBonusEnabled { get; set; } = true;
		public bool IsGuildQuestsEnabled { get; set; } = true;
		public bool IsBrawlsSmartCapIsEnabled { get; set; } = false;
		public bool IsEventMissionsPlayable { get; set; }
		public bool IsUsesRefills { get; set; } = false;
		public int BrawlsCustomCap { get; set; }
		public EnergyConfiguration EnergyConfiguration { get; set; }

		public BotConfiguration()
		{
		}

		public static BotConfiguration ProduceFromBot(Bot bot)
			=>
			new BotConfiguration
			{
				HuntingHardMode = bot.HuntingHardMode,
				IsHuntingEnabled = bot.IsHuntingEnabled,
				IsMissionsEnabled = bot.IsMissionsEnabled,
				IsDailyBonusEnabled = bot.IsDailyBonusEnabled,
				IsBrawlsSmartCapIsEnabled = bot.IsBrawlsSmartCapIsEnabled,
				IsGuildQuestsEnabled = bot.IsGuildQuestsEnabled,
				IsEventMissionsPlayable = bot.IsEventMissionsEnabled,
				IsUsesRefills = bot.IsUsesRefills,
				BrawlsCustomCap = bot.BrawlsCustomCap,
				EnergyConfiguration = bot.EnergyConfig
			};
	}
}
