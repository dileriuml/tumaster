﻿using System;
using System.Collections.Generic;
using System.Linq;
using TUMaster.Core.Network;
using TUMaster.Core.Xml;

namespace TUMaster.Core.Services
{
	public class BotManager
	{
		#region Private fields

		List<Bot> Bots { get; }

		#endregion

		#region Fields

		readonly MissionsService missionsService;
		readonly PvpBattleService battleService;
		readonly FactionService factionService;
		readonly XmlGameDb xmlGameDb;
		readonly UsersCache usersCache;
		readonly ProfileService profileService;
		readonly UsersCredsManager credsManager;
		readonly ConnectionManager connectionManager;

		#endregion

		#region Constructor

		public BotManager(ConnectionManager connectionManager,
			MissionsService missionsService,
			PvpBattleService battleService,
			ProfileService profileService,
			FactionService factionService,
			XmlGameDb xmlGameDb,
			UsersCache usersCache,
			UsersCredsManager credsManager)
		{
			this.missionsService = missionsService;
			this.battleService = battleService;
			this.factionService = factionService;
			this.xmlGameDb = xmlGameDb;
			this.usersCache = usersCache;
			this.credsManager = credsManager;
			this.profileService = profileService;
			this.connectionManager = connectionManager;
			Bots = new List<Bot>();
		}

		#endregion

		#region Config methods

		//NOTE: Useless method, for future
		public void Initialize()
		{
			var users = credsManager.GetUsers();
			users
				.Where(x => usersCache.ContainsUser(x.UserId))
				.Select(x => x.UserId)
				.ForEach(AddBot);
		}

		#endregion

		#region Public methods

		public Bot GetUserBot(string userId) => Bots.FirstOrDefault(x => x.UserId == userId);

		public void AddBot(string userId)
		{
			if (Bots.All(x => x.UserId != userId))
			{
				Bots.Add(CreateBot(userId));
			}
		}

		public void RemoveBot(string userId)
		{
			var bot = Bots.FirstOrDefault(x => x.UserId == userId);

			if (bot != null)
			{
				bot.ForceStopBot();
			}
		}

		public Bot[] AllBots => Bots.ToArray();

		#endregion

		#region Private methods

		private Bot CreateBot(string userId)
		{
			return new Bot(userId, 
				usersCache, 
				profileService, 
				battleService, 
				missionsService,
				factionService,
				credsManager, 
				xmlGameDb, 
				connectionManager);
		}

		#endregion
	}
}
