﻿namespace TUMaster.Core.Services.Botting
{
	public class EnergyConfigActionItem
	{
		public EnergySpendType EnergySpendType { get; set; }

		public int ItemId { get; set; }
	}
}
