﻿namespace TUMaster.Core.Services.Botting
{
	public class EnergyConfiguration
	{
		public MissionConfiguration MissionConfig { get; set; }

		public GuildQuestConfiguration GuildQuestConfig { get; set; }

		public CampaignConfiguration CampaignConfig { get; set; }

		public EnergySpendType[] SpendEnergyPriorityList { get; set; }

		public EnergyConfiguration()
		{
			MissionConfig = new MissionConfiguration();
			GuildQuestConfig = new GuildQuestConfiguration();
			CampaignConfig = new CampaignConfiguration();
			SpendEnergyPriorityList = new EnergySpendType[3];
		}

		public EnergyConfigActionItem ProduceCurrentConfigurationItem()
		{
			return new EnergyConfigActionItem();
		}

		public static EnergyConfiguration ProduceDefault()
		{
			return new EnergyConfiguration
			{
				SpendEnergyPriorityList = new []
				{
					EnergySpendType.Mission,
					EnergySpendType.GuildQuest,
					EnergySpendType.Campaign,
				}
			};
		}
	}
}
