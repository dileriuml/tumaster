﻿namespace TUMaster.Core.Services.Botting
{
	public class MissionPriorityInfo
	{
		public int Id { get; set; }

		public int CompletionLimit { get; set; }
	}
}
