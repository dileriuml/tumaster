﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class CampaignService : BaseService
	{
		public async Task<ServiceCampaignData> StartCampaign(string userId)
		{
			var campaignResponse = await ExecuteRequestAsync(RequestMessages.StartCampaignMessage, userId);
			return campaignResponse.CampaignData;
		}
	}
}
