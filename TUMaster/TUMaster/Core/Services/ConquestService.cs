﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data.Conquest;

namespace TUMaster.Core.Services
{
	public class ConquestService : BattleServiceBase
	{
		public async Task<ServiceConquestData> GetConquestDataAsync(string userId)
		{
			var conquestData = await ExecuteGenericRequestAsync<ServiceConquestData>(RequestMessages.GetConquestDataMessage, userId);
			return conquestData == null || string.IsNullOrEmpty(conquestData.Name) ? null : conquestData;
		}

		public async Task FightConquestBattleAsync(string userId, ZoneData inZone)
		{
			var requestMessage = string.Format(RequestMessages.FightConquestBattleMessageFormat, inZone.Id);
			await ExecuteRawRequestAsync(requestMessage, userId);
			await SkipBattleAsync(userId);
		}

		public async Task ClaimConquestRewards(string userId)
		{
			await ExecuteRequestAsync(RequestMessages.ClaimConquestRewardsMessage, userId);
		}
	}
}
