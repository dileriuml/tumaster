﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data.Faction;
using TUMaster.Core.Data.Quests;

namespace TUMaster.Core.Services
{
	public class FactionService : BattleServiceBase
	{
		public async Task<(ServiceQuestInfo[] guildQuests, FactionData factionData)> GetFactionInfo(string userId)
		{
			var response = await ExecuteRequestAsync(RequestMessages.UpdateFactionMessage, userId);
			return (response.GuildQuests, response.Faction);
		}

		public async Task<bool> DoGuildQuest(string userId, ServiceQuestInfo availableQuest)
		{
			var questMessageFormat = string.Format(RequestMessages.FightFactionQuestMessageFormat, availableQuest.Id);
			var response = await ExecuteRequestAsync(questMessageFormat, userId);
			var questResponse = await SkipBattleAsync(userId);
			return response?.Response?.Result ?? false;
		}
	}
}
