﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class InventoryService : BaseService
	{
		public async Task SalvageCardAsync(string cardId, string userId)
		{
			var request = string.Format(RequestMessages.SalvageMessageFormat, cardId);
			await ExecuteRequestAsync(request, userId);
		}

		public async Task<List<ServiceCardData>> GetInventoryAsync(ServiceUserData userData)
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetInventoryMessage, userData.UserID);
			return result.Cards;
		}

		public async Task<APIResponse> SalvageCardAsync(string userId, int cardId)
		{
			var request = string.Format(RequestMessages.SalvageCardMessageFormat, cardId);
			return await ExecuteRequestAsync(request, userId);
		}

		public async Task<APIResponse> UpgradeCardAsync(string userId, int cardId)
		{
			var request = string.Format(RequestMessages.UpgrageCardMessageFormat, cardId);
			return await ExecuteRequestAsync(request, userId);
		}

		public async Task SalvageAllCommonsAsync(string userId)
			=> await ExecuteRequestAsync(RequestMessages.SalvageAllCommonsMessage, userId);

		public async Task SalvageAllRaresAsync(string userId)
			=> await ExecuteRequestAsync(RequestMessages.SalvageAllRaresMessage, userId);

		public async Task<ServiceDeckData> GetUserDeck(string userId)
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetUserDeckMessage, userId);
			return result.Deck;
		}

		public async Task<bool> SetUserDeckAsync(ServiceDeckData deckData, string userId)
		{
			var cards = string.Join(",", deckData.Cards.Select(kp => string.Format("\"{0}\":\"{1}\"", kp.Key, kp.Value)));
			var message = string.Format(RequestMessages.SetUserDeckMessageFormat, cards, deckData.CommanderId);
			var result = await ExecuteRawRequestAsync(message, userId);
			return result.Contains("\"result\":true");
		}

		public async Task<bool> SetCardLock(int cardId, bool isLocked, string userId)
		{
			var message = string.Format(RequestMessages.SetCardLockMessageFormat, cardId, isLocked ? 1 : 0);
			var resp = await ExecuteRequestAsync(message, userId);
			return resp != null && resp.Response != null && resp.Response.Result;
		}
	}
}
