﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class MissionsService : BattleServiceBase
	{
		public async Task<APIResponse> GetMissionCompletionsAsync(string userId)
		{
			return await ExecuteRequestAsync(RequestMessages.GetMissionCompletions, userId);
		}

		public async Task<APIResponse> FightMissionAsync(int missionId, string userId)
		{
			var user = userId;
			var request = string.Format(RequestMessages.FightMissionMessageFormat, missionId);
			var res = await ExecuteRequestAsync(request, user);

			if (res.Response == null || !res.Response.Result)
				return res;

			return await SkipBattleAsync(user);
		}
	}
}
