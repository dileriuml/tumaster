﻿using System.Threading.Tasks;
using ServiceStack.Text;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public enum AccountResponse
	{
		Success,
		InvalidCredentials,
		Failed,
		Banned
	}
	public class ProfileService : BaseService
	{
		public async Task<JsonObject> GetUserAccountUnMappedAsync(string userId)
		{
			var result = await ExecuteRequestToJsonAsync(RequestMessages.GetProfileData, userId);
			return result.Get<JsonObject>(ServiceUserData.UserDataJsonKey);
		}

		public async Task<ServiceUserData> GetUserAccountAsync(string userId)
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetProfileData, userId);
			return result.UserData;
		}

		public async Task<ServiceUserData> GetTestUserAccountAsync()
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetProfileData, User.Empty);
			return result.UserData;
		}

		public async Task<string[]> GetCardFileNamesListAsync()
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetProfileData, User.Empty);
			return result.CardInfos.CardFileNames;
		}

		public async Task<ServiceUserData> CallInitAsync(string userId)
		{
			var result = await ExecuteRequestAsync(RequestMessages.InitMessage, userId);
			return result.UserData;
		}

		public async Task<AccountResponse> TryLogin(User user)
		{
			var emptyRequest = await ExecuteEmptyRequestAsync(user);

			if (emptyRequest.Response == null)
				return AccountResponse.Failed;

			if (emptyRequest.Banned)
				return AccountResponse.Banned;

			//Someway true means that there is invalid credentials
			if (emptyRequest.Response.Result == true)
				return AccountResponse.InvalidCredentials;

			return AccountResponse.Success;
		}

		public async Task<AccountResponse> TryGetUserAccountAsync(string userId, ServiceUserData responsedUserData)
		{
			var emptyRequest = await ExecuteEmptyRequestAsync(userId);

			if (emptyRequest.Response == null)
				return AccountResponse.Failed;

			if (emptyRequest.Banned)
				return AccountResponse.Banned;

			//Someway true means that there is invalid credentials
			if (emptyRequest.Response.Result == true)
				return AccountResponse.InvalidCredentials;
			
			responsedUserData.Remap(await GetUserAccountUnMappedAsync(userId));
			return AccountResponse.Success;
		}

		public async Task<APIResponse> SetUserFlag<T>(string userId, string flag, T value)
		{
			var request = string.Format(RequestMessages.FlagSettingMessageFormat, flag, value.ToString());
			return await ExecuteRequestAsync(request, userId);
		}

		public async Task<APIResponse> UseDailyBonusAsync(string userId)
		{
			var res = await ExecuteEmptyRequestAsync(userId);
			return res.IsDailyBonusReady ? await ExecuteRequestAsync(RequestMessages.UseDailyMessageBonus, userId) : null;
		}

		public string GetUserPassword(string userId) => usersManager.GetUserById(userId).Password;

		public async Task<User> GetTestUser()
		{
			var sdata = await GetTestUserAccountAsync();
			return new User { UserId = sdata.UserID, Password = sdata.UserKey, Syncode = sdata.SyncodeKey };
		}

		public async Task<long> GetLeftTimeForDailyBonus(string userId)
		{
			var response = await ExecuteEmptyRequestAsync(userId);
			return response.DailyBonusTime - response.ServerTime;
		} 
	}
}
