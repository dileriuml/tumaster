﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Brawls;

namespace TUMaster.Core.Services
{
	public class PvpBattleService : BattleServiceBase
	{
		public async Task<string> PerformHuntingAsync(string userId, ILongProgress progress)
		{
			var wins = 0;
			var goldGot = 0;
			var brGot = 0;
			var tries = 0;
			var targets = (await GetTargetsToHuntAsync(userId)).OrderBy(x => x.BattleRate).Select(x => x.UserId);

			for (progress.Progress = 0; progress.Progress < progress.MaxProgress; progress.Progress++)
			{
				if (progress.IsStopped)
					break;

				await HuntAsync(targets.ElementAt(progress.Progress), userId);
				var res = (await SkipBattleAsync(userId)).BattleData ?? (await GetBattleResultAsync(userId));

				if (res != null)
				{
					tries++;
					wins += res.IsWinner ? 1 : 0;

					res.Rewards.FirstOrDefault().With(x => {
						goldGot += x.Gold;
						brGot += x.TrophyRating;
					});
				}
			}

			return string.Format("Wins : {0} / , Gold : {1}, Br : {2}, Tries : {3}", wins, goldGot, brGot);
		}

		public async Task HuntTillStaminaAsync(ServiceUserData data, bool hard, ICancelationHost cancelactionHost, Func<Task> onEachBattle = null)
		{
			IEnumerator<int> targets = null;

			do
			{
				if (cancelactionHost.CheckActionIsCanceled())
				{
					throw new OperationCanceledException();
				}

				if (targets == null || !targets.MoveNext())
				{
					targets = await GetHuntingTargetsAsync(data.UserID, hard);
					targets.MoveNext();
				}

				var resp = (await HuntAsync(targets.Current, data.UserID)).Response;
				await SkipBattleAsync(data.UserID);
				
				if (resp == null || !resp.Result)
					break;

				if (onEachBattle != null)
				{
					await onEachBattle();
				}

				await Task.Delay(2500);
			} while (true);
		}

		public async Task BuyStaminaRefillAsync(string userId) => await ExecuteRequestAsync(RequestMessages.BuyStaminaRefill, userId);

		public async Task ClaimBrawlReward(string userId) => await ExecuteRequestAsync(RequestMessages.ClaimBrawlRewardMessage, userId);

		public async Task<List<ServiceHuntingTarget>> GetTargetsToHuntAsync(string userId)
		{
			var request = string.Format(RequestMessages.GetBattlesMessage);
			var result = await ExecuteRequestAsync(request, userId);
			return result.HuntingTargets;
		}

		public async Task<APIResponse> FightBrawlBattleAsync(string userId)
		{
			var resp = await ExecuteRawRequestAsync(RequestMessages.FightBrawlBattleMessage, userId);
			return await SkipBattleAsync(userId);
		}

		async Task<IEnumerator<int>> GetHuntingTargetsAsync(string userId, bool hard)
		{
			var targets = await GetTargetsToHuntAsync(userId);
			return (hard ? targets.OrderByDescending(x => x.BattleRate).AsEnumerable() : targets.AsEnumerable())
				.Select(x => x.UserId).GetEnumerator();
		}

		async Task<APIResponse> HuntAsync(long targetId, string userId)
		{
			var request = string.Format(RequestMessages.StartHuntingMessageFormat, targetId);
			return await ExecuteRequestAsync(request, userId);
		}

		public async Task<ServiceBrawlsData> GetBrawlsInfo(string userId)
		{
			var response = await ExecuteRequestAsync(RequestMessages.GetBrawlsMessage, userId);
			return response.UserBrawlData;
		}
	}
}