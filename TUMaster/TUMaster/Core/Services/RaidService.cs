﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class RaidService : BattleServiceBase
	{
		public async Task BuyRaidRefillAsync(string userId)
		{
			await ExecuteRequestAsync(RequestMessages.BuyEnergyRefill, userId);
		}

		public async Task<APIResponse> DoRaidBattleAsync(string userId, ServiceRaidStatusData data)
		{
			var request = string.Format(RequestMessages.StartRaidBattleMessageFormat, data.RaidId, data.Level);
			var raidResponse = await ExecuteRequestAsync(request, userId);
			await SkipBattleAsync(userId);
			return raidResponse;
		}

		public async Task<ServiceRaidStatusData> GetCurrentRaidInfoAsync(string userId)
		{
			var raidData = await ExecuteGenericRequestAsync<ServiceRaidStatusData>(RequestMessages.GetRaidInfoMessage, userId);
			return raidData.RaidId == 0 ? null : raidData;
		}

		public async Task ClaimRaidRewardAsync(string userId, int raidId)
		{
			var request = string.Format(RequestMessages.ClaimRaidRewardMessage, raidId);
			await ExecuteRequestAsync(request, userId);
		}
	}
}
