﻿using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Store;

namespace TUMaster.Core.Services
{
	public class StoreService : BaseService
	{
		private InventoryService inventoryService;

		public StoreService(InventoryService inventoryService)
		{
			this.inventoryService = inventoryService;
		}

		public async Task<ServiceStoreData> GetStoreInfoAsync(string userId)
		{
			var result = await ExecuteRequestAsync(RequestMessages.GetStoreInfoMessage, userId);
			return result.StoreData;
		}

		private async Task<BuyResultResponse> BuyPackAsync(string userId, string packMessageFormat, int packId)
		{
			return await ExecuteGenericRequestAsync<BuyResultResponse>(string.Format(packMessageFormat, packId), userId);
		}

		private async Task<BuyResultResponse> BuyPromoAsync(string userId, string packMessageFormat, int itemId, int itemType, int expectedCost)
		{
			return await ExecuteGenericRequestAsync<BuyResultResponse>(string.Format(packMessageFormat, itemId, itemType, expectedCost), userId);
		}

		public async Task<BuyResultResponse> BuyItemAsync(ServiceStoreItem storeItem, string userId)
		{
			switch (storeItem.GetType().Name)
			{
				case nameof(ServicePack):
					return await BuyItemAsync((ServicePack)storeItem, userId);
				case nameof(ServicePromo):
					return await BuyItemAsync((ServicePromo)storeItem, userId);
			}

			return null;
		}

		public async Task BuyOutItemAsync(ServiceStoreItem item, ServiceUserData userId, ILongProgress progress)
		{
			switch (item.GetType().Name)
			{
				case nameof(ServicePack):
					await BuyOutItemAsync((ServicePack)item, userId, progress);
					break;
				case nameof(ServicePromo):
					await BuyOutItemAsync((ServicePromo)item, userId, progress);
					break;
			}
		}

		public async Task<BuyResultResponse> BuyItemAsync(ServicePack pack, string userId)
		{
			string messageFormat = null;

			switch (pack.CurrencyType)
			{
				case PackCurrency.Gold:
					messageFormat = RequestMessages.BuyPackGoldMessageFormat;
					break;
				case PackCurrency.WB:
					messageFormat = RequestMessages.BuyPackTokensMessageFormat;
					break;
			}

			return await BuyPackAsync(userId, messageFormat, pack.PackId);
		}

		public async Task BuyOutItemAsync(ServicePack pack, ServiceUserData userData, ILongProgress progress)
		{
			switch (pack.CurrencyType)
			{
				case PackCurrency.Gold:
					progress.MaxProgress = userData.Money;
					break;
				case PackCurrency.WB:
					progress.MaxProgress = userData.Tokens;
					break;
			}

			while (progress.Progress < progress.MaxProgress)
			{
				if (progress.IsStopped)
					return;

				await BuyItemAsync(pack, userData.UserID);

				switch (pack.CurrencyType)
				{
					case PackCurrency.Gold:
						progress.Progress += pack.GoldCost;
						break;
					case PackCurrency.WB:
						progress.Progress += pack.WbCost;
						break;
				}
			}
		}

		public async Task<BuyResultResponse> BuyItemAsync(ServicePromo promoItem, string userId)
		{
			string messageFormat = null;
			int expectedCost = 0;

			switch (promoItem.CurrencyType)
			{
				case PackCurrency.Gold:
					messageFormat = RequestMessages.BuyPromoGoldMessageFormat;
					expectedCost = promoItem.GoldCost;
					break;
				case PackCurrency.WB:
					messageFormat = RequestMessages.BuyPromoTokenMessageFormat;
					expectedCost = promoItem.WbCost;
					break;
				default:
					return null;
			}

			return await BuyPromoAsync(userId, messageFormat, promoItem.Id, promoItem.ItemType, expectedCost);
		}

		public async Task BuyOutItemAsync(ServicePromo promoItem, ServiceUserData userData, ILongProgress progress)
		{
			if (promoItem.Items == null)
			{
				switch (promoItem.CurrencyType)
				{
					case PackCurrency.Gold:
						progress.MaxProgress = userData.Money / promoItem.GoldCost;
						break;
					case PackCurrency.WB:
						progress.MaxProgress = userData.Tokens / promoItem.WbCost;
						break;
				}
			}
			else
			{
				progress.MaxProgress = promoItem.Items.Sum(x => x.Count);
			}

			while (progress.Progress++ < progress.MaxProgress)
			{
				if (progress.IsStopped)
					return;

				var result = await BuyItemAsync(promoItem, userData.UserID);

				if (result.IsTooManyCards)
				{
					//TODO: Check sp to not overcap
					await inventoryService.SalvageAllCommonsAsync(userData.UserID);
				}
			}
		}

	}
}
