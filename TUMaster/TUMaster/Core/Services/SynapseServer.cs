﻿using System;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using ServiceStack.Text;
using TUMaster.Core.Data;
using TUMaster.Utils;

namespace TUMaster.Core.Services
{
	public class SynapseServer
	{
		const string SERVER_URL = "http://mobile.tyrantonline.com/";
		const string API_MESSAGE_FORMAT = "{0}api.php?message={1}&user_id={2}&password={3}&syncode={4}";

		readonly string url = SERVER_URL;
		
		public string FormatMessage(string message, IUser user)
		{
			return string.Format(API_MESSAGE_FORMAT, url, message, user.UserId, user.Password, user.Syncode);
		}

		public async Task<string> CallAPIAsync(string message, IUser user)
		{
			try
			{
				var request = WebRequest.CreateHttp(FormatMessage(message, user));
				request.Timeout = 5000;

				var webResponse = await request.GetResponseAsync();

				using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
				{
					return await streamReader.ReadToEndAsync();
				}
			}
			catch (Exception ex)
			{
				Log.Message(ex.Message);
			}

			return string.Empty;
		}

		public static async Task<int> CheckServerIsOnlineAsync()
		{
			try
			{
				var request = WebRequest.CreateHttp(SERVER_URL);
				request.Timeout = 2000;
				var webResponse = await request.GetResponseAsync();

				using (var streamReader = new StreamReader(webResponse.GetResponseStream()))
				{
					return streamReader.ReadToEnd() == "OK" ? 1 : 0;
				}
			}
			catch
			{
				return 0;
			}
		}

		public async Task<APIResponse> CallAPIWithEmptyMessageAsync(IUser user)
		{
			try
			{
				var jsonResult = JsonObject.Parse(await CallAPIAsync(string.Empty, user));
				return new APIResponse(jsonResult);
			}
			catch (Exception ex)
			{
				Log.Message(ex.Message);
			}

			return new APIResponse();
		}

		public async Task<JsonObject> CallAPIWithResponseToJsonAsync(string message, IUser user)
		{
			try
			{
				return JsonObject.Parse(await CallAPIAsync(message, user));
			}
			catch (Exception ex)
			{
				Log.Message(ex.Message);
			}

			return null;
		}

		public async Task<APIResponse> CallAPIWithResponseAsync(string message, IUser user)
		{
			try
			{
				return new APIResponse(await CallAPIWithResponseToJsonAsync(message, user));
			}
			catch (Exception ex)
			{
				Log.Message(ex.Message);
			}

			return new APIResponse();
		}

		public async Task<T> CallAPIWithGenericResponseAsync<T>(string message, IUser user)
			where T : GameServiceResponse, new()
		{
			try
			{
				var genericResponse = new T();
				var jsonResponse = await CallAPIWithResponseToJsonAsync(message, user);
				genericResponse.Map(jsonResponse);
				return genericResponse;
			}
			catch (Exception ex)
			{
				Log.Message(ex.Message);
			}

			return null;
		}
	}
}
