﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Brawls;
using TUMaster.Core.Data.Conquest;
using TUMaster.Core.Data.Quests;

namespace TUMaster.Core.Services
{
	public class UsersCache
	{
		readonly ProfileService profileService;
		readonly UsersCredsManager credsManager;
		readonly WarService warService;
		readonly FactionService factionService;
		readonly PvpBattleService battlesService;
		readonly ConquestService conquestService;
		readonly RaidService raidService;

		Dictionary<string, ServiceUserData> UserDatas { get; }
		Dictionary<string, WarInfo> UserWarDatas { get; }
		Dictionary<string, ServiceRaidStatusData> UserRaidDatas { get; }
		Dictionary<string, ServiceBrawlsData> UserBrawlDatas { get; }
		Dictionary<string, ServiceQuestInfo[]> GuildQuestDatas { get; }
		Dictionary<string, ServiceConquestData> UserConquestDatas { get; }
		Dictionary<string, Action<AccountResponse>> UserDataUpdateHandlers { get; }
		
		public UsersCache(
			ProfileService profileService, 
			UsersCredsManager credsManager, 
			WarService warService,
			FactionService factionService,
			ConquestService conquestService,
			PvpBattleService battlesService,
			RaidService raidService)
		{
			this.profileService = profileService;
			this.credsManager = credsManager;
			this.warService = warService;
			this.battlesService = battlesService;
			this.factionService = factionService;
			this.conquestService = conquestService;
			this.raidService = raidService;

			UserDatas = new Dictionary<string, ServiceUserData>();
			UserDataUpdateHandlers = new Dictionary<string, Action<AccountResponse>>();
			UserWarDatas = new Dictionary<string, WarInfo>();
			UserBrawlDatas = new Dictionary<string, ServiceBrawlsData>();
			UserConquestDatas = new Dictionary<string, ServiceConquestData>();
			UserRaidDatas = new Dictionary<string, ServiceRaidStatusData>();
			GuildQuestDatas = new Dictionary<string, ServiceQuestInfo[]>();
		}

		public bool ContainsUser(string userId) => UserDatas.ContainsKey(userId);

		public async Task<ServiceUserData> LoadUser(string userId, 
			bool reloadWarData = true,
			bool reloadBrawlsData = true,
			bool reloadConquestData = true, 
			bool reloadRaidData = true,
			bool reloadFactionQuests = true)
		{
			ServiceUserData userData = null;

			if (!UserDatas.ContainsKey(userId))
			{
				userData = new ServiceUserData();
			}
			else
			{
				userData = UserDatas[userId];
			}

			var result = await profileService.TryGetUserAccountAsync(userId, userData);

			if (result == AccountResponse.Success)
			{
				if (!UserDatas.ContainsKey(userId))
				{
					UserDatas[userId] = await profileService.CallInitAsync(userId);
				}
				else
				{
					UserDatas[userId] = userData;
				}

				if (reloadWarData)
				{
					UserWarDatas[userId] = await warService.GetCurrentWarInfoAsync(userId);
				}

				if (reloadBrawlsData)
				{
					UserBrawlDatas[userId] = await battlesService.GetBrawlsInfo(userId);
				}

				if (reloadConquestData)
				{
					UserConquestDatas[userId] = await conquestService.GetConquestDataAsync(userId);
				}

				if (reloadRaidData)
				{
					UserRaidDatas[userId] = await raidService.GetCurrentRaidInfoAsync(userId);
				}

				if (reloadFactionQuests)
				{
					var (guildQuests, factionData) = await factionService.GetFactionInfo(userId);
					GuildQuestDatas[userId] = guildQuests;
					UserDatas[userId].FactionName = factionData.Name;
				}
			}

			if (UserDataUpdateHandlers.TryGetValue(userId, out Action<AccountResponse> handlerForUpdate))
			{
				handlerForUpdate(result);
			}

			return userData;
		}

		public async Task LoadUsersFromManagerAsync()
		{
			var users = credsManager.GetUsers();
			var userLoadingTasks = users.Select(item => LoadUser(item.UserId));
			await Task.WhenAll(userLoadingTasks);
		}

		public void AddUpdateHandler(string userId, Action<AccountResponse> response)
		{
			if (!UserDataUpdateHandlers.ContainsKey(userId))
			{
				UserDataUpdateHandlers[userId] = delegate { }; // Empty delegate to avoid exceptions
			}

			UserDataUpdateHandlers[userId] += response;
		}

		public ServiceQuestInfo[] GetCurrentGuildQuests(string userId) => GuildQuestDatas[userId];

		public ServiceUserData GetCurrentData(string userId) => UserDatas[userId];

		public WarInfo GetCurrentWarData(string userId) => UserWarDatas[userId];

		public ServiceRaidStatusData GetCurrentRaidData(string userId) => UserRaidDatas[userId];

		public ServiceBrawlsData GetCurrentBrawlsData(string userId) => UserBrawlDatas[userId];

		public ServiceConquestData GetCurrentConquestData(string userId) => UserConquestDatas[userId];

		public void RemoveUser(string userId)
		{
			UserDatas.Remove(userId);
			UserDataUpdateHandlers.Remove(userId);
		}
	}
}
