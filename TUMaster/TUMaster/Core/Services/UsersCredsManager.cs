﻿using System;
using System.Collections.Generic;
using System.Linq;
using TUMaster.Core.Data;
using TUMaster.Core.Xml;

namespace TUMaster.Core.Services
{
	public class UsersCredsManager
	{
		#region Constants

		const string USERS_BD_FILENAME = "Users.xml";

		#endregion

		readonly XmlWorker xmlWorker;

		public UsersCredsManager(XmlWorker xmlWorker)
		{
			this.xmlWorker = xmlWorker;
			LoadUsers();
		}

		List<User> Users { get; set; }

		#region Public methods

		public User[] GetUsers() => Users.ToArray();

		public void LoadUsers() => Users = xmlWorker.Load<List<User>>(USERS_BD_FILENAME) ?? new List<User>();

		public void SaveUsers() => xmlWorker.Save(Users, USERS_BD_FILENAME);

		public void AddUser(User user) => Users.Add(user);

		public void RemoveUser(string userId) => Users.Remove(Users.FirstOrDefault(x => x.UserId == userId));

		public void RemoveAllUsers(Predicate<User> userRule) => Users.RemoveAll(userRule);

		public User GetUserById(string userId) => Users.FirstOrDefault(x => x.UserId == userId);

		#endregion
	}
}
