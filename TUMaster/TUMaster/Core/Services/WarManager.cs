﻿using System.Threading.Tasks;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class WarManager
	{
		private WarService warService;
		private UsersCache usersCache;

		public WarManager(WarService warService, UsersCache usersCache)
		{
			this.warService = warService;
			this.usersCache = usersCache;
		}

		public async Task<string> PerformBattlesAsync(ILongProgress la, Slots slotToAttack, string userId)
		{
			while (la.Progress < la.MaxProgress)
			{
				if (la.IsStopped)
					return la.Message;

				var resp = await warService.FightWarBattleAsync(slotToAttack, userId);

				var isFailed = false;

				try
				{
					if (!resp.Response.Result)
					{
						continue;
					}
					else
					{
						la.Message += resp.BattleData.IsWinner ? "Win!, " : "Lost!, ";
						la.Message += $"Slot : {slotToAttack}, ";

						var rewards = resp.BattleData.Rewards;

						if (rewards != null && rewards.Count > 0)
							la.Message += $"Damage : {rewards[0].SlotDamage}, Rate : {rewards[0].WarPoints}\n";
					}
				}
				catch
				{
					la.Message += "Failed!\n";
					isFailed = true;
				}

				la.Progress++;
				await usersCache.LoadUser(userId);
				await Task.Delay(3000);
				var currentWarData = usersCache.GetCurrentWarData(userId);

				if (currentWarData.WarData.Energy == 0)
				{
					la.Message += "Out of energy\n";
					isFailed = true;
				}
				else
				{
					la.Message += $"{currentWarData.WarData.Energy}\n";
				}

				if (la.IsStopped || isFailed)
					return la.Message;
			}

			return la.Message;
		}

		public async Task ClaimWarReward(string userId)
		{
			await warService.ClaimRewardAsync(userId);
		}
	}
}
