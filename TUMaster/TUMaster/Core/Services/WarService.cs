﻿using System.Threading.Tasks;
using TUMaster.Core.Constants;
using TUMaster.Core.Data;

namespace TUMaster.Core.Services
{
	public class WarService : BattleServiceBase
	{
		public async Task<APIResponse> FightWarBattleAsync(Slots slot, string userId)
		{
			var user = userId;
			var request = string.Format(RequestMessages.StartWarBattleMessageFormat, (int)slot);
			var response = await ExecuteRequestAsync(request, user);

			if (response.Response != null && !response.Response.Result)
				return response;

			return await SkipBattleAsync(user);
		}

		public async Task<WarInfo> GetCurrentWarInfoAsync(string userId)
		{
			var result = await ExecuteGenericRequestAsync<WarInfo>(RequestMessages.UpdateFactionWarMessage, userId);
			return result;
		}

		public async Task ClaimRewardAsync(string userId)
		{
			await ExecuteRawRequestAsync(RequestMessages.ClaimWarRewardMessage, userId);
		}
	}
}
