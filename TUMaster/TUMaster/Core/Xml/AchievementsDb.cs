﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TUMaster.Core.Xml.Models;

namespace TUMaster.Core.Xml
{
	public class AchievementsDb
	{
		[XmlElement("achievement")]
		public List<Achievement> Achievements { get; set; }

		public void Sort()
		{
			Achievements = Achievements.OrderBy(x => x.Id).ToList();
		}
	}
}
