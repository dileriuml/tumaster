﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TUMaster.Core.Xml.Models;
using TUMaster.Core.Xml.Models.Card;

namespace TUMaster.Core.Xml
{
	public class CardsDb
	{
		private int initialId;

		[XmlElement("cardSet")]
		public List<CardSet> Sets { get; set; }

		[XmlElement("skilltype")]
		public List<SkillType> Skills { get; set; }

		[XmlElement("unit")]
		public List<XmlCard> RawCards { get; set; }

		[XmlIgnore]
		public List<CardDTO> Cards { get; set; }

		public CardDTO GetCard(int id) => Cards.FirstOrDefault(x => x.Id == id);
		//{
		//	var card = ;

		//	if (card != null)
		//		return card;

		//	var res =
		//		from itemCard in Cards
		//		from upgrade in itemCard.Upgrades
		//		where upgrade.CardId == id
		//		select itemCard.OfLevel(upgrade.Level);

		//	return res.FirstOrDefault();
		//}

		public void SortAll()
		{
			RawCards = RawCards.OrderBy(x => x.Id).ToList();
			Sets = Sets.OrderBy(x => x.Id).ToList();
			Skills = Skills.OrderBy(x => x.Id).ToList();
			Cards = Cards.OrderBy(x => x.Id).ToList();
			initialId = Cards.FirstOrDefault()?.Id ?? 0;
		}

		public static CardsDb ProduceFinalDb(IEnumerable<CardsDb> cards)
		{
			var cardsDb = new CardsDb()
			{
				RawCards = cards.SelectMany(sm => sm.RawCards).ToList(),
				Sets = cards.SelectMany(sm => sm.Sets).ToList(),
				Skills = cards.SelectMany(sm => sm.Skills).ToList()
			};

			cardsDb.Cards = cardsDb.RawCards.SelectMany(x =>
			{
				var card = new CardDTO(x);
				var upgrades = new List<CardDTO> { card };
				upgrades.AddRange(card.Upgrades);
				return upgrades;
			}).ToList();
			cardsDb.SortAll();
			return cardsDb;
		}
	}
}
