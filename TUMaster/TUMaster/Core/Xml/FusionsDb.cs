﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TUMaster.Core.Xml.Models;

namespace TUMaster.Core.Xml
{
	public class FusionsDb
	{
		[XmlElement("fusion_recipe")]
		public List<Fusion> Fusions { get; set; }

		public Fusion GetFusionByCardId(int cardId) => Fusions.FirstOrDefault(fusion => fusion.Card == cardId);

		public void Sort()
		{
			Fusions = Fusions.OrderBy(x => x.Card).ToList();
		}
	}
}
