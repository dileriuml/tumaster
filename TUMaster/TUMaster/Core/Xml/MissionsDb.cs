﻿using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using TUMaster.Core.Xml.Models;

namespace TUMaster.Core.Xml
{
	public class MissionsDb
	{
		[XmlElement("mission")]
		public List<Mission> Missions { get; set; }

		public Mission GetMission(int id) => Missions.FirstOrDefault(mission => mission.Id == id);

		public void Sort()
		{
			Missions = Missions.OrderBy(x => x.Id).ToList();
		}
	}
}
