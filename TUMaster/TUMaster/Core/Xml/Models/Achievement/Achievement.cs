﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	/// <summary>
	/// Description of Achievement.
	/// </summary>
	[XmlRoot("achievement")]
	public class Achievement
	{
		[XmlElement("id")]
		public int Id { get; set; }

		[XmlElement("name")]
		public string Name { get; set; }

		[XmlElement("desc")]
		public string Description { get; set; }

		[XmlElement("reward")]
		public List<AchievementReward> Rewards { get; set; }

		public override string ToString()
		{
			return string.Format("[Achievement] :\n\tid = {0}\n\tname = {1}\n\tdesc = {2}\n\trewards:\n\t\t{3}\n\n",
													 Id, Name, Description, Rewards.CatElements("\n\t\t"));
		}
	}
}
