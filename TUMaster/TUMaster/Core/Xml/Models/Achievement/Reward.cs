﻿using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	/// <summary>
	/// Description of Reward.
	/// </summary>
	[XmlRoot("reward")]
	public class AchievementReward
	{
		[XmlElement("gold")]
		public int Gold { get; set; }

		[XmlElement("wb")]
		public int Wbs { get; set; }

		[XmlElement("sp")]
		public int SP { get; set; }

		public int GP { get; set; }

		[XmlElement("card")]
		public CardReward Card { get; set; }

		public override string ToString()
		{
			return $"[Reward Gold={Gold}, Wbs={Wbs}, Card={Card.Id}x{Card.Count}]";
		}
	}

	[XmlRoot("card")]
	public class CardReward
	{
		[XmlAttribute("id")]
		public int Id { get; set; }

		[XmlAttribute("amount")]
		public int Count { get; set; }
	}

	[XmlRoot("rewards")]
	public class MissionReward
	{
		#region IReward implementation

		[XmlElement("gold")]
		public int Gold { get; set; }

		[XmlElement("sp")]
		public int Sp { get; set; }

		[XmlElement("card")]
		public CardReward Card { get; set; }

		#endregion

		public override string ToString()
		{
			return string.Format("[Reward Gold={0}, CardId={1}]", Gold, Card);
		}
	}
}
