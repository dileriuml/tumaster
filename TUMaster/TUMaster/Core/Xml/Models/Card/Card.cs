﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TUMaster.Core.Xml.Models.Card
{
	public class CardDTO
	{
		private XmlCard cardInfo;
		private int attack;
		private CardDTO[] upgrades;

		#region Constructors

		public CardDTO()
		{
			Level = 1;
		}

		public CardDTO(XmlCard fromCard, bool isUpgrade = false) : this()
		{
			cardInfo = fromCard;
			attack = fromCard.Attack.ToInt();

			if (!isUpgrade)
			{
				ProduceUpgrades();
			}
		}

		#endregion

		#region Properties

		public int Id => cardInfo.Id;

		public string Name => cardInfo.Name;

		public int Health => cardInfo.Health;

		public int FusionLevel => cardInfo.FusionLevel;

		public int Attack => attack;

		public int Delay => cardInfo.Delay.ToInt();
		
		public bool IsCommander => (Id >= 1000 && Id < 2000) || (Id >= 25000 && Id < 30000);
		
		public bool IsDominion => Id >= 50080 && Id < 55000;
		
		public bool Upgradable => Level < MaxLevel;
		
		public string RarityStr => Rarity.ToString();

		public Rarity Rarity => (Rarity)cardInfo.Rarity;

		public int RarityId => cardInfo.Rarity;

		public int SPCost
		{
			get
			{
				if (IsCommander)
					return 0;

				switch (Rarity)
				{
					case Rarity.Common:
						return Level == 3 ? 5 : Level;

					case Rarity.Rare:
						return Level * 5;

					case Rarity.Epic:
						switch (Level)
						{
							case 1:
								return 20;
							case 2:
								return 25;
							case 3:
								return 30;
							case 4:
								return 40;
							case 5:
								return 50;
							case 6:
								return 65;
						}

						break;
					case Rarity.Legendary:
						switch (Level)
						{
							case 1:
								return 40;
							case 2:
								return 45;
							case 3:
								return 60;
							case 4:
								return 75;
							case 5:
								return 100;
							case 6:
								return 125;
						}

						break;
					case Rarity.Vindicator:
						switch (Level)
						{
							case 1:
								return 80;
							case 2:
								return 85;
							case 3:
								return 100;
							case 4:
								return 125;
							case 5:
								return 175;
							case 6:
								return 250;
						}

						break;
				}

				return 0;
			}
		}

		public Faction Faction => (Faction)cardInfo.Faction;

		public string FactionName => Faction.ToString();

		public int Level { get; set; }

		public int MaxLevel
		{
			get
			{
				switch (Rarity)
				{
					case Rarity.Common:
						return 3;
					case Rarity.Rare:
						return 4;
					default:
						return 6;
				}
			}
		}

		public Skill[] Skills => cardInfo.Skills.ToArray();

		public IEnumerable<CardDTO> Upgrades => upgrades;

		#endregion

		public static int UpgradeSpCost(int level, bool isCommander)
		{
			int spCost = 0;

			switch (level)
			{
				case 2:
					spCost = 5;
					break;
				case 3:
					spCost = 15;
					break;
				case 4:
					spCost = 30;
					break;
				case 5:
					spCost = 75;
					break;
				case 6:
					spCost = 150;
					break;
			}

			return spCost * (isCommander ? 2 : 1);
		}

		private CardDTO OfLevel(int level) => upgrades[level - 2];

		private void ProduceUpgrades()
		{
			upgrades = cardInfo.Upgrades.Select(x =>
				new CardDTO(cardInfo.WithUpgrade(x), true)
				{
					Level = x.Level
				}).ToArray();
		}

		public override string ToString() => $"{cardInfo.Name} Lv.{Level};\n\t{cardInfo.Skills.CatElements(";\n\t")}\n";
	}
}
