﻿using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("cardSet")]
	public class CardSet
	{
		[XmlElement("id")]
		public int Id { get; set; }

		[XmlElement("name")]
		public string Name { get; set; }
	}
}
