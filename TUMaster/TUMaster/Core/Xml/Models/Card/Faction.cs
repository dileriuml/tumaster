﻿namespace TUMaster.Core.Xml.Models
{
	public enum Faction
	{
		NoFaction,
		Imperial,
		Raider,
		Bloodthirsty,
		Xeno,
		Righteous,
		Progenitor
	}
}
