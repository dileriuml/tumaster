﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("fusion_recipe")]
	public class Fusion
	{
		[XmlElement("card_id")]
		public int Card { get; set; }

		[XmlElement("resource")]
		public List<Resource> Resources { get; set; }

		public override string ToString()
		{
			return string.Format("\nFusion Card={0}, Resources=\n\t{1}", Card, Resources.CatElements("\n\t"));
		}

		[XmlRoot("resource")]
		public class Resource
		{
			[XmlAttribute("card_id")]
			public int Card { get; set; }

			[XmlAttribute("number")]
			public int Count { get; set; }

			public override string ToString()
			{
				return string.Format("[Resource Card={0}, Count={1}]\n", Card, Count);
			}
		}
	}
}
