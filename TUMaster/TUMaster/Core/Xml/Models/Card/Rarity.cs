﻿namespace TUMaster.Core.Xml.Models
{
	public enum Rarity
	{
		Common = 1,
		Rare,
		Epic,
		Legendary,
		Vindicator,
		Mythic
	}
}
