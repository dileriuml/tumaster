﻿using System.Collections.Generic;
using System.Xml.Serialization;
using TUMaster.Core.Xml.Models.Card;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("unit")]
	public class XmlCard
	{
		[XmlElement("id")]
		public int Id { get; set; }

		[XmlElement("picture")]
		public string PictureName { get; set; }

		[XmlElement("name")]
		public string Name { get; set; }

		[XmlElement("cost")]
		public string Delay { get; set; }

		[XmlElement("rarity")]
		public int Rarity { get; set; }

		[XmlElement("type")]
		public int Faction { get; set; }

		[XmlElement("set")]
		public int Set { get; set; }

		[XmlElement("fusion_level")]
		public int FusionLevel { get; set; }

		[XmlElement("attack")]
		public string Attack { get; set; }

		[XmlElement("health")]
		public int Health { get; set; }

		[XmlElement("skill")]
		public List<Skill> Skills { get; set; }

		[XmlElement("upgrade")]
		public List<Upgrade> Upgrades { get; set; }

		public XmlCard WithUpgrade(Upgrade upgrade)
		{
			var xmlCard = MemberwiseClone() as XmlCard;
			xmlCard.ApplyUpgrade(upgrade);
			return xmlCard;
		}

		private void ApplyUpgrade(Upgrade upgrade)
		{
			Id = upgrade.CardId;
			
			if (upgrade.Health != Constants.DefaultHealth)
			{
				Health = upgrade.Health;
			}

			if (upgrade.AttackString != string.Empty)
			{
				Attack = upgrade.AttackString;
			}

			if (upgrade.Delay != string.Empty)
			{
				Delay = upgrade.Delay;
			}

			if (upgrade.Skills == null && upgrade.Skills.Count != 0)
			{
				Skills = upgrade.Skills;
			}
			else
			{
				for (int i = upgrade.Level - 1; i >= 2; i--)
				{
					var upgradeOfPrevLevel = Upgrades[i - 2];

					if (upgradeOfPrevLevel.Skills != null && upgradeOfPrevLevel.Skills.Count != 0)
					{
						Skills = upgradeOfPrevLevel.Skills;
						break;
					}
				}
			}
		}
	}
}
