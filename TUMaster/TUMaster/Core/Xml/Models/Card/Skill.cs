﻿using System;
using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("skill")]
	public class Skill : IEquatable<Skill>
	{
		[XmlAttribute("id")]
		public string Name { get; set; }

		[XmlAttribute("x")]
		public int Power { get; set; }

		[XmlAttribute("n")]
		public int Count { get; set; }

		[XmlAttribute("all")]
		public int WorksOnAll { get; set; }

		[XmlAttribute("y")]
		public int FactionId { get; set; }

		[XmlAttribute("c")]
		public int Cooldawn { get; set; }

		[XmlIgnore]
		public Faction Faction => (Faction)FactionId;

		[XmlIgnore]
		public string FactionName
			=> Faction == Faction.NoFaction ? string.Empty : Faction.ToString();

		[XmlAttribute("s")]
		public string SkillForEnhance { get; set; }

		[XmlAttribute("s2")]
		public string SkillForEvolve { get; set; }

		[XmlIgnore]
		public bool All => WorksOnAll == 1;

		#region IEquatable implementation

		public bool Equals(Skill other) => Name == other.Name;

		public override int GetHashCode() => Name == null ? 0 : Name.GetHashCode();

		#endregion

		public override string ToString()
		{
			var power = 0;

			if (Power != 0)
				power = Power;

			if (Cooldawn != 0)
				power = Cooldawn;

			if (Count != 0)
				power = Count;

			return $"{Name.UpperFirst()}{(All ? " All" : string.Empty)} {SkillForEnhance} {FactionName} {power}";
		}
	}
}
