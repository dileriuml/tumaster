﻿using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("skillType")]
	public class SkillType
	{
		[XmlElement("id")]
		public string Id { get; set; }

		[XmlElement("name")]
		public string Name { get; set; }

		[XmlElement("icon")]
		public string PictureName { get; set; }

		[XmlElement("desc")]
		public string Description { get; set; }
	}
}
