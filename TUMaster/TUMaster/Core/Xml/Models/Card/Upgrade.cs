﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models.Card
{
	[XmlRoot("upgrade")]
	public class Upgrade
	{
		[XmlElement("card_id")]
		public int CardId { get; set; }

		[XmlElement("level")]
		public int Level { get; set; }

		[XmlElement("attack")]
		public string AttackString { get; set; }

		[XmlElement("cost")]
		public string Delay { get; set; }

		[XmlIgnore]
		public int Attack => AttackString.ToInt();

		[XmlElement("health")]
		public int Health { get; set; }

		[XmlElement("skill")]
		public List<Skill> Skills { get; set; }

		public Upgrade()
		{
			AttackString = string.Empty;
			Delay = string.Empty;
			Health = Constants.DefaultHealth;
			
		}
	}
}
