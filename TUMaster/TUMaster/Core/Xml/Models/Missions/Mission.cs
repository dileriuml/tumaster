﻿using System.Xml.Serialization;

namespace TUMaster.Core.Xml.Models
{
	[XmlRoot("mission")]
	public class Mission
	{
		[XmlElement("id")]
		public int Id { get; set; }

		[XmlElement("name")]
		public string Name { get; set; }

		[XmlElement("energy")]
		public int EnergyCost { get; set; }

		[XmlElement("event_req")]
		public int EventReq { get; set; }

		[XmlElement("location_id")]
		public int LocationId { get; set; }

		[XmlElement("req")]
		public int Require { get; set; }

		[XmlElement("mission_type", IsNullable = true)]
		public int? MissionType { get; set; }

		[XmlElement("rewards")]
		public MissionReward Rewards { get; set; }

		public double Profit => (double)Rewards.Gold / EnergyCost;

		public int CompleteCount { get; set; }

		public override string ToString()
		{
			return $"Mission Id={Id}, Name={Name}, EnergyCost={EnergyCost}, Rewards=\n\t{Rewards}";
		}
	}
}
