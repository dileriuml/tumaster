﻿namespace TUMaster.Core.Xml
{
	public static class Pathes
	{
		private const string WebPathAssets = "http://mobile.tyrantonline.com/assets/";
		public const string AchievementsXml = "achievements.xml";
		public const string FustionsXml = "fusion_recipes_cj2.xml";
		public const string MissionsXml = "missions.xml";

		public static string PathToWeb(string filePath) => $"{WebPathAssets}{filePath}";
	}
}
