﻿using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;

namespace TUMaster.Core.Xml
{
	public static class TUXmlUtils
	{
		const string RootElement = "root";

		static XmlSerializer GetSerializer<T>(string fileName)
			=> new XmlSerializer(typeof(T), new XmlRootAttribute(RootElement));

		public static async Task<XmlReader> GetXmlReaderAsync(string fileName, bool isNeedToReloadFromWeb = false)
		{
			if (isNeedToReloadFromWeb || !File.Exists(fileName))
				using (var webStream = await new WebClient().OpenReadTaskAsync(Pathes.PathToWeb(fileName)))
				using (var fileStream = File.Create(fileName))
					await webStream.CopyToAsync(fileStream);

			return XmlReader.Create(fileName);
		}

		public static Task<T> DeserializeAsync<T>(this XmlReader reader, string fileName)
			where T : class
			=> Task.Run(() => GetSerializer<T>(fileName).Deserialize(reader) as T);
	}
}
