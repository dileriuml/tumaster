﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using TUMaster.Core.Services;
using TUMaster.Utils;

namespace TUMaster.Core.Xml
{
	public class XmlGameDb
	{
		#region Fields

		private readonly ProfileService profileService;

		#endregion

		#region Collections

		public AchievementsDb AchievementsDb { get; private set; }

		public CardsDb CardsDb { get; private set; }
		
		public MissionsDb MissionsDb { get; private set; }

		public FusionsDb FustionsDb { get; private set; }

		#endregion

		#region Constructor

		public XmlGameDb(ProfileService profileService)
		{
			this.profileService = profileService;
		}

		#endregion

		#region Init

		public async Task InitAsync(ILongProgress progress = null, bool isNeedToReloadFromWeb = false)
		{
			try
			{
				var files = typeof(Pathes).GetAllConstants<string>();
				var cardFileNames = await profileService.GetCardFileNamesListAsync();
				var allFiles = files.Concat(cardFileNames).ToArray();
				var cardDbs = new List<CardsDb>();
				progress.With(p => p.Message = $"Loading app files...");

				var dbLoadingTasks = allFiles.Select(async fileName =>
				{
					using (var xmlReader = await TUXmlUtils.GetXmlReaderAsync(fileName, isNeedToReloadFromWeb))
					{
						switch (fileName)
						{
							case Pathes.AchievementsXml:
								AchievementsDb = await xmlReader.DeserializeAsync<AchievementsDb>(fileName);
								break;
							case Pathes.FustionsXml:
								FustionsDb = await xmlReader.DeserializeAsync<FusionsDb>(fileName);
								break;
							case Pathes.MissionsXml:
								MissionsDb = await xmlReader.DeserializeAsync<MissionsDb>(fileName);
								break;
							default:
								var deserializedCardDb = await xmlReader.DeserializeAsync<CardsDb>(fileName);
								cardDbs.Add(deserializedCardDb);
								break;
						}
					}

					progress.With(p => p.Progress += 100 / allFiles.Length);
				});

				await Task.WhenAll(dbLoadingTasks);
				CardsDb = CardsDb.ProduceFinalDb(cardDbs);
			}
			catch (Exception e)
			{
				Log.Message(e.Message);
			}
		}

		#endregion
	}
}
