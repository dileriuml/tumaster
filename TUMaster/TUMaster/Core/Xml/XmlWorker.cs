﻿using System.IO;
using ServiceStack.Text;

namespace TUMaster.Core.Xml
{
	public class XmlWorker
	{
		public const string FILENAME = "DefaultSaved.xml";

		public void Save<T>(T objToSave, string fileName = FILENAME) where T: class
		{
			using (var writer = new StreamWriter(fileName))
				XmlSerializer.SerializeToWriter(objToSave, writer);
		}

		public T Load<T>(string fileName = FILENAME)
			where T : class
		{
			try
			{
				using (var reader = new StreamReader(fileName))
					return XmlSerializer.DeserializeFromReader<T>(reader);
			}
			catch
			{
				return null;
			}
		}
	}
}
