﻿using System;
using System.Windows;
using MvvmCross.Platforms.Wpf.Views;
using TUMaster.UI.Windows;

namespace TUMaster
{
	public partial class LaunchScreen : MvxMetroWindow
	{
		public LaunchScreen()
		{
			InitializeComponent();
		}

		private void OnInitialized(object sender, EventArgs e)
		{
			if (Application.Current is MvxApplication mvxApplication)
			{
				mvxApplication.ApplicationInitialized();
			}

			Close();
		}
	}
}
