﻿using System.Linq;
using System.Windows.Controls;
using MvvmCross;
using MvvmCross.IoC;
using MvvmCross.Platforms.Wpf.Core;
using MvvmCross.Platforms.Wpf.Presenters;
using TUMaster.Core.Network;
using TUMaster.Core.Services;
using TUMaster.Core.Xml;
using TUMaster.UI.Presenter;
using TUMaster.VM;
using TUMaster.VM.App;

namespace TUMaster
{
	public class Setup : MvxWpfSetup<CoreApp>
	{
		protected override IMvxWpfViewPresenter CreateViewPresenter(ContentControl root) 
			=> new WindowsPresenter();

		public override void InitializePrimary()
		{
			base.InitializePrimary();
			RegisterServices();
			RegisterControllers();
		}

		private void RegisterControllers()
		{
			new[]
			{
				typeof(StoreControllerVm),
				typeof(WarControllerVm),
				typeof(InventoryControllerVm),
				typeof(ProfileControllerVm),
				typeof(BotControllerVm)
			}.AsTypes().RegisterAsDynamic();
		}

		private void RegisterServices()
		{
			Mvx.LazyConstructAndRegisterSingleton<XmlWorker, XmlWorker>();
			Mvx.LazyConstructAndRegisterSingleton<XmlGameDb, XmlGameDb>();
			Mvx.LazyConstructAndRegisterSingleton<ConnectionManager, ConnectionManager>();

			var types = CreatableTypes()
				.InNamespace(typeof(BaseService).Namespace)
				.Except(typeof(BaseService))
				.ToList();

			types.AsTypes().RegisterAsLazySingleton();
		}
	}
}
