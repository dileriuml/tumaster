﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Interactivity;

namespace TUMaster.UI.Behaviors
{
	public class DataGridSelectedItemsBehavior<T> : Behavior<DataGrid> where T : class
	{
		protected override void OnAttached()
		{
			AssociatedObject.SelectionChanged += AssociatedObjectSelectionChanged;
		}

		protected override void OnDetaching()
		{
			AssociatedObject.SelectionChanged -= AssociatedObjectSelectionChanged;
		}

		void AssociatedObjectSelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			var selectedItems = AssociatedObject.SelectedItems.OfType<T>().ToArray();
			SelectedItems = selectedItems.Length == 0 ? null : selectedItems;
		}

		public static readonly DependencyProperty SelectedItemsProperty =
				DependencyProperty.Register(nameof(SelectedItems), typeof(IEnumerable<T>), typeof(DataGridSelectedItemsBehavior<T>),
				new FrameworkPropertyMetadata(null, FrameworkPropertyMetadataOptions.BindsTwoWayByDefault));

		public IEnumerable<T> SelectedItems
		{
			get => (IEnumerable<T>)GetValue(SelectedItemsProperty);
			set => SetValue(SelectedItemsProperty, value);
		}
	}
}
