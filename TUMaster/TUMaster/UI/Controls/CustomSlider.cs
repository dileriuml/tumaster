﻿using MahApps.Metro.Controls;
using System.Windows;
using System.Windows.Controls;
using TUMaster.UI.Extensions;

namespace TUMaster.UI.Controls
{
	public class CustomSlider : Slider
	{
		private MetroThumb metroThumb;

		public CustomSlider()
		{
			Style = (Style)FindResource("FlatSlider");
		}

		public ControlTemplate ThumbTemplate
		{
			get => (ControlTemplate)GetValue(ThumbTemplateProperty);
			set => SetValue(ThumbTemplateProperty, value);
		}

		public static readonly DependencyProperty ThumbTemplateProperty =
				DependencyProperty.Register(nameof(ThumbTemplate),
					typeof(ControlTemplate),
					typeof(CustomSlider),
					new PropertyMetadata(null));

		public override void OnApplyTemplate()
		{
			base.OnApplyTemplate();
			metroThumb = this.GetFirstAncestorOfType<MetroThumb>();
			metroThumb.Template = ThumbTemplate;
		}
	}
}
