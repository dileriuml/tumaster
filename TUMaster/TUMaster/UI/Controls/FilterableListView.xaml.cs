﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TUMaster.UI.Controls
{
	public partial class FilterableListView : ListView
	{
		public FilterableListView()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty FilterableTypeProperty =
		 DependencyProperty.Register(nameof(FilterableType), typeof(Type), typeof(FilterableListView),
																 new FrameworkPropertyMetadata(null));

		public Type FilterableType
		{
			get { return (Type)GetValue(FilterableTypeProperty); }
			set { SetValue(FilterableTypeProperty, value); }
		}
	}
}
