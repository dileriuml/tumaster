﻿using System.Windows;
using System.Windows.Controls;

namespace TUMaster.UI.Controls
{
	/// <summary>
	/// Interaction logic for IndicableProgressBar.xaml
	/// </summary>
	public partial class IndicableProgressBar : UserControl
	{
		public static readonly DependencyProperty CurrentValueProperty =
			DependencyProperty.Register("CurrentValue", typeof(int), typeof(IndicableProgressBar),
																	new FrameworkPropertyMetadata { BindsTwoWayByDefault = true });

		public int CurrentValue
		{
			get { return (int)GetValue(CurrentValueProperty); }
			set { SetValue(CurrentValueProperty, value); }
		}

		public static readonly DependencyProperty MaxValueProperty =
			DependencyProperty.Register("MaxValue", typeof(int), typeof(IndicableProgressBar),
																	new FrameworkPropertyMetadata { BindsTwoWayByDefault = true });

		public int MaxValue
		{
			get { return (int)GetValue(MaxValueProperty); }
			set { SetValue(MaxValueProperty, value); }
		}

		public IndicableProgressBar()
		{
			InitializeComponent();
		}
	}
}
