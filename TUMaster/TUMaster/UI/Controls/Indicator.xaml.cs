﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TUMaster.UI.Controls
{
	public partial class Indicator : Border, INotifyPropertyChanged
	{
		public Indicator()
		{
			InitializeComponent();
		}

		public static readonly DependencyProperty IconSourceProperty =
			DependencyProperty.Register(nameof(IconSource), typeof(ImageSource), typeof(Indicator),
																	new FrameworkPropertyMetadata(null) { BindsTwoWayByDefault = true });

		public ImageSource IconSource
		{
			get { return (ImageSource)GetValue(IconSourceProperty); }
			set { SetValue(IconSourceProperty, value); }
		}

		public static readonly DependencyProperty TextValueProperty =
			DependencyProperty.Register(nameof(TextValue), typeof(string), typeof(Indicator),
																	new FrameworkPropertyMetadata(null) { BindsTwoWayByDefault = true });

		public string TextValue
		{
			get
			{
				return (string)GetValue(TextValueProperty);
			}
			set
			{
				SetValue(TextValueProperty, value);
				RaiseTextChanged();
			}
		}

		public static readonly DependencyProperty IsReadOnlyProperty =
			DependencyProperty.Register(nameof(IsReadOnly), typeof(bool), typeof(Indicator),
																	new FrameworkPropertyMetadata(true));

		public bool IsReadOnly
		{
			get { return (bool)GetValue(IsReadOnlyProperty); }
			set { SetValue(IsReadOnlyProperty, value); }
		}

		#region INotifyPropertyChanged implementation

		public event PropertyChangedEventHandler PropertyChanged;

		public void RaiseTextChanged()
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TextValue)));
		}

		#endregion
	}
}
