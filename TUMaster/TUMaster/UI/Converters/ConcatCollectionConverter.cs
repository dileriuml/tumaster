﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Windows;
using System.Windows.Data;

namespace TUMaster.UI.Converters
{
	public class ConcatCollectionConverter : IMultiValueConverter
	{
		#region IMultiValueConverter implementation

		public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
		{
			var list = new List<object>();

			foreach (var element in values)
			{
				if (element == DependencyProperty.UnsetValue)
					continue;

				var ienum = element as IEnumerable;

				if (ienum != null)
				{
					list.AddRange(ienum.Cast<object>());
				}
				else
				{
					list.Add(element);
				}
			}

			return list;
		}
		
		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
