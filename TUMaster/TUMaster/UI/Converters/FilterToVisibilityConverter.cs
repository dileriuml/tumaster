﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Data;

namespace TUMaster.UI.Converters
{
	public class FilterToVisibilityConverter : IMultiValueConverter
	{
		#region IMultiValueConverter implementation

		public object Convert(object[] values, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			if (values.Contains(null))
				return Visibility.Visible;

			String filterString = null;
			PropertyInfo filterProperty = null;

			foreach (var val in values)
			{
				val.WithType<string>(v => filterString = v);
				val.WithType<PropertyInfo>(v => filterProperty = v);
			}

			return filterProperty.With(p => p.GetValue(values[0]).ToString().Contains(filterString, StringComparison.OrdinalIgnoreCase)) ? Visibility.Visible : Visibility.Collapsed;
		}

		public object[] ConvertBack(object value, Type[] targetTypes, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion

	}
}
