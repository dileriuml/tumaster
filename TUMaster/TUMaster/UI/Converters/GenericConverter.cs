﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TUMaster.UI.Converters
{
	public abstract class GenericConverter<TIn, TOut> : IValueConverter
	{
		public object Convert(object value, Type targetType, object parameter, CultureInfo culture) => Convert((TIn)value);

		public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture) => ConvertBack((TOut)value);

		protected abstract TOut Convert(TIn inValue);

		protected virtual TIn ConvertBack(TOut outValue) => throw new NotImplementedException();
	}
}
