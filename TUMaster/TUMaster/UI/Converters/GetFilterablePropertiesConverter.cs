﻿using System;
using System.Linq;
using System.Reflection;
using System.Windows.Data;
using TUMaster.VM.Attributes;

namespace TUMaster.UI.Converters
{
	public class GetFilterableProperiesConverter : IValueConverter
	{
		#region IValueConverter implementation

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var valueAsType = value as Type;

			if (valueAsType == null)
				return null;

			return from p in valueAsType.GetProperties()
						 let filterAtrType = p.GetCustomAttribute(typeof(FilterableAttribute)) as FilterableAttribute
						 where filterAtrType != null
						 orderby filterAtrType.Priority descending
						 select p;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
