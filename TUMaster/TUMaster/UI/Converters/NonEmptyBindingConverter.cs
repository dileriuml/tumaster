﻿using System;
using System.Windows.Data;

namespace TUMaster.UI.Converters
{
	/// <summary>
	/// Description of NonEmptyBindingConverter.
	/// </summary>
	public class NonEmptyBindingConverter : IValueConverter
	{
		#region IValueConverter implementation

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return string.IsNullOrWhiteSpace(value.ToString()) ? parameter : value;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return string.IsNullOrWhiteSpace(value.ToString()) ? parameter : value;
		}

		#endregion
	}
}
