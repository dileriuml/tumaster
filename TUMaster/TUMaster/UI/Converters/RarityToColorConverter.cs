﻿using System;
using System.Windows.Data;
using System.Windows.Media;
using TUMaster.Core.Xml.Models;

namespace TUMaster.UI.Converters
{
	/// <summary>
	/// Description of RarityToColorConverter.
	/// </summary>
	public class RarityToColorConverter : IValueConverter
	{
		#region IValueConverter implementation

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			var rarity = (Rarity)value;

			switch (rarity)
			{
				case Rarity.Common:
					return new LinearGradientBrush(Colors.White, Colors.White, 45);

				case Rarity.Rare:
					return new LinearGradientBrush(Colors.Silver, Colors.White, 45);

				case Rarity.Epic:
					return new LinearGradientBrush(Colors.Gold, Colors.White, 45);

				case Rarity.Legendary:
					return new LinearGradientBrush(Colors.MediumPurple, Colors.White, 45);

				case Rarity.Vindicator:
					return new LinearGradientBrush(Colors.Silver, Colors.DarkSlateGray, 45);
			}

			return null;
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
