﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TUMaster.UI.Converters
{
	public class SelectedItemsCollectionConverter<T> : GenericConverter<IList, IEnumerable<T>>
	{
		protected override IEnumerable<T> Convert(IList inValue) => inValue.OfType<T>();
	}
}
