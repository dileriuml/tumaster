﻿using System;
using System.Windows;
using System.Windows.Data;

namespace TUMaster.UI.Converters
{
	public class ValueToResourceConverter : IValueConverter
	{
		#region IValueConverter implementation

		public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			return Application.Current.TryFindResource(value);
		}

		public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
		{
			throw new NotImplementedException();
		}

		#endregion

	}
}
