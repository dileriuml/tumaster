﻿using System.Windows;
using System.Windows.Media;

namespace TUMaster.UI.Extensions
{
	public static class DependencyObjectExtensions
	{
		public static T GetFirstAncestorOfType<T>(this DependencyObject obj)
			where T : DependencyObject
		{
			if (obj == null) return null;

			int childrenCount = VisualTreeHelper.GetChildrenCount(obj);

			for (int i = 0; i < childrenCount; i++)
			{
				var child = VisualTreeHelper.GetChild(obj, i);
				var foundChild = (child as T) ?? GetFirstAncestorOfType<T>(child);

				if (foundChild != null)
				{
					return foundChild;
				}
			}

			return null;
		}
	}
}
