﻿using System.Linq;
using System.Windows;
using TUMaster.VM;
using TUMaster.UI.Windows;
using MvvmCross.Platforms.Wpf.Presenters;
using MvvmCross.ViewModels;
using MvvmCross.Presenters.Hints;
using MvvmCross.Platforms.Wpf.Presenters.Attributes;

namespace TUMaster.UI.Presenter
{
	public class WindowsPresenter : MvxWpfViewPresenter
	{
		public override void Show(MvxViewModelRequest request)
		{
			if (request.ViewModelType == typeof(GameVm))
			{
				var existingWindow = Application.Current.Windows
					.OfType<Window>()
					.FirstOrDefault(x => 
						(x.DataContext is GameVm game) && 
						game.UserData.UserID == request.ParameterValues.First().Value);

				if (existingWindow != null)
				{
					existingWindow.Show();
					return;
				}
			}

			base.Show(request);
		}

		protected override void ShowWindow(
			FrameworkElement element, 
			MvxWindowPresentationAttribute attribute, 
			MvxViewModelRequest request)
		{
			if (element is Window window)
			{
				switch (window)
				{
					case MainWindow mainWindow:
						Application.Current.MainWindow = mainWindow;
						break;
					default:
						window.Owner = Application.Current.Windows.OfType<MainWindow>().SingleOrDefault();
						break;
				}
			}

			base.ShowWindow(element, attribute, request);
		}
	}
}
