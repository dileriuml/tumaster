﻿using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using TUMaster.VM.Game;

namespace TUMaster.UI.Windows
{
	[MvxWindowPresentation(Modal = true)]
	public partial class BuyoutGoldWindow : MvxMetroWindow<BuyoutGoldVm>
	{
		public BuyoutGoldWindow()
		{
			InitializeComponent();
		}
	}
}
