﻿using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using TUMaster.VM;

namespace TUMaster.UI.Windows
{
	[MvxWindowPresentation(Modal = true)]
	public partial class GameControllerWindow : MvxMetroWindow<GameVm>
	{
		public GameControllerWindow()
		{
			InitializeComponent();
		}
	}
}
