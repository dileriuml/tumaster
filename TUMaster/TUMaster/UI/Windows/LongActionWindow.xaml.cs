﻿using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using TUMaster.VM.Special;

namespace TUMaster.UI.Windows
{
	[MvxWindowPresentation(Modal = true)]
	public partial class LongActionWindow : MvxMetroWindow<LongActionVm>
	{
		public LongActionWindow()
		{
			InitializeComponent();
		}
	}
}
