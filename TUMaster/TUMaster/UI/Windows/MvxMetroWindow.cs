﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using MahApps.Metro.Controls;
using MvvmCross.Platforms.Wpf.Views;
using MvvmCross.ViewModels;
using MvvmCross.Views;

namespace TUMaster.UI.Windows
{
	public class MvxMetroWindow : MetroWindow, IMvxWpfView, IMvxWindow, IDisposable
	{
		public static readonly DependencyProperty BeforeSaveCommandProperty =
			DependencyProperty.Register(
				nameof(BeforeCloseCommand),
				typeof(ICommand),
				typeof(MvxMetroWindow));

		public ICommand BeforeCloseCommand
		{
			get => (ICommand)GetValue(BeforeSaveCommandProperty);
			set => SetValue(BeforeSaveCommandProperty, value);
		}

		public MvxMetroWindow()
		{
			Unloaded += MvxWindowUnloaded;
			Loaded += MvxWindowLoaded;
		}

		public string Identifier { get; set; }

		IMvxViewModel IMvxView.ViewModel
		{
			get => DataContext as IMvxViewModel;
			set => DataContext = value;
		}

		protected override void OnClosing(CancelEventArgs e)
		{
			BeforeCloseCommand?.Execute(null);
			base.OnClosing(e);
		}

		private void MvxWindowUnloaded(object sender, RoutedEventArgs e)
		{
			var mvxView = this as IMvxView;
			mvxView.ViewModel?.ViewDisappearing();
			mvxView.ViewModel?.ViewDisappeared();
			mvxView.ViewModel?.ViewDestroy();
		}

		private void MvxWindowLoaded(object sender, RoutedEventArgs e)
		{
			var mvxView = this as IMvxView;
			mvxView.ViewModel?.ViewAppearing();
			mvxView.ViewModel?.ViewAppeared();
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}

		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				Unloaded -= MvxWindowUnloaded;
				Loaded -= MvxWindowLoaded;
			}
		}

		~MvxMetroWindow()
		{
			Dispose(false);
		}
	}

	public class MvxMetroWindow<TVm> : MvxMetroWindow, IMvxWpfView<TVm>
		where TVm : class, IMvxViewModel
	{
		TVm viewModel;

		public TVm ViewModel
		{
			get => viewModel;
			set => ((IMvxView)this).ViewModel = viewModel = value;
		}
	}
}
