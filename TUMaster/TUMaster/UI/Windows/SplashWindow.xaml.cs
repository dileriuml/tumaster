﻿using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using TUMaster.VM.App;

namespace TUMaster.UI.Windows
{
	[MvxWindowPresentation(Modal = true)]
	public partial class SplashWindow : MvxMetroWindow<SplashViewModel>
	{
		public SplashWindow()
		{
			InitializeComponent();
		}
	}
}
