﻿using MvvmCross.Platforms.Wpf.Presenters.Attributes;
using TUMaster.VM.Special;

namespace TUMaster.UI.Windows
{
	[MvxWindowPresentation(Modal = true)]
	public partial class TextInfoWindow : MvxMetroWindow<TextInfoVm>
	{
		public TextInfoWindow()
		{
			InitializeComponent();
		}
	}
}
