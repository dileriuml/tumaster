﻿using MvvmCross.ViewModels;

namespace TUMaster.VM.App
{
	public class CoreApp : MvxApplication
	{
		public override void Initialize()
		{
			RegisterAppStart<SplashViewModel>();
			base.Initialize();
		}
	}
}
