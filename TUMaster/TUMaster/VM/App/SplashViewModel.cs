﻿using System.Threading.Tasks;
using TUMaster.Core.Network;
using TUMaster.Core.Services;
using TUMaster.Core.Xml;
using TUMaster.VM.Game;
using TUMaster.VM.Special;

namespace TUMaster.VM.App
{
	public class SplashViewModel : ProgressViewModelBase
	{
		private XmlGameDb xmlDb;
		private UsersCredsManager usersManager;
		private TaskCompletionSource<int> waitingForConnection;

		public SplashViewModel(XmlGameDb xmlDb, UsersCredsManager usersManager, ConnectionManager connectionManager)
		{
			this.xmlDb = xmlDb;
			this.usersManager = usersManager;
			connectionManager.ConnectionStatusChanged += ConnectionManagerConnectionStatusChanged;
			waitingForConnection = new TaskCompletionSource<int>();
		}

		private async void ConnectionManagerConnectionStatusChanged(ConnectionStatus status)
		{
			switch (status)
			{
				case ConnectionStatus.Online:
					waitingForConnection?.SetResult(0);
					break;

				case ConnectionStatus.Offline:
					Message = "App is offline and will be closed in 3 sec...";
					await Task.Delay(3000);
					await NavigationService.Close(this);
					break;
			}
		}

		public override async Task Initialize()
		{
			#region Init main managers and xml database

			await waitingForConnection.Task;
			waitingForConnection = null;

			await xmlDb.InitAsync(this);

			#endregion

			NavigationService.Navigate<MainVm>();
			NavigationService.Close(this);
		}
	}
}
