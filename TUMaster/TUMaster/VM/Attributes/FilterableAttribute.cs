﻿using System;

namespace TUMaster.VM.Attributes
{
	[AttributeUsage(AttributeTargets.Property)]
	public class FilterableAttribute : Attribute
	{
		public int Priority { get; set; }

		public FilterableAttribute()
		{
			Priority = 0;
		}
	}
}
