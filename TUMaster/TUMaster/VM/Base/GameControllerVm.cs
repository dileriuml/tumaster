﻿using System.Threading.Tasks;
using MvvmCross;
using MvvmCross.ViewModels;
using TUMaster.Core.Data;
using TUMaster.Core.Services;
using TUMaster.VM.Commands;

namespace TUMaster.VM
{
	public abstract class GameControllerVm : VmBase, IMvxViewModel<string>
	{
		#region Static services

		protected readonly ProfileService profileService;
		protected readonly UsersCache usersCache;
		protected readonly UsersCredsManager credsManager;

		#endregion

		#region Fields

		User user;
		bool isLoading = false;

		#endregion

		protected GameControllerVm(string controllerName)
		{
			Title = controllerName;
			profileService = Mvx.Resolve<ProfileService>();
			usersCache = Mvx.Resolve<UsersCache>();
			credsManager = Mvx.Resolve<UsersCredsManager>();
			ReloadCommand = new AsyncCommand(AsyncLoad);
		}

		#region Init

		public void Prepare(string userId)
		{
			user = credsManager.GetUserById(userId);
			usersCache.AddUpdateHandler(userId, OnUserDataUpdated);
			OnInit();
		}

		protected virtual void OnInit()
		{
		}

		#endregion

		#region Properties

		public bool IsLoading
		{
			get
			{
				return isLoading;
			}

			set
			{
				isLoading = value;
				RaisePropertyChanged();
			}
		}

		public string Title { get; }

		public User User => user;

		public ServiceUserData UserAccountData => usersCache.GetCurrentData(user.UserId);
		
		#endregion

		#region Commands

		public AsyncCommand ReloadCommand { get; }

		#endregion

		#region Methods

		protected internal async Task AsyncLoad()
		{
			if (IsLoading)
				return;

			IsLoading = true;
			await ReloadUserDataAsync();
			await LoadAsync();
			IsLoading = false;
		}

		protected virtual Task LoadAsync() => Task.FromResult(0);

		protected Task ReloadUserDataAsync() => usersCache.LoadUser(user.UserId);

		private void OnUserDataUpdated(AccountResponse result)
		{
			if (result == AccountResponse.Success)
			{
				RaisePropertyChanged(nameof(UserAccountData));
				OnUserDataUpdated();
			}
			else
			{
				//TODO: Add appropriate dialogs
			}
		}

		protected virtual void OnUserDataUpdated()
		{

		}

		#endregion
	}
}
