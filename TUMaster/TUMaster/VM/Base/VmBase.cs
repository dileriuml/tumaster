﻿using MvvmCross.ViewModels;
using System.ComponentModel;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class VmBase : MvxViewModel, INotifyPropertyChanged
	{
		public void ShowInfoMesasge(string title, string text)
		{
			NavigationService.Navigate<TextInfoVm, (string, string)>((title, text));
		}

		public override void ViewDestroy(bool viewFinishing = true)
		{
			base.ViewDestroy(viewFinishing);

			if (viewFinishing)
			{
				OnClose();
			}
		}

		protected virtual void OnClose()
		{
		}
	}
}
