﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace TUMaster.VM.Commands
{
	public class AsyncCommand : ICommand
	{
		bool isExecuting;
		Func<bool> canExecute;
		Func<Task> asyncHandler;

		public AsyncCommand(Func<Task> asyncHandler, Func<bool> canExecute = null)
		{
			this.asyncHandler = asyncHandler;
			this.canExecute = canExecute;
		}

		public bool CanExecute(object parameter) => canExecute == null || canExecute();

		public async void Execute(object parameter)
		{
			if (asyncHandler != null || !isExecuting)
			{
				isExecuting = true;
				await asyncHandler();
				isExecuting = false;
			}
		}

		public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		public event EventHandler CanExecuteChanged;
	}

	public class AsyncCommand<T> : ICommand
	{
		bool isExecuting;
		Func<T, Task> asyncHandler;
		Func<T, bool> canExecute;

		public AsyncCommand(Func<T, Task> asyncHandler, Func<T, bool> canExecute = null)
		{
			this.asyncHandler = asyncHandler;
			this.canExecute = canExecute;
		}

		public bool CanExecute(object parameter) => canExecute == null || canExecute((T)parameter);

		public async void Execute(object parameter)
		{
			if (asyncHandler != null || !isExecuting)
			{
				isExecuting = false;
				await asyncHandler((T)parameter);
				isExecuting = true;
			}
		}

		public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		public event EventHandler CanExecuteChanged;
	}
}
