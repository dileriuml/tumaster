﻿using System;
using System.Windows.Input;

namespace TUMaster.VM.Commands
{
	public class Command : ICommand
	{
		Action action;
		Func<bool> canExecute;

		public Command(Action action, Func<bool> canExecute = null)
		{
			this.action = action;
			this.canExecute = canExecute;
		}

		public void Execute(object parameter) => Execute();

		public void Execute() => action?.Invoke();

		public bool CanExecute(object parameter) => canExecute == null || canExecute();

		public void RaiseCanExecuteChanged() => CanExecuteChanged?.Invoke(this, EventArgs.Empty);

		public event EventHandler CanExecuteChanged;
	}

	public class Command<T> : ICommand
	{
		Action<T> action;
		Func<T, bool> canExecute;

		public Command(Action<T> action, Func<T, bool> canExecute = null)
		{
			this.action = action;
			this.canExecute = canExecute;
		}

		public void Execute(object parameter) => action?.Invoke((T)parameter);

		public bool CanExecute(object parameter) => canExecute == null || canExecute((T)parameter);

		public void OnCanExecuteChanged()
		{
			var handler = CanExecuteChanged;
			handler.With(hEvent => hEvent(this, EventArgs.Empty));
		}

		public event EventHandler CanExecuteChanged;
	}
}
