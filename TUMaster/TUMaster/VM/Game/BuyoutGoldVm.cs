﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.ViewModels;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Store;
using TUMaster.Core.Services;
using TUMaster.Core.Xml;
using TUMaster.Core.Xml.Models;
using TUMaster.VM.Game.Controllers.Inventory;

namespace TUMaster.VM.Game
{
	public class BuyoutGoldVm : VmBase, IMvxViewModel<(string userId, ServiceStoreItem itemPack)>
	{
		private ServiceStoreItem itemPack;
		private readonly StoreService storeService;
		private ServiceUserData userData;
		private UsersCache usersCache;
		private CardsDb cardsDb;
		private string userId;
		private bool isPaused;
		private int gainedCommons;
		private int gainedRares;

		public BuyoutGoldVm(StoreService storeService, UsersCache usersCache, CardsDb cardsDb)
		{
			this.storeService = storeService;
			this.usersCache = usersCache;
			this.cardsDb = cardsDb;
		}

		public int SpentAmount { get; set; }

		public ServiceUserData UserData
		{
			get => userData;
			set => SetProperty(ref userData, value);
		}

		public int GainedCommons
		{
			get => gainedCommons;
			set => SetProperty(ref gainedCommons, value);
		}

		public int GainedRares
		{
			get => gainedRares;
			set => SetProperty(ref gainedRares, value);
		}

		public bool IsPaused
		{
			get => isPaused;
			set => SetProperty(ref isPaused, value);
		}

		public void Prepare((string userId, ServiceStoreItem itemPack) parameters)
		{
			this.userId = parameters.userId;
			this.itemPack = parameters.itemPack;
			UserData = usersCache.GetCurrentData(userId);
		}

		private async Task TryUpdateUserData()
		{
			UserData = await usersCache.LoadUser(userId);
		}

		public ObservableCollection<EarnedCardVm> EarnedCards { get; } = new ObservableCollection<EarnedCardVm>();

		public async Task RunBuyingProcess()
		{
			IsPaused = false;

			while (true)
			{
				var buyResult = await storeService.BuyItemAsync(itemPack, userId);

				if (buyResult.IsTooManyCards || !buyResult.Response.Result)
				{
					break;
				}

				var (Rares, Commons, EpicAndBetter) = LoadBuyResult(buyResult);
				GainedCommons += Commons;
				GainedRares += Rares;
				EarnedCards.AddUsingDispatcher(EpicAndBetter);
			}

			IsPaused = true;
		}

		private (int Rares, int Commons, EarnedCardVm[] EpicAndBetter) LoadBuyResult(BuyResultResponse buyResult)
		{
			var cardIds = buyResult.NewCards;
			var receivedCards = cardIds.Select(x => new { Card = cardsDb.GetCard(x.Key), Count = x.Value });

			return (
				Rares: receivedCards.Where(x => x.Card.Rarity == Rarity.Common).Sum(x => x.Count),
				Commons: receivedCards.Where(x => x.Card.Rarity == Rarity.Rare).Sum(x => x.Count),
				EpicAndBetter: receivedCards
					.Where(x => x.Card.RarityId > (int)Rarity.Rare)
					.Select(x => new EarnedCardVm(x.Card) { Count = x.Count })
					.ToArray());
		}
	}
}
