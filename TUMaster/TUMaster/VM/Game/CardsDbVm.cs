﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TUMaster.Core.Xml;
using TUMaster.Core.Xml.Models.Card;

namespace TUMaster.VM
{
	public class CardsDbVm : VmBase
	{
		private readonly XmlGameDb xmlGameDb;
		private List<CardDTO> cards;
		private string filter;

		public CardsDbVm(XmlGameDb xmlGameDb)
		{
			this.xmlGameDb = xmlGameDb;
			Cards = new ObservableCollection<CardVm>();
		}

		public ObservableCollection<CardVm> Cards { get; private set; }

		public string Filter
		{
			get => filter;

			set
			{
				filter = value;

				if (!string.IsNullOrEmpty(filter) && filter.Length > 3)
				{
				//	OnFilterUpdated();
				}

				RaisePropertyChanged();
			}
		}

		private void OnFilterUpdated()
		{
			var card = xmlGameDb.CardsDb.GetCard(filter.ToInt());
			Cards = new ObservableCollection<CardVm>(new[] { new CardVm(card) });
			RaisePropertyChanged(nameof(Cards));
		}

		public void Init()
		{
			cards = xmlGameDb.CardsDb.Cards;
			var cardsVm = cards.Where(x => x.Level == 1).Select(x => new CardVm(x)).ToList();
			Cards = new ObservableCollection<CardVm>(cardsVm);
			RaisePropertyChanged(nameof(Cards));
		}
	}
}
