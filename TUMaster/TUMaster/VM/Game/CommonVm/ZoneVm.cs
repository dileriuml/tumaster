﻿using MvvmCross.ViewModels;
using TUMaster.Core.Data.Conquest;

namespace TUMaster.VM.Game.CommonVm
{
	public class ZoneVm : MvxViewModel
	{
		private ZoneData zoneInfo;

		public ZoneData ZoneInfo
		{
			get => zoneInfo;
			set => SetProperty(ref zoneInfo, value);
		}

		public ZoneVm(ZoneData zoneInfo) => this.zoneInfo = zoneInfo;
	}
}
