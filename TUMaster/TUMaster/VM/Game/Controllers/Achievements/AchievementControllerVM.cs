﻿using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using TUMaster.Core.Xml;

namespace TUMaster.VM
{
	public class AchievementControllerVm : GameControllerVm
	{
		#region Fields
		
		int claims = 1;
		bool isLoaded = false;
		XmlGameDb xmlDb;

		#endregion

		#region Constructors

		public AchievementControllerVm(XmlGameDb xmlDb) : base("Achievements")
		{
			this.xmlDb = xmlDb;
			Achievements = new ObservableCollection<AchievementVm>();
		}

		#endregion

		#region Properties

		public ObservableCollection<AchievementVm> Achievements { get; }

		public int CountOfClaims
		{
			get
			{
				return claims;
			}

			set
			{
				if (value <= 0)
					value = 1;

				claims = value;
				RaisePropertyChanged();
			}
		}

		#endregion

		#region ILoadable implementation
		
		protected override async Task LoadAsync()
		{
			if (isLoaded)
				return;

			xmlDb.AchievementsDb.Achievements.ForEach(item => Achievements.AddUsingDispatcher(new AchievementVm(item)));
			isLoaded = true;
		}

		public bool IsLoaded => isLoaded;

		#endregion
	}
}
