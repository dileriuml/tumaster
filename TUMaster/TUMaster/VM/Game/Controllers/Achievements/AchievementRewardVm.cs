﻿using TUMaster.Core.Xml.Models;

namespace TUMaster.VM
{
	public enum RewardTypes
	{
		None,
		Gold,
		Wbs,
		Card,
		Sp,
		GP
	}

	public class AchievementRewardVm
	{
		AchievementReward reward;
		RewardTypes rewardType;

		public AchievementRewardVm(AchievementReward reward)
		{
			this.reward = reward;

			if (reward.Gold != 0)
				rewardType = RewardTypes.Gold;
			else if (reward.Wbs != 0)
				rewardType = RewardTypes.Wbs;
			else if (reward.Card != null)
				rewardType = RewardTypes.Card;
			else if (reward.SP != 0)
				rewardType = RewardTypes.Sp;
			else if (reward.GP != 0)
				rewardType = RewardTypes.GP;
		}

		public RewardTypes RewardType
		{
			get
			{
				return rewardType;
			}
		}

		public object Reward
		{
			get
			{
				switch (rewardType)
				{
					case RewardTypes.Gold:
						return reward.Gold;
					case RewardTypes.Wbs:
						return reward.Wbs;
					case RewardTypes.Sp:
						return reward.SP;
					case RewardTypes.GP:
						return reward.GP;
				}

				return null;
			}
		}
	}
}
