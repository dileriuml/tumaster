﻿using System.Collections.ObjectModel;
using System.Linq;
using TUMaster.Core.Xml.Models;
using TUMaster.VM.Attributes;

namespace TUMaster.VM
{
	public class AchievementVm : VmBase
	{
		Achievement achievement;

		public AchievementVm(Achievement achievement)
		{
			this.achievement = achievement;
			Rewards = new ObservableCollection<AchievementRewardVm>(achievement.Rewards.Select(r => new AchievementRewardVm(r)));
		}

		public Achievement Achievement => achievement;

		[Filterable]
		public string WholeInfo => string.Join("^", achievement.Name, achievement.Description);

		[Filterable]
		public string RewardType => string.Join("^", Rewards.Select(a => a.RewardType.ToString()));

		public ObservableCollection<AchievementRewardVm> Rewards { get; }
	}
}
