﻿using System.Collections.Generic;
using System.Reflection;

namespace TUMaster.VM
{
	/// <summary>
	/// Description of BattleEnergyActions.
	/// </summary>
	public static class BattleEnergyActions
	{
		public const string WAR_CORE_ATTACK = "War - Core";
		public const string WAR_BOTTOM_DEF_ATTACK = "War - bottom defence";
		public const string WAR_TOP_DEF_ATTACK = "War - top defence";
		public const string WAR_TOP_SIEGE_ATTACK = "War - top siege";
		public const string WAR_BOTTOM_SIEGE_ATTACK = "War - bottom siege";
		
		public static IEnumerable<string> AllActions => typeof(BattleEnergyActions).GetAllConstants<string>();
	}
}
