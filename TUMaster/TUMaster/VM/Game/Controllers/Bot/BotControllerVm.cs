﻿using System;
using System.Threading.Tasks;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Brawls;
using TUMaster.Core.Services;
using TUMaster.VM.Commands;

namespace TUMaster.VM
{
	#region Enums

	public enum BattleMode
	{
		PerformAttacks,
		RaidKillLevel,
		RaidDamageToDeal
	}

	public enum BotStatus
	{
		Working,
		Stopping,
		Stopped
	}

	public enum BotBlockingStatus
	{
		NotBlocked,
		Blocked,
		Unblocking
	}

	#endregion

	public class BotControllerVm : GameControllerVm
	{
		private BotManager botManager;
		private Bot bot;
		private ServiceBrawlsData brawlsData;

		#region Constructors & Destructors

		public BotControllerVm(BotManager botManager) : base("Bot")
		{
			this.botManager = botManager;
			RunPauseBotCommand = new Command(DoRunPauseBot);
			UnblockCommand = new AsyncCommand(DoUnblockBotAsync);
		}

		#endregion

		#region Properties

		public ServiceBrawlsData BrawlsData => brawlsData;

		public bool IsBrawlsCapControlEnabled
		{
			get => bot.IsBrawlsSmartCapIsEnabled;

			set
			{
				bot.IsBrawlsSmartCapIsEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool HuntingHardMode
		{
			get => bot.HuntingHardMode;

			set
			{
				bot.HuntingHardMode = value;
				RaisePropertyChanged();
			}
		}

		public bool IsHuntingEnabled
		{
			get => bot.IsHuntingEnabled;

			set
			{
				bot.IsHuntingEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsMissionsEnabled
		{
			get => bot.IsMissionsEnabled;

			set
			{
				bot.IsMissionsEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsDailyBonusEnabled
		{
			get => bot.IsDailyBonusEnabled;

			set
			{
				bot.IsDailyBonusEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsEventMissionsPlayable
		{
			get => bot.IsEventMissionsEnabled;

			set
			{
				bot.IsEventMissionsEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsGuildQuestsEnabled
		{
			get => bot.IsGuildQuestsEnabled;

			set
			{
				bot.IsGuildQuestsEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsEventMissionsEnabled
		{
			get => bot.IsEventMissionsEnabled;

			set
			{
				bot.IsEventMissionsEnabled = value;
				RaisePropertyChanged();
			}
		}

		public bool IsUsesRefills
		{
			get => bot.IsUsesRefills;

			set
			{
				bot.IsUsesRefills = value;
				RaisePropertyChanged();
			}
		}

		public int BrawlsCustomCap
		{
			get => bot.BrawlsCustomCap;

			set
			{
				bot.BrawlsCustomCap = value;
				RaisePropertyChanged();
			}
		}
		
		#endregion

		#region Readonly properties

		public BotStatus BotStatus => bot.BotStatus;

		public BotBlockingStatus BlockingStatus => bot.BlockingStatus;

		public double DowntimeLeft => bot.DowntimeLeft;

		public long LoadedTimeLeftForDaily => bot.LoadedTimeLeftForDaily;

		public TimeSpan TimeLeftForDailyBonus => TimeSpan.FromSeconds(Math.Max(LoadedTimeLeftForDaily, 0));

		#endregion

		#region Commands

		public Command RunPauseBotCommand { get; }

		public AsyncCommand UnblockCommand { get; }

		#endregion

		#region Init

		protected override void OnInit()
		{
			bot = botManager.GetUserBot(User.UserId);
			bot.BotInfoUpdated += BotInfoUpdated;
		}

		private void BotInfoUpdated(object sender, string[] props) => RaiseAllPropertiesChanged();

		#endregion

		#region Do on user data updated

		protected override void OnUserDataUpdated()
		{
			brawlsData = usersCache.GetCurrentBrawlsData(UserAccountData.UserID);
			RaisePropertyChanged(nameof(BrawlsData));
		}

		#endregion

		#region Command execute handlers

		private Task DoUnblockBotAsync() => bot.UnblockAsync();

		private void DoRunPauseBot() => bot.RunPause();

		#endregion
	}
}
