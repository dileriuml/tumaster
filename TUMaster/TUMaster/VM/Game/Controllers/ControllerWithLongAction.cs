﻿using System.Threading.Tasks;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class ControllerWithLongAction : GameControllerVm
	{
		private LongActionVm longAction;

		public ControllerWithLongAction(string name) : base(name)
		{
		}

		public LongActionVm LongAction
		{
			get
			{
				return longAction;
			}

			set
			{
				longAction = value;
				RaisePropertyChanged();
			}
		}

		protected async Task ExecuteNewActionWithConfig(LongActionVm.LongActionConfiguration config)
		{
			LongAction = new LongActionVm { IsUsedAsView = false };
			LongAction.Init(config);
			await LongAction.ExecuteAsync();
		}
	}
}
