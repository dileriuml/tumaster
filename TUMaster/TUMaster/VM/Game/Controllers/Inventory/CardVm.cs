﻿using TUMaster.Core.Xml.Models;
using TUMaster.Core.Xml.Models.Card;
using TUMaster.VM.Attributes;

namespace TUMaster.VM
{
	public class CardVm : VmBase
	{
		#region Fields

		protected readonly CardDTO cardModel;

		#endregion

		#region Constructor

		public CardVm(CardDTO cardModel) => this.cardModel = cardModel;

		#endregion

		#region Properties

		public int CardId => cardModel.Id;

		[Filterable]
		public Rarity Rarity => cardModel.Rarity;

		[Filterable(Priority = 1)]
		public string Name => cardModel.Name;

		[Filterable]
		public int Health => cardModel.Health;

		[Filterable]
		public int? Attack => cardModel.Attack;

		[Filterable]
		public int? FusionLevel => cardModel.FusionLevel;

		[Filterable]
		public int Level => cardModel.Level;

		[Filterable]
		public int MaxLevel => cardModel.MaxLevel;

		[Filterable]
		public int SpCost => cardModel.SPCost;

		public string CardSet => null;

		public bool IsCommander => cardModel.IsCommander;

		public bool IsDominion => cardModel.IsDominion;

		[Filterable]
		public int Delay => cardModel.Delay;

		[Filterable]
		public string Skills => string.Join<Skill>("\n", cardModel.Skills);

		[Filterable]
		public Faction Faction => cardModel.Faction;

		[Filterable]
		public string Fullinfo => cardModel.ToString();

		#endregion
	}
}
