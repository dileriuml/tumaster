﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross;
using TUMaster.Core.Data;
using TUMaster.Core.Services;

namespace TUMaster.VM
{
	public class DeckVM : VmBase
	{
		#region Fields

		InventoryCardVm commanderCard;
		InventoryCardVm dominionCard;
		readonly InventoryService inventoryService;
		
		#endregion

		#region Ctor

		public DeckVM()
		{
			Cards = new ObservableCollection<InventoryCardVm>();
			inventoryService = Mvx.Resolve<InventoryService>();
		}

		#endregion

		#region Properties

		public ObservableCollection<InventoryCardVm> Cards { get; }

		public InventoryCardVm Commander
		{
			get => commanderCard;
			set => SetProperty(ref commanderCard, value);
		}

		public InventoryCardVm Dominion
		{
			get => dominionCard;
			set => SetProperty(ref dominionCard, value);
		}

		#endregion

		#region Methods

		public void Load(ServiceDeckData deckData, ObservableCollection<InventoryCardVm> cardsInInventory)
		{
			Cards.ClearUsingDispatcher();
			cardsInInventory.Where(x => deckData.Cards.ContainsKey(x.CardId))
											.Select(x => x.Take(deckData.Cards[x.CardId]))
											.ForEach(Cards.AddUsingDispatcher);

			Commander = cardsInInventory.FirstOrDefault(x => x.CardId == deckData.CommanderId).Take(1);
			Dominion = cardsInInventory.FirstOrDefault(x => x.CardId == deckData.DominionId).Take(1);
		}

		public async Task<bool> CommitDeckAsync(ServiceUserData userData)
		{
			var cardsDict = new Dictionary<int, int>();
			Cards.ForEach(x => cardsDict.Add(x.CardId, x.Count));

			var deckToCommit = new ServiceDeckData
			{
				Cards = cardsDict,
				CommanderId = commanderCard.CardId
			};

			return await inventoryService.SetUserDeckAsync(deckToCommit, userData.UserID);
		}

		public void RemoveCard(InventoryCardVm card)
		{
			card.Count--;

			if (card.Count == 0)
			{
				Cards.Remove(card);
			}
		}

		#endregion
	}
}
