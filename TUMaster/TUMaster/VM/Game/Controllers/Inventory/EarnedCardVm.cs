﻿using TUMaster.Core.Xml.Models;
using TUMaster.Core.Xml.Models.Card;

namespace TUMaster.VM.Game.Controllers.Inventory
{
	public class EarnedCardVm : CardVm
	{
		public int Count { get; set; }

		public EarnedCardVm(CardDTO card) : base(card)
		{
		}
	}
}
