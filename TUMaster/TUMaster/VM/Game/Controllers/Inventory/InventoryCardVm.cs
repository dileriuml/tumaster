﻿using System.Text;
using TUMaster.Core.Data;
using TUMaster.Core.Xml.Models.Card;
using TUMaster.VM.Attributes;

namespace TUMaster.VM
{
	#region Enums

	public enum CardManipulationModes
	{
		None,
		Salvage,
		Upgrade
	}

	#endregion

	public class InventoryCardVm : CardVm
	{
		#region Fields

		CardManipulationModes mode = CardManipulationModes.None;
		int countToSalvage;
		int levelToUpgrade;
		ServiceCardData cardData;

		#endregion

		#region Constructor

		public InventoryCardVm(ServiceCardData cardData, CardDTO cardModel) 
			: base(cardModel)
		{
			this.cardData = cardData;
			countToSalvage = Count;
			levelToUpgrade = MaxLevel;
		}

		#endregion

		#region Properties

		public CardManipulationModes Mode
		{
			get => mode;

			set
			{
				mode = value;
				RaisePropertyChanged();
			}
		}
		
		public int CountToUpgradeSalvage
		{
			get => countToSalvage;

			set
			{
				if (value > Count)
					value = Count;

				if (value <= 0)
					value = 1;

				countToSalvage = value;
				RaisePropertyChanged();
			}
		}

		public int LevelToUpgrade
		{
			get => levelToUpgrade;

			set
			{
				if (value < Level)
					value = Level;

				if (value > MaxLevel)
					value = MaxLevel;

				levelToUpgrade = value;
				RaisePropertyChanged();
			}
		}

		public int CountOfLevelsToUpgrade => levelToUpgrade - Level;

		[Filterable]
		public int Count
		{
			get => cardData.Quantity;

			set
			{
				cardData.Quantity = value;
				RaisePropertyChanged();
			}
		}

		[Filterable]
		public string IsLockedByUser => cardData.isLocked ? "Yes" : "No";

		public bool IsLocked
		{
			get => cardData.isLocked;

			set
			{
				cardData.isLocked = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(IsLockedByUser));
			}
		}

		#endregion

		#region Overrided methods

		public override string ToString()
		{
			var builder = new StringBuilder(Name);

			if (Level < MaxLevel)
			{
				builder.AppendFormat("-{0}", Level);
			}

			if (Count > 1)
			{
				builder.AppendFormat(" ({0})", Count);
			}

			return builder.ToString();
		}

		#endregion

		#region Methods

		public InventoryCardVm Take(int count)
		{
			var takenCardData = cardData.Take(count);
			this.RaisePropertyChanged(nameof(Count));
			return new InventoryCardVm(takenCardData, cardModel);
		}

		#endregion
	}
}
