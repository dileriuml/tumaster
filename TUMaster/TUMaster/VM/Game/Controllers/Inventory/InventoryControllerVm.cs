﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using MvvmCross.Logging;
using TUMaster.Core.Constants;
using TUMaster.Core.Services;
using TUMaster.Core.Xml;
using TUMaster.Core.Xml.Models;
using TUMaster.VM.Commands;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class InventoryControllerVm : ControllerWithLongAction
	{
		#region Fields

		bool salvaging;
		XmlGameDb xmlGameDb;
		InventoryService inventoryService;

		#endregion

		#region Constructor

		public InventoryControllerVm(XmlGameDb xmlGameDb, InventoryService inventoryService) : base("Inventory")
		{
			Deck = new DeckVM();
			this.xmlGameDb = xmlGameDb;
			this.inventoryService = inventoryService;
			SalvageCardsCommand = new AsyncCommand<IEnumerable<InventoryCardVm>>(async x => await SalvageCards(x));
			SalvageAllCommand = new AsyncCommand<Rarity>(async x => await SalvageAllAsync(x));
			GetDeckInfoForTOCommand = new Command(ShowDeckInfoForTO);
			UpgradeCardsCommand = new Command<IEnumerable<InventoryCardVm>>(x => UpgradeCards(x));
			CommitDeckCommand = new AsyncCommand(async () => await Deck.CommitDeckAsync(UserAccountData));
			PutCardInDeckCommand = new Command<InventoryCardVm>(PutInDeck);
			PutCardFromDeckCommand = new Command<InventoryCardVm>(PutFromDeck);
			SetCommanderCommand = new Command<InventoryCardVm>(x => Deck.Commander = x);
			SetLockCommand = new AsyncCommand<IEnumerable<InventoryCardVm>>(SetLock);
			Inventory = new ObservableCollection<InventoryCardVm>();
		}

		#endregion

		#region ILoadable implementation

		protected override async Task LoadAsync()
		{
			Inventory.Clear();

			try
			{
				var allCards = await inventoryService.GetInventoryAsync(UserAccountData);

				var allCardsQuery = allCards
					.Where(cardData => cardData.Quantity > 0)
					.Select(cardData => new InventoryCardVm(cardData, xmlGameDb.CardsDb.GetCard(cardData.CardID)))
					.OrderBy(InventoryCardVm => InventoryCardVm.Rarity);

				foreach (InventoryCardVm element in allCardsQuery)
				{
					Inventory.Add(element);
				}

				var deckData = await inventoryService.GetUserDeck(UserAccountData.UserID);
				Deck.Load(deckData, Inventory);
				IsLoaded = true;
			}
			catch (Exception)
			{
				MessageBox.Show("Error while loading cards!\nTry to update xml database!", "Error!");
			}
		}

		public bool IsLoaded { get; private set; }

		#endregion

		#region Properties

		public bool Salvaging
		{
			get => salvaging;
			set => SetProperty(ref salvaging, value);
		}

		public int CardCount => Inventory.Sum(card => card.Count);

		public ObservableCollection<InventoryCardVm> Inventory { get; }

		public DeckVM Deck { get; }

		#endregion

		#region Commands

		public AsyncCommand<IEnumerable<InventoryCardVm>> SalvageCardsCommand { get; }

		public AsyncCommand<Rarity> SalvageAllCommand { get; }

		public Command<IEnumerable<InventoryCardVm>> UpgradeCardsCommand { get; }

		public Command GetDeckInfoForTOCommand { get; }

		public Command<InventoryCardVm> PutCardInDeckCommand { get; }

		public Command<InventoryCardVm> PutCardFromDeckCommand { get; }

		public Command<InventoryCardVm> SetCommanderCommand { get; }

		public AsyncCommand<IEnumerable<InventoryCardVm>> SetLockCommand { get; }

		public AsyncCommand CommitDeckCommand { get; }

		#endregion

		#region Commands actions

		void PutInDeck(InventoryCardVm selectedCard)
		{
			if (selectedCard == null)
				return;

			if (selectedCard.Count > 0)
				Deck.Cards.Add(selectedCard.Take(1));
		}

		void PutFromDeck(InventoryCardVm selectedCard)
		{
			if (selectedCard == null || selectedCard.Count == 0)
				return;

			Deck.RemoveCard(selectedCard);
			Inventory.FirstOrDefault(x => x.CardId == selectedCard.CardId).Count++;
		}

		async void UpgradeCards(IEnumerable<InventoryCardVm> cardsToUpgrade)
		{
			var actionConfig = new LongActionVm.LongActionConfiguration(
				async la =>
				{
					foreach (var element in cardsToUpgrade)
					{
						for (int i = 0; i < element.CountOfLevelsToUpgrade; i++)
						{
							for (int j = 0; j < element.CountToUpgradeSalvage; j++)
							{
								if (la.IsStopped)
									return;

								await inventoryService.UpgradeCardAsync(UserAccountData.UserID, element.CardId + i);
								la.Progress++;
							}
						}
					}
				},
				AsyncLoad,
				maxProgress: cardsToUpgrade.Sum(x => x.CountOfLevelsToUpgrade * x.CountToUpgradeSalvage));
			await ExecuteNewActionWithConfig(actionConfig);
		}

		async Task SalvageCards(IEnumerable<InventoryCardVm> cardsToSalvage)
		{
			var config = new LongActionVm.LongActionConfiguration(
				async la =>
				{
					try
					{
						foreach (var element in cardsToSalvage)
						{
							for (int i = 0; i < element.CountToUpgradeSalvage; i++)
							{
								if (la.IsStopped)
									return;

								await inventoryService.SalvageCardAsync(UserAccountData.UserID, element.CardId);
								la.Progress++;
							}
						}
					}
					catch (Exception ex)
					{
						Log.Log(MvxLogLevel.Error, () => ex.Message);
					}
				},
				AsyncLoad,
				maxProgress: cardsToSalvage.Sum(x => x.CountToUpgradeSalvage),
				message: RequestMessages.Salvage);
			await ExecuteNewActionWithConfig(config);
		}

		async Task SalvageAllAsync(Rarity rarity)
		{
			Salvaging = true;

			var longActionConfig = new LongActionVm.LongActionConfiguration(async i =>
			{
				if (Rarity.Common == rarity)
				{
					await inventoryService.SalvageAllCommonsAsync(UserAccountData.UserID);
				}
				else
				{
					await inventoryService.SalvageAllRaresAsync(UserAccountData.UserID);
				}
			}, message: $"Salvaging all {rarity.ToString()}");

			await ExecuteNewActionWithConfig(longActionConfig);

			Salvaging = false;
			await AsyncLoad();
		}

		void ShowDeckInfoForTO()
		{
			var text = string.Format("Deck : \n{0}\r\nInventory : \n{1}",
									 DeckRule(new[] { Deck.Commander, Deck.Dominion }.Concat(Deck.Cards)),
									 InventoryRule(Inventory));
			ShowInfoMesasge("Deck info for optimizer", text);

			string InventoryRule(IEnumerable<InventoryCardVm> info)
			{
				var epicInfo = info.Where(x => (int)x.Rarity > (int)Rarity.Rare && x.Count > 0);

				var commanders = epicInfo.Where(x => x.IsCommander)
					.OrderByDescending(x => x.FusionLevel)
					.OrderByDescending(x => x.Level);
				
				var shardsAndCores = epicInfo.Where(x =>
					!x.IsCommander &&
					(x.Name.Contains("shard", StringComparison.InvariantCultureIgnoreCase) ||
					x.Name.Contains("core", StringComparison.InvariantCultureIgnoreCase)));

				var singles = epicInfo.Where(x => x.FusionLevel == 0 && !x.IsCommander).Except(shardsAndCores);

				var quadsAndDoubles = epicInfo
					.Where(x => x.FusionLevel >= 1 && !x.IsCommander)
					.OrderByDescending(x => x.FusionLevel).Except(shardsAndCores);

				return string.Join("\r\n",
					quadsAndDoubles
					.Concat(commanders)
					.Concat(singles)
					.Concat(shardsAndCores));
			};

			string DeckRule(IEnumerable<InventoryCardVm> info)
			{
				return string.Join("\r\n",
					info
					.OrderByDescending(x => x.FusionLevel)
					.OrderByDescending(x => x.IsDominion)
					.OrderByDescending(x => x.IsCommander)
					.Where(x => (int)x.Rarity > (int)Rarity.Rare && x.Count > 0));
			};
		}

		async Task SetLock(IEnumerable<InventoryCardVm> xCol)
		{
			IsLoading = true;

			foreach (var x in xCol)
			{
				if (x != null && x.Count > 0 && 
					await inventoryService.SetCardLock(x.CardId, !x.IsLocked, UserAccountData.UserID))
				{
					x.IsLocked = !x.IsLocked;
				}
			}

			IsLoading = false;
		}

		#endregion
	}
}
