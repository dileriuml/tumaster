﻿using TUMaster.Core.Data;

namespace TUMaster.VM
{
	public class ProfileControllerVm : GameControllerVm
	{
		public ProfileControllerVm() : base("Profile")
		{
		}

		public string Password => User.Password;
	}
}
