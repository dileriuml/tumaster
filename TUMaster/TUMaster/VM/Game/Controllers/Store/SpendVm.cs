﻿using System.Collections.Generic;
using System.Linq;
using TUMaster.Core.Data;
using TUMaster.Core.Services;

namespace TUMaster.VM.Game.Controllers.Store
{
	public class SpendVm : VmBase
	{
		private readonly StoreService storeService;
		private readonly UsersCache usersCache;
		private string userId;

		public SpendVm(StoreService storeService, UsersCache usersCache)
		{
			this.storeService = storeService;
			this.usersCache = usersCache;
		}

		public void Init(string userId)
		{
			this.userId = userId;
		}

		private async void DoSpendGold()
		{
			var userData = await usersCache.LoadUser(userId);
			var userStoreInfo = await storeService.GetStoreInfoAsync(userId);
			
			var results = new List<InventoryCardVm>();

			var goldItems = userStoreInfo.Items.Where(x => x.CurrencyType == PackCurrency.Gold).OrderByDescending(x => x.GoldCost);

			var goldLeft = userData.Money;

			foreach (var item in goldItems)
			{
				var availableToBuy = goldLeft / item.GoldCost;
				
				goldLeft -= availableToBuy * item.GoldCost;
			}
		}
	}
}
