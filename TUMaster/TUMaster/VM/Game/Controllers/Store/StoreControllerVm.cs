﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core.Services;
using TUMaster.VM.Commands;
using TUMaster.VM.Game;
using TUMaster.VM.Game.Controllers.Store;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class StoreControllerVm : ControllerWithLongAction
	{
		private const string BuyingActionMessage = "Buying...";

		#region Fields

		private int countToBuy;
		private readonly StoreService storeService;
		private int maxCountToBuy;
		private StoreItemVm selectedItem;

		#endregion

		#region Constructor

		public StoreControllerVm(StoreService storeService) : base("Store")
		{
			this.storeService = storeService;
			countToBuy = 1;
			StoreItems = new ObservableCollection<StoreItemVm>();
			BuyOutCommand = new Command(RunBuyOut);
			BuyCountCommand = new Command(BuyCount);
			SpendGoldCommand = new Command(DoSpendGold);
		}

		#endregion

		#region Overriden

		protected override void OnUserDataUpdated()
		{
			StoreItems.ForEach(UpdateItemCanBeBought);
			RaiseAllPropertiesChanged();
		}

		#endregion

		#region Properties

		public ObservableCollection<StoreItemVm> StoreItems { get; }

		public StoreItemVm SelectedItem
		{
			get => selectedItem;

			set
			{
				selectedItem = value;
				CountToBuy = 0;
				value.With(UpdateItemCanBeBought);
				RaisePropertyChanged(nameof(IsCanBuy));
				UpdateMaxCountOfItemsToBuy();
				RaisePropertyChanged();
			}
		}

		public int CountToBuy
		{
			get
			{
				return countToBuy;
			}

			set
			{
				countToBuy = value;
				RaisePropertyChanged();
			}
		}

		public bool IsCanSpendGold =>
			StoreItems != null &&
			StoreItems.Any(x => x.StoreItem.GoldCost <= UserAccountData.Money);
 
		public int MaxCountToBuy
		{
			get => maxCountToBuy;

			set
			{
				maxCountToBuy = value;
				RaisePropertyChanged();
			}
		}

		public bool IsCanBuy => 
			(LongAction == null || !LongAction.IsPerforming) &&
			SelectedItem != null &&
			SelectedItem.IsBuyable;

		#endregion

		#region Commands

		public Command BuyOutCommand { get; }

		public Command BuyCountCommand { get; }

		public Command SpendGoldCommand { get; }

		#endregion

		#region Private methods

		private void UpdateMaxCountOfItemsToBuy()
		{
			MaxCountToBuy = CalculateMaxCountOfItems();
			CountToBuy = MaxCountToBuy;
		}

		private int CalculateMaxCountOfItems()
		{
			if (SelectedItem == null)
				return 0;

			switch (SelectedItem.StoreItem.CurrencyType)
			{
				case Core.Data.PackCurrency.Gold:
					return UserAccountData.Money / SelectedItem.StoreItem.GoldCost;
					
				case Core.Data.PackCurrency.WB:
					return UserAccountData.Tokens / SelectedItem.StoreItem.WbCost;
			}

			return 0;
		}

		private void UpdateItemCanBeBought(StoreItemVm storeItem)
		{
			switch (storeItem.StoreItem.CurrencyType)
			{
				case Core.Data.PackCurrency.Gold:
					storeItem.IsBuyable = UserAccountData.Money > storeItem.StoreItem.GoldCost;
					break;

				case Core.Data.PackCurrency.WB:
					storeItem.IsBuyable = UserAccountData.Tokens > storeItem.StoreItem.WbCost;
					break;
			}
		}

		#endregion

		#region Command actions

		void DoSpendGold() => NavigationService.Navigate<SpendVm>();

		void RunBuyOut() => NavigationService.Navigate<BuyoutGoldVm>();

		async void BuyCount()
		{
			var config = new LongActionVm.LongActionConfiguration(
				async la =>
				{
					while (la.Progress < la.MaxProgress)
					{
						if (la.IsStopped)
							return;

						var result = await storeService.BuyItemAsync(SelectedItem.StoreItem, UserAccountData.UserID);
						la.Progress++;
						await ReloadUserDataAsync();
					}
				},
				AsyncLoad,
				ProgressShowTypes.Nums,
				CountToBuy, BuyingActionMessage);
			await ExecuteNewActionWithConfig(config);
		}

		#endregion

		#region implemented abstract members of AbstractGameControllerVM

		protected override async Task LoadAsync()
		{
			IsLoaded = false;
			StoreItems.ClearUsingDispatcher();
			var userStoreInfo = await storeService.GetStoreInfoAsync(UserAccountData.UserID);
			userStoreInfo.Items
				.OrderBy(x => x.CurrencyType)
				.ForEach(x => StoreItems.AddUsingDispatcher(new StoreItemVm(x)));
			IsLoaded = true;
		}

		public bool IsLoaded { get; set; }

		#endregion
	}
}
