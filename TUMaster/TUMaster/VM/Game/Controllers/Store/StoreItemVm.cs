﻿using TUMaster.Core.Data;

namespace TUMaster.VM
{
	public class StoreItemVm : VmBase
	{
		private bool isBuyable;

		public StoreItemVm(ServiceStoreItem item)
		{
			StoreItem = item;
		}

		public ServiceStoreItem StoreItem { get; }
		
		public bool IsBuyable
		{
			get => isBuyable;

			set
			{
				isBuyable = value;
				RaisePropertyChanged();
			}
		}
	}
}
