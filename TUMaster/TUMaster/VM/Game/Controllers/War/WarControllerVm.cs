﻿using System;
using System.Threading.Tasks;
using MvvmCross.Logging;
using TUMaster.Core.Data;
using TUMaster.Core.Services;
using TUMaster.Utils;
using TUMaster.VM.Commands;
using TUMaster.VM.Game.Controllers.War;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class WarControllerVm : ControllerWithLongAction
	{
		#region Fields

		private readonly WarService warService;
		private WarInfo warData;
		private string warReport;
		private Slots slot;
		private long timeLeft;
		private int countOfPlays;

		#endregion

		#region Constructor

		public WarControllerVm(WarService warService) : base("War controller")
		{
			this.warService = warService;
			countOfPlays = 0;
			PerformWarBattlesCommand = new AsyncCommand(PerformBattles);
		}

		#endregion

		#region Properties

		public ServiceWarData WarData => warData.WarData;

		public bool IsWar => WarData != null;

		public string ReportWar
		{
			get
			{
				return warReport;
			}

			set
			{
				warReport = value;
				RaisePropertyChanged();
			}
		}

		public bool IsAttacksEnabled => WarData.Energy > 0;

		public TimeSpan WarTimeLeft => TimeSpan.FromSeconds(timeLeft);

		public double TimeLeft
		{
			get
			{
				return timeLeft;
			}

			set
			{
				timeLeft = (long)value;
				RaisePropertyChanged();
			}
		}

		public int CountOfPlays
		{
			get
			{
				return countOfPlays;
			}

			set
			{
				countOfPlays = value;
				RaisePropertyChanged();
			}
		}

		public Slots ChosenSlot
		{
			get
			{
				return slot;
			}

			set
			{
				slot = value;
				RaisePropertyChanged();
			}
		}

		#endregion

		#region Commands

		public AsyncCommand PerformWarBattlesCommand { get; }

		#endregion

		#region ILoadable implementation

		protected override void OnInit()
		{
			warData = usersCache.GetCurrentWarData(User.UserId);
			RaiseAllPropertiesChanged();
		}

		protected override async Task LoadAsync()
		{
			await UpdateTimeLeft();
			IsLoaded = true;
		}

		public bool IsLoaded { get; private set; }

		#endregion

		#region Methods

		private async Task UpdateTimeLeft()
		{
			try
			{
				TimeLeft = warData.WarData.EndTime - await warService.GetServerTimeAsync(UserAccountData.UserID);
				RaisePropertyChanged(nameof(WarTimeLeft));
			}
			catch (Exception ex)
			{
				Log.Log(MvxLogLevel.Error, () => ex.Message);
			}
		}

		protected override void OnUserDataUpdated()
		{
			warData = usersCache.GetCurrentWarData(User.UserId);
			RaiseAllPropertiesChanged();
		}

		public void SetSlotFromEnergyAction(string energyAction)
			=> ChosenSlot = energyAction.SlotFromSpendEnergyAction();

		public async Task PerformBattles()
		{
			var config = new LongActionVm.LongActionConfiguration(
				async la => {
					ReportWar = string.Empty;

					while (la.Progress < la.MaxProgress)
					{
						if (la.IsStopped)
							return;

						var resp = await warService.FightWarBattleAsync(ChosenSlot, UserAccountData.UserID);

						try
						{
							if (!resp.Response.Result)
							{
								continue;
							}
							else
							{
								ReportWar += resp.BattleData.IsWinner ? "Win!, " : "Lost!, ";
								ReportWar += string.Format("Slot : {0}, ", ChosenSlot);

								var rewards = resp.BattleData.Rewards;

								if (rewards != null && rewards.Count > 0)
									ReportWar += string.Format("Damage : {0}, Rate : {1}\n", rewards[0].SlotDamage, rewards[0].WarPoints);
							}
						}
						catch
						{
							ReportWar += "Failed!\n";
							continue;
						}

						la.Progress++;
						await ReloadUserDataAsync();
						await Task.Delay(3000);

						if (la.IsStopped)
							return;
					}
				},
				async () => await AsyncLoad(),
				ProgressShowTypes.Nums,
				CountOfPlays);
			await ExecuteNewActionWithConfig(config);
		}

		#endregion
	}
}
