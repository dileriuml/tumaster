﻿using System.Collections.Generic;
using TUMaster.Core.Data;

namespace TUMaster.VM.Game.Controllers.War
{
	public static class WarUtils
	{
		private static readonly Dictionary<string, Slots> EnergyActionToSlotMapping
			= new Dictionary<string, Slots>
		{
			{ BattleEnergyActions.WAR_BOTTOM_DEF_ATTACK, Slots.BottomDefence },
			{ BattleEnergyActions.WAR_BOTTOM_SIEGE_ATTACK, Slots.BottomSiege },
			{ BattleEnergyActions.WAR_CORE_ATTACK, Slots.Core },
			{ BattleEnergyActions.WAR_TOP_DEF_ATTACK, Slots.TopDefence },
			{ BattleEnergyActions.WAR_TOP_SIEGE_ATTACK, Slots.TopSiege }
		};

		public static Slots SlotFromSpendEnergyAction(this string energyAction)
			=> EnergyActionToSlotMapping[energyAction];
	}
}
