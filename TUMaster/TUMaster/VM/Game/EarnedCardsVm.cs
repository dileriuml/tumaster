﻿using System.Collections.Generic;
using System.Linq;
using MvvmCross.ViewModels;
using TUMaster.Core.Xml;
using TUMaster.VM.Game.Controllers.Inventory;

namespace TUMaster.VM.Game
{
	public class EarnedCardsVm : VmBase, IMvxViewModel<List<(int cardId, int count)>>
	{
		private XmlGameDb cardsDb;

		public EarnedCardsVm(XmlGameDb cardsDb)
		{
			this.cardsDb = cardsDb;
		}

		public EarnedCardVm[] CardsEarned { get; private set; }

		public void Prepare(List<(int cardId, int count)> cards)
		{
			CardsEarned = cards.Select(card => new EarnedCardVm(cardsDb.CardsDb.GetCard(card.cardId))
			{
				Count = card.count
			}).ToArray();
		}
	}
}
