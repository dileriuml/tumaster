﻿using MvvmCross.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TUMaster.Core;
using TUMaster.Core.Data;
using TUMaster.Core.Data.Brawls;
using TUMaster.Core.Data.Conquest;
using TUMaster.Core.Services;
using TUMaster.VM.Commands;
using TUMaster.VM.Game.CommonVm;
using TUMaster.VM.Game.Controllers.War;
using TUMaster.VM.Special;

namespace TUMaster.VM
{
	public class GameItemVm : MvxViewModel, IMvxViewModel<string>
	{
		#region Fields

		private BotManager botManager;
		private WarManager warManager;
		private UsersCache usersCache;
		private ServiceUserData userData;
		private PvpBattleService battleService;
		private ConquestService conquestService;
		private RaidService raidService;

		private bool isSelected;
		private ZoneVm selectedZone;
		private Bot bot;

		private WarInfo warInfo;
		private int countOfPlays;
		private int countOfBrawlsToPlay;
		private int countOfConquestsToPlay;
		private int countOfRaidToPlay;

		#endregion

		#region Properties

		public Command ShowCurrentUserCommand { get; }

		public string UserId => userData.UserID;

		public string Name => userData.Name;

		public string FactionName => userData.FactionName;

		public int Energy => userData.Energy;

		public int Stamina => userData.Stamina;

		public int BattleEnergy => userData.BattleEnergy;

		public int WarEnergy => warInfo.WarData?.Energy ?? 0;

		public int MaxWarEnergy => warInfo.WarData?.MaxEnergy ?? 0;

		public int MaxEnergy => userData.MaxEnergy;

		public int MaxStamina => userData.MaxStamina;

		public bool CanUseEnergy => WarEnergy != 0;

		public bool CanUseBrawls => BrawlsInfo.BattleEnergy != 0;

		public ServiceBrawlsData BrawlsInfo { get; private set; }

		public ServiceRaidStatusData RaidData { get; private set; }

		public ServiceConquestData ConquestData { get; private set; }

		public BotStatus BotStatus => bot.BotStatus;

		public bool WarIsPresent => warInfo?.WarData != null;

		public bool IsWarClaimRewardTime => warInfo?.IsRewardsTime ?? false;

		public bool IsWarRewardsNotClaimed => warInfo?.WarEventInfo?.CurrentMember?.IsRewardsNotClaimed ?? false;

		public bool IsConquestRewardsNotClaimed => ConquestData?.IsRewardsNotClaimed ?? false;

		public BotBlockingStatus BlockingStatus => bot.BlockingStatus;

		public ILongProgress LongProgress { get; }

		public bool IsSelected
		{
			get => isSelected;
			set => SetProperty(ref isSelected, value);
		}

		public int CountOfPlays
		{
			get => countOfPlays;
			set
			{
				SetProperty(ref countOfPlays, value);
				UpdateProgressVm();
			}
		}

		public int CountOfBrawlsToPlay
		{
			get => countOfBrawlsToPlay;

			set
			{
				value = Math.Min(value, BrawlsInfo.BattleEnergy);
				SetProperty(ref countOfBrawlsToPlay, value);
				RaisePropertyChanged(nameof(CanUseBrawls));
			}
		}

		public int CountOfConquestToPlay
		{
			get => countOfConquestsToPlay;
			set => SetProperty(ref countOfConquestsToPlay, value);
		}

		public int CountOfRaidToPlay
		{
			get => countOfRaidToPlay;
			set => SetProperty(ref countOfRaidToPlay, value);
		}

		public ZoneVm SelectedZone
		{
			get => selectedZone;

			set
			{
				value = Zones?.FirstOrDefault(x => x.ZoneInfo.Id == value.ZoneInfo.Id);
				SetProperty(ref selectedZone, value);
			}
		}

		public List<ZoneVm> Zones { get; set; }

		#endregion

		#region Constructor

		public GameItemVm(
			BotManager botManager, 
			UsersCache usersCache, 
			WarManager warManager,
			PvpBattleService battlesService,
			ConquestService conquestService,
			RaidService raisService)
		{
			this.botManager = botManager;
			this.warManager = warManager;
			this.usersCache = usersCache;
			this.battleService = battlesService;
			this.conquestService = conquestService;
			this.raidService = raisService;
			LongProgress = new ProgressViewModelBase();
			ShowCurrentUserCommand = new Command(DoShowCurrentUserGame);
		}

		#endregion

		#region Handlers

		private void OnUpdateUserData(AccountResponse resp)
		{
			if (resp == AccountResponse.Success)
			{
				SetInfoFromUsersCache(UserId);
				RaiseAllPropertiesChanged();
			}
		}

		private void SetInfoFromUsersCache(string userId)
		{
			userData = usersCache.GetCurrentData(userId);
			warInfo = usersCache.GetCurrentWarData(userId);
			BrawlsInfo = usersCache.GetCurrentBrawlsData(userId);
			ConquestData = usersCache.GetCurrentConquestData(userId);
			RaidData = usersCache.GetCurrentRaidData(userId);

			if (ConquestData != null)
			{
				UpdateZones();
			}

			UpdateProgressVm();
		}

		private void UpdateZones()
		{
			if (Zones == null)
			{
				Zones = ConquestData.Zones.Select(zoneData => new ZoneVm(zoneData)).ToList();
				SelectedZone = Zones.FirstOrDefault();
			}
			else
			{
				Zones.ForEach(zoneVm =>
				{
					zoneVm.ZoneInfo = ConquestData.Zones.FirstOrDefault(z => z.Id == zoneVm.ZoneInfo.Id);
				});
			}
		}

		private void UpdateProgressVm()
		{
			LongProgress.MaxProgress = Math.Min(WarEnergy, CountOfPlays);
		}

		private void OnBotInfoUpdated(object sender, string[] props) => props.ForEach(RaisePropertyChanged);

		private void DoShowCurrentUserGame() => NavigationService.Navigate<GameVm, string>(UserId);

		#endregion

		public Task RunBot() => bot.TryRunBot();

		public void StopBot() => bot.ForceStopBot();

		public Task UnblockBot() => bot.UnblockAsync();

		public Task BlockBot() => bot.BlockAsync();

		public async Task ClaimConquestReward()
		{
			await conquestService.ClaimConquestRewards(UserId);
			await UpdateUserDataAsync();
		}

		public async Task ClaimBrawlReward()
		{
			await battleService.ClaimBrawlReward(UserId);
			await UpdateUserDataAsync();
		}

		public async Task ClaimWarReward()
		{
			await warManager.ClaimWarReward(UserId);
			await UpdateUserDataAsync();
		}

		public async Task ClaimRaidReward()
		{
			await raidService.ClaimRaidRewardAsync(UserId, RaidData.RaidId);
			await UpdateUserDataAsync();
		}

		public async Task DoBrawl()
		{
			for (int i = 0; i < CountOfBrawlsToPlay; i++)
			{
				await battleService.FightBrawlBattleAsync(UserId);
				await UpdateUserDataAsync();
			}
		}

		public async Task SpendConquestEnergy()
		{
			for (int i = 0; i < countOfConquestsToPlay; i++)
			{
				await conquestService.FightConquestBattleAsync(UserId, SelectedZone.ZoneInfo);
				await UpdateUserDataAsync();
			}
		}

		public async Task SpendRaidEnergy()
		{
			int lastLevel = RaidData.Level;

			for (int i = 0; i < countOfRaidToPlay; i++)
			{
				//Check if level changed to stop attacking
				if (lastLevel != RaidData.Level)
				{
					break;
				}

				lastLevel = RaidData.Level;
				await raidService.DoRaidBattleAsync(UserId, RaidData);
				await UpdateUserDataAsync();
				await Task.Delay(2000);
				//Interval between battles
			}
		}

		public void Prepare(string userId)
		{
			bot = botManager.GetUserBot(userId);
			bot.BotInfoUpdated += OnBotInfoUpdated;
			SetInfoFromUsersCache(userId);
			usersCache.AddUpdateHandler(userId, OnUpdateUserData);
		}

		public async Task SpendBattleEnergy(string currentSpendEnergyAction)
		{
			switch (currentSpendEnergyAction)
			{
				case BattleEnergyActions.WAR_BOTTOM_DEF_ATTACK:
				case BattleEnergyActions.WAR_BOTTOM_SIEGE_ATTACK:
				case BattleEnergyActions.WAR_CORE_ATTACK:
				case BattleEnergyActions.WAR_TOP_DEF_ATTACK:
				case BattleEnergyActions.WAR_TOP_SIEGE_ATTACK:
					await warManager.PerformBattlesAsync(LongProgress, currentSpendEnergyAction.SlotFromSpendEnergyAction(), UserId);
					break;
			}

			LongProgress.Progress = 0;
			UpdateProgressVm();
		}

		private async Task UpdateUserDataRaidAsync() => await usersCache.LoadUser(UserId, false, false, false, true, false);

		private async Task UpdateUserDataBrawlAsync() => await usersCache.LoadUser(UserId, false, true, false, false, false);

		private async Task UpdateUserDataAsync() => await usersCache.LoadUser(UserId);
	}
}
