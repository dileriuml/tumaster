﻿using System.Collections.Generic;
using System.Windows.Input;
using MvvmCross.ViewModels;
using TUMaster.Core.Data;
using TUMaster.Core.Services;

namespace TUMaster.VM
{
	public class GameVm : VmBase, IMvxViewModel<string>
	{
		#region Fields

		int currentController;
		UsersCache usersCache;
		BotManager botManager;

		#endregion

		#region Constructor

		public GameVm(UsersCache usersCache, 
			ProfileControllerVm profileController,
			InventoryControllerVm inventoryController,
			BotControllerVm botController,
			WarControllerVm warController,
			StoreControllerVm storeController,
			BotManager botManager)
		{
			ProfileController = profileController;
			InventoryController = inventoryController;
			BotController = botController;
			WarController = warController;
			StoreController = storeController;
			this.botManager = botManager;

			Controllers = new List<GameControllerVm> {
				ProfileController,
				InventoryController,
				BotController,
				WarController,
				StoreController
			};

			this.usersCache = usersCache;
		}

		#endregion

		#region Init

		public void Prepare(string userId)
		{
			UserData = usersCache.GetCurrentData(userId);
			usersCache.AddUpdateHandler(userId, OnUserDataUpdated);
			Controllers.ForEach(x => x.Prepare(userId));

			if (!WarController.IsWar)
			{
				Controllers.Remove(WarController);
			}
		}

		#endregion

		#region Properties

		public ServiceUserData UserData { get; private set; }

		public List<GameControllerVm> Controllers { get; }

		public ProfileControllerVm ProfileController { get; }

		public StoreControllerVm StoreController { get; }

		public InventoryControllerVm InventoryController { get; }

		public BotControllerVm BotController { get; }

		public WarControllerVm WarController { get; }

		public int CurrentController
		{
			get
			{
				return currentController;
			}

			set
			{
				currentController = value;
				RaisePropertyChanged();
				Controllers[value].AsyncLoad();
			}
		}

		#endregion

		#region Commands

		public ICommand RemoveUserCommand { get; set; }

		#endregion

		private void OnUserDataUpdated(AccountResponse result)
		{
			if (result == AccountResponse.Success)
			{
				RaisePropertyChanged(nameof(UserData));
			}
			else
			{
				//TODO: Add appropriate dialogs
			}
		}
	}
}
