﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using MvvmCross.ViewModels;
using TUMaster.Core.Data;
using TUMaster.Core.Extensions;
using TUMaster.Core.Network;
using TUMaster.Core.Services;
using TUMaster.Core.Services.Botting;
using TUMaster.Core.Xml;
using TUMaster.VM.Commands;
using TUMaster.VM.Game.CommonVm;
using TUMaster.VM.Special;

namespace TUMaster.VM.Game
{
	public class MainVm : VmBase
	{
		#region Fields

		private readonly ConnectionManager connectionManager;
		private readonly UsersCredsManager usersManager;
		private readonly ProfileService profileService;
		private readonly PvpBattleService battleService;
		private readonly ConquestService conquestService;
		private readonly RaidService raidService;
		private readonly BotManager botManager;
		private readonly WarManager warManager;
		private readonly XmlGameDb xmlDb;
		private readonly IMvxViewModelLoader vmLoader;
		private readonly UsersCredsManager credsManager;

		private TaskCompletionSource<int> waitingForConnection;
		private GameItemVm currentGame;
		private UsersCache usersCache;
		private string currentSpendEnergyAction;
		private int currentBrawlsMaxSpend;
		private int countOfBrawlsToSpend;
		private int countOfRaidsToSpend;
		private int currentConquestMaxSpend;
		private int currentRaidMaxToSpend;
		private int currentConquestToSpend;
		private ZoneVm selectedZone;
		private IEnumerable<GameItemVm> selectedGames;

		private bool isLoading;
		private bool connectionIsUp;

		#endregion

		#region Init&destroy

		public MainVm(
			ConnectionManager connectionManager,
			UsersCredsManager usersManager,
			ProfileService profileService,
			PvpBattleService battleService,
			ConquestService conquestService,
			RaidService raidService,
			UsersCache usersCache,
			BotManager botManager,
			WarManager warManager,
			XmlGameDb xmlDb,
			UsersCredsManager credsManager,
			IMvxViewModelLoader vmLoader)
		{
			this.vmLoader = vmLoader;
			this.usersCache = usersCache;
			this.connectionManager = connectionManager;
			this.usersManager = usersManager;
			this.profileService = profileService;
			this.battleService = battleService;
			this.conquestService = conquestService;
			this.raidService = raidService;
			this.botManager = botManager;
			this.warManager = warManager;
			this.xmlDb = xmlDb;
			this.credsManager = credsManager;

			connectionManager.ConnectionStatusChanged += ConnectionManagerConnectionStatusChanged;
			waitingForConnection = new TaskCompletionSource<int>();
			ConnectionManagerConnectionStatusChanged(connectionManager.ConnectionStatus);

			AddUserGameCommand = new AsyncCommand(AddUserGameAsync);
			UpdateXMLCommand = new Command(UpdateXML);
			ShowAboutWindow = new Command(DoShowAboutWindow);
			SaveCommand = new Command(PerformSave);

			Games = new ObservableCollection<GameItemVm>();
			LaunchBotsForGamesCommand = new Command(() => LaunchBots(true));
			StopBotsForGamesCommand = new Command(() => StopBots(true));
			UnblockBotsForGamesCommand = new Command(() => SwitchBlockStatus(true, false));
			BlockBotsForGamesCommand = new Command(() => SwitchBlockStatus(true, true));
			BlockAllBotsCommand = new Command(() => SwitchBlockStatus(false, true));
			UnblockAllBotsCommand = new Command(() => SwitchBlockStatus(false, false));
			StopAllBotsCommand = new Command(() => StopBots(false));
			RunAllBotsCommand = new Command(() => LaunchBots(false));
			SpendEnergyForGamesCommand = new Command(DoSpendBattleEnergy);
			SpendBrawlsEnergyForGamesCommand = new Command(DoSpendBrawlsEnergy);
			RefreshAllCommand = new Command(DoRefreshAllCommand);
			ShowCardsDbCommand = new Command(DoShowCardsDbCommand);
			SpendConquestEnergyForGamesCommand = new Command(DoSpendConquestEnergyCommand);
			SpendRaidEnergyForGamesCommand = new Command(DoSpendRaidEnergyCommand);
			ClaimBrawlRewardCommand = new Command(DoClaimBrawlReward);
			ClaimRaidRewardCommand = new Command(DoClaimRaidReward);
			ClaimWarRewardCommand = new Command(DoClaimWarReward);
			ClaimConquestRewardCommand = new Command(DoClaimConquestReward);
		}

		public override async Task Initialize()
		{
			IsLoading = true;
			await waitingForConnection.Task;
			waitingForConnection = null;

			var users = usersManager.GetUsers();

			await usersCache.LoadUsersFromManagerAsync();

			users = usersManager.GetUsers();
			users.Where(x => usersCache.ContainsUser(x.UserId))
				.ForEach(OnNewUserAdded);

			Games.CollectionChanged += GamesCollectionChanged;
			currentGame = Games.FirstOrDefault();
			IsLoading = false;
			UpdateDataAvailability();
		}

		#endregion

		#region Handlers

		private void GamesCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
		{
			switch (e.Action)
			{
				case NotifyCollectionChangedAction.Remove:
					foreach (var game in e.OldItems.OfType<GameItemVm>())
					{
						var gameUser = game.UserId;
						usersManager.RemoveUser(game.UserId);
						usersCache.RemoveUser(game.UserId);
					}

					break;
			}

			UpdateDataAvailability();
		}

		private void OnNewUserAdded(User newUser)
		{
			botManager.AddBot(newUser.UserId);
			var gameVm = vmLoader.LoadVm<GameItemVm, string>(newUser.UserId);
			Games.AddUsingDispatcher(gameVm);
		}

		private void PerformSave()
		{
			botManager.AllBots
				.ForEach(bot =>
				{
					var config = BotConfiguration.ProduceFromBot(bot);
					var user = usersManager.GetUserById(bot.UserId);
					user.BotConfiguration = config;
				});

			usersManager.SaveUsers();
		}

		#endregion

		#region Properties

		public bool IsLoading
		{
			get => isLoading;
			set => SetProperty(ref isLoading, value);
		}

		public ObservableCollection<GameItemVm> Games { get; }

		public IEnumerable<GameItemVm> SelectedGames
		{
			get => selectedGames;

			set
			{
				selectedGames = value;
				UpdateRewardsAvailability();
			}
		}

		public string CurrentSpendEnergyAction
		{
			get => currentSpendEnergyAction;
			set => SetProperty(ref currentSpendEnergyAction, value);
		}

		public ZoneVm SelectedZone
		{
			get => selectedZone;
			set
			{
				SetProperty(ref selectedZone, value);
				UpdateSelectedZoneForSelectedGames();
			}
		}

		public int CurrentBrawlsMaxSpend
		{
			get => currentBrawlsMaxSpend;
			set => SetProperty(ref currentBrawlsMaxSpend, value);
		}

		public int CurrentConquestMaxSpend
		{
			get => currentConquestMaxSpend;
			set => SetProperty(ref currentConquestMaxSpend, value);
		}

		public int CurrentRaidMaxToSpend
		{
			get => currentRaidMaxToSpend;
			set => SetProperty(ref currentRaidMaxToSpend, value);
		}

		public int CountOfRaidsToSpend
		{
			get => countOfRaidsToSpend;

			set
			{
				SetProperty(ref countOfRaidsToSpend, value);
				UpdateRaidsValueOnSelectedGames();
			}
		}

		public int CurrentConquestToSpend
		{
			get => currentConquestToSpend;

			set
			{
				SetProperty(ref currentConquestToSpend, value);
				UpdateConquestValueOnSelectedGames();
			}
		}

		public int CountOfBrawlsToSpend
		{
			get => countOfBrawlsToSpend;
			set
			{
				SetProperty(ref countOfBrawlsToSpend, value);
				UpdateBrawlsValueOnSelectedGames();
			}
		}

		private void UpdateRaidsValueOnSelectedGames()
		{
			SelectedGames?.ForEach(giv => giv.CountOfRaidToPlay = countOfRaidsToSpend);
		}

		private void UpdateBrawlsValueOnSelectedGames()
		{
			SelectedGames?.ForEach(giv => giv.CountOfBrawlsToPlay = countOfBrawlsToSpend);
		}

		private void UpdateConquestValueOnSelectedGames()
		{
			SelectedGames?.ForEach(giv => giv.CountOfConquestToPlay = currentConquestToSpend);
		}

		private void UpdateSelectedZoneForSelectedGames()
		{
			SelectedGames?.ForEach(giv => giv.SelectedZone = selectedZone);
		}

		public GameItemVm CurrentGame
		{
			get => currentGame;
			set => SetProperty(ref currentGame, value);
		}

		public bool IsConnectionUp
		{
			get => connectionIsUp;
			set => SetProperty(ref connectionIsUp, value);
		}

		public bool IsAnyBrawl => Games?.Any(x => x.BrawlsInfo != null) ?? false;

		public bool IsAnyWar => Games?.Any(x => x.WarIsPresent) ?? false;

		public bool IsAnyConquest => Games?.Any(x => x.ConquestData != null) ?? false;

		public bool IsAnyRaid => Games?.Any(x => x.RaidData != null) ?? false;

		public bool IsBrawlRewardsForSelectedNotClaimed => SelectedGames != null && SelectedGames.All(x => x.BrawlsInfo.IsRewardsNotClaimed);

		public bool IsRaidRewardsForSelectedNotClaimed => SelectedGames != null && SelectedGames.All(x => x.RaidData.IsNotClaimedReward);

		public bool IsWarRewardsForSelectedNotClaimed => SelectedGames != null && SelectedGames.All(x => x.IsWarRewardsNotClaimed);

		public bool IsConquestRewardsForSelectedNotClaimed => SelectedGames != null && SelectedGames.All(x => x.IsConquestRewardsNotClaimed);

		public bool IsWarRewardsTime => Games != null && (Games.FirstOrDefault()?.IsWarClaimRewardTime ?? false);

		#endregion

		#region Commands

		public Command ShowCardsDbCommand { get; }

		public AsyncCommand AddUserGameCommand { get; }

		public Command UpdateXMLCommand { get; }

		public Command ShowAboutWindow { get; }

		public Command SaveCommand { get;  }

		public Command LaunchBotsForGamesCommand { get; }

		public Command StopBotsForGamesCommand { get; }

		public Command UnblockBotsForGamesCommand { get; }

		public Command BlockBotsForGamesCommand { get; }

		public Command BlockAllBotsCommand { get; }

		public Command UnblockAllBotsCommand { get; }

		public Command StopAllBotsCommand { get; }

		public Command RunAllBotsCommand { get; }

		public Command SpendEnergyForGamesCommand { get; }

		public Command SpendBrawlsEnergyForGamesCommand { get; }

		public Command SpendConquestEnergyForGamesCommand { get; }

		public Command SpendRaidEnergyForGamesCommand { get; }

		public Command ClaimBrawlRewardCommand { get; }

		public Command ClaimRaidRewardCommand { get; }

		public Command ClaimConquestRewardCommand { get; }

		public Command ClaimWarRewardCommand { get; }

		public Command RefreshAllCommand { get; }

		#endregion

		#region Methods

		private void ConnectionManagerConnectionStatusChanged(ConnectionStatus status)
		{
			switch (status)
			{
				case ConnectionStatus.Online:
					IsConnectionUp = true;
					waitingForConnection?.SetResult(0);
					break;
				case ConnectionStatus.Offline:
					IsConnectionUp = false;
					break;
			}
		}

		private void UpdateXML() => NavigationService.Navigate<UpdateXmlVm>();

		private void LaunchBots(bool selectedOnly) =>
			(selectedOnly ? SelectedGames : Games)
			.ForEach(b => b.RunBot());

		private void StopBots(bool selectedOnly) => 
			(selectedOnly ? SelectedGames : Games)
			.ForEach(b => b.StopBot());

		private void SwitchBlockStatus(bool selectedOnly, bool isBlocked) => 
			(selectedOnly ? SelectedGames : Games)
			.ForEach(b =>
			{
				if (isBlocked)
				{
					b.BlockBot();
				}
				else
				{
					b.UnblockBot();
				}
			});

		private async void DoSpendBattleEnergy()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var warTasks = SelectedGames.Select(async b => await b.SpendBattleEnergy(CurrentSpendEnergyAction));
			await Task.WhenAll(warTasks);

			IsLoading = false;
		}

		private void UpdateDataAvailability()
		{
			RaisePropertyChanged(nameof(IsAnyBrawl));
			RaisePropertyChanged(nameof(IsAnyWar));
			RaisePropertyChanged(nameof(IsAnyConquest));
			RaisePropertyChanged(nameof(IsAnyRaid));
			RaisePropertyChanged(nameof(IsWarRewardsTime));
			UpdateRewardsAvailability();
			UpdateCommonInfo();
		}

		private void UpdateCommonInfo()
		{
			CurrentBrawlsMaxSpend = Games
				.Select(x => x.BrawlsInfo?.BattleEnergy ?? 0)
				.Max();

			CurrentConquestMaxSpend = Games
				.Select(x => x.ConquestData?.Energy ?? 0)
				.Max();

			CurrentRaidMaxToSpend = Games
				.Select(x => x.RaidData?.Energy ?? 0)
				.Max();

			SelectedZone = Games
				.Select(x => x.SelectedZone)
				.FirstOrDefault();
		}

		private void UpdateRewardsAvailability()
		{
			RaisePropertyChanged(nameof(IsBrawlRewardsForSelectedNotClaimed));
			RaisePropertyChanged(nameof(IsRaidRewardsForSelectedNotClaimed));
			RaisePropertyChanged(nameof(IsWarRewardsForSelectedNotClaimed));
			RaisePropertyChanged(nameof(IsConquestRewardsForSelectedNotClaimed));
		}

		private async void DoSpendBrawlsEnergy()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var brawlTasks = SelectedGames.Select(i => i.DoBrawl());
			await Task.WhenAll(brawlTasks);

			IsLoading = false;
		}

		private async void DoSpendConquestEnergyCommand()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var conquestTasks = SelectedGames.Select(b => b.SpendConquestEnergy());
			await Task.WhenAll(conquestTasks);

			IsLoading = false;
		}

		private async void DoSpendRaidEnergyCommand()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var conquestTasks = SelectedGames.Select(b => b.SpendRaidEnergy());
			await Task.WhenAll(conquestTasks);

			IsLoading = false;
		}

		private void DoShowAboutWindow() => NavigationService.Navigate<AboutVm>();

		private void DoShowCurrentUserGame() => NavigationService.Navigate<GameVm, string>(CurrentGame.UserId);

		private void DoShowCardsDbCommand() => NavigationService.Navigate<CardsDbVm>();

		private async void DoClaimConquestReward()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var claimTasks = SelectedGames.Select(async x => await x.ClaimConquestReward());
			await Task.WhenAll(claimTasks);

			RaisePropertyChanged(nameof(IsBrawlRewardsForSelectedNotClaimed));
			IsLoading = false;
		}

		private async void DoClaimBrawlReward()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var claimTasks = SelectedGames.Select(async x => await x.ClaimBrawlReward());
			await Task.WhenAll(claimTasks);

			RaisePropertyChanged(nameof(IsBrawlRewardsForSelectedNotClaimed));
			IsLoading = false;
		}

		private async void DoClaimRaidReward()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var claimTasks = SelectedGames.Select(async x => await x.ClaimRaidReward());
			await Task.WhenAll(claimTasks);

			RaisePropertyChanged(nameof(IsRaidRewardsForSelectedNotClaimed));
			IsLoading = false;
		}

		private async void DoClaimWarReward()
		{
			if (SelectedGames == null)
				return;

			IsLoading = true;

			var claimTasks = SelectedGames.Select(async x => await x.ClaimWarReward());
			await Task.WhenAll(claimTasks);

			RaisePropertyChanged(nameof(IsWarRewardsForSelectedNotClaimed));
			IsLoading = false;
		}

		private async Task AddUserGameAsync()
		{
			var user = await NavigationService.Navigate<UserAddingVm, User>();

			if (user == null)
			{
				return;
			}

			IsLoading = true;

			credsManager.AddUser(user);
			await usersCache.LoadUser(user.UserId);
			OnNewUserAdded(user);

			IsLoading = false;
		}

		private async void DoRefreshAllCommand()
		{
			IsLoading = true;

			var refreshUserTasks = Games.Select(item => usersCache.LoadUser(item.UserId));
			await Task.WhenAll(refreshUserTasks);
			UpdateDataAvailability();

			IsLoading = false;
		}

		#endregion
	}
}
