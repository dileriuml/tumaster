﻿using System;
using System.Threading.Tasks;
using TUMaster.Core;
using TUMaster.VM.Commands;

namespace TUMaster.VM.Special
{
	public class LongActionVm : ProgressViewModelBase
	{
		#region Fields

		Func<ILongProgress, Task> longAction;
		Func<Task> afterAction;

		#endregion

		#region Constructor

		public LongActionVm()
		{

		}

		public void Init(LongActionConfiguration longActionConfig)
		{
			longAction = longActionConfig.LongAction;
			afterAction = longActionConfig.AfterAction;
			ShowType = longActionConfig.ShowType;
			MaxProgress = longActionConfig.MaxProgress;
			Message = longActionConfig.Message;
			StopCommand = new Command(() => IsStopped = true);
		}

		#endregion

		#region Properties

		public bool IsUsedAsView { get; set; } = true;

		public ProgressShowTypes ShowType { get; private set; }

		#endregion

		#region Commands

		public Command StopCommand { get; private set; }

		#endregion

		#region Methods

		public async Task ExecuteAsync()
		{
			IsStopped = false;
			await longAction(this);

			if (afterAction != null)
			{
				await afterAction();
			}

			IsStopped = true;

			if (IsUsedAsView)
			{
				await NavigationService.Close(this);
			}
		}

		#endregion

		public class LongActionConfiguration
		{
			public Func<ILongProgress, Task> LongAction { get; }
			public Func<Task> AfterAction { get; }
			public int MaxProgress { get; }
			public ProgressShowTypes ShowType { get; }
			public string Message { get; }

			public LongActionConfiguration(
				Func<ILongProgress, Task> longAction,
				Func<Task> afterAction = null, 
				ProgressShowTypes showType = ProgressShowTypes.Percentage, 
				int maxProgress = 100,
				string message = "Loading")
			{
				LongAction = longAction;
				AfterAction = afterAction;
				ShowType = showType;
				MaxProgress = maxProgress;
				Message = message;
			}
		}
	}
}
