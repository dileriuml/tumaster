﻿using TUMaster.Core;

namespace TUMaster.VM.Special
{
	public class ProgressViewModelBase : VmBase, ILongProgress
	{
		private int progress = 0;
		private int maxProgress = 100;
		private string message = "Loading";
		private bool isStopped;

		public int Progress
		{
			get
			{
				return progress;
			}

			set
			{
				progress = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(ProgressWeight));
				RaisePropertyChanged(nameof(ProgressPercentage));
			}
		}

		public decimal ProgressWeight => (decimal)progress / maxProgress;

		public int ProgressPercentage => (int)(ProgressWeight * 100);

		public bool IsStopped
		{
			get
			{
				return isStopped;
			}

			protected set
			{
				isStopped = value;
				RaisePropertyChanged();
				RaisePropertyChanged(nameof(IsPerforming));
			}
		}

		public string Message
		{
			get
			{
				return message;
			}

			set
			{
				message = value;
				RaisePropertyChanged();
			}
		}

		public int MaxProgress
		{
			get
			{
				return maxProgress;
			}

			set
			{
				maxProgress = value;
				RaisePropertyChanged();
			}
		}

		public bool IsPerforming => !isStopped;

		#region ICancelationHost implementation

		public bool CheckActionIsCanceled() => isStopped;

		#endregion
	}
}
