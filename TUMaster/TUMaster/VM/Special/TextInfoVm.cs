﻿using MvvmCross.ViewModels;

namespace TUMaster.VM.Special
{
	public class TextInfoVm : VmBase, IMvxViewModel<(string title, string text)>
	{
		public string InfoName { get; set; }

		public string WholeinfoText { get; set; }

		public void Prepare((string title, string text) parameter)
		{
			InfoName = parameter.title;
			WholeinfoText = parameter.text;
		}
	}
}
