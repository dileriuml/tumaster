﻿using System.Threading.Tasks;
using TUMaster.Core.Network;
using TUMaster.Core.Services;
using TUMaster.Core.Xml;

namespace TUMaster.VM.Special
{
	public class UpdateXmlVm : ProgressViewModelBase
	{
		private XmlGameDb xmlDb;
		private ConnectionManager connectionManager;
		private TaskCompletionSource<int> waitingForConnection;

		public UpdateXmlVm(XmlGameDb xmlDb, UsersCredsManager usersManager, ConnectionManager connectionManager)
		{
			this.xmlDb = xmlDb;
			this.connectionManager = connectionManager;
			connectionManager.ConnectionStatusChanged += ConnectionManagerConnectionStatusChanged;
			waitingForConnection = new TaskCompletionSource<int>();
		}

		private async void ConnectionManagerConnectionStatusChanged(ConnectionStatus status)
		{
			switch (status)
			{
				case ConnectionStatus.Online:
					waitingForConnection?.SetResult(0);
					break;

				case ConnectionStatus.Offline:
					Message = "App is offline and will be closed in 3 sec...";
					await Task.Delay(3000);
					await NavigationService.Close(this);
					break;
			}
		}

		public async void Init()
		{
			#region Update xml database

			if (connectionManager.ConnectionStatus == ConnectionStatus.Offline)
			{
				await waitingForConnection.Task;
			}

			waitingForConnection = null;

			await xmlDb.InitAsync(this, true);

			#endregion

			await NavigationService.Close(this);
		}
	}
}
