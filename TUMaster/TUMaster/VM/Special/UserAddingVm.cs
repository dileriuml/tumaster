﻿using System.Threading.Tasks;
using MvvmCross.ViewModels;
using TUMaster.Core.Data;
using TUMaster.Core.Services;
using TUMaster.VM.Commands;

namespace TUMaster.VM.Special
{
	public class UserAddingVm : VmBase, IMvxViewModelResult<User>
	{
		private const string LoggingInMessage = "Logging in";
		private const string WrongCredentialsMessage = "Something is invalid...";
		private const string BannedMessage = "This user is banned";
		private const string UsersExistsMessage = "This user is already logged in";
		private readonly ProfileService profileService;
		private bool isLoggingIn;
		private UsersCache usersCache;
		
		public UserAddingVm(ProfileService profileService, UsersCache usersCache)
		{
			this.usersCache = usersCache;
			this.profileService = profileService;
			LoginCommand = new AsyncCommand(DoLogin);
			ResultUser = new User();
		}

		public TaskCompletionSource<object> CloseCompletionSource { get; set; }

		public User ResultUser { get; }

		public string Message { get; private set; }

		public bool IsLoggingIn
		{
			get => isLoggingIn;
			set => SetProperty(ref isLoggingIn, value);
		}

		public string UserId
		{
			get => ResultUser.UserId;
			set
			{
				ResultUser.UserId = value;
				RaisePropertyChanged();
			}
		}

		public string Password
		{
			get => ResultUser.Password;

			set
			{
				ResultUser.Password = value;
				RaisePropertyChanged();
			}
		}

		public string Syncode
		{
			get => ResultUser.Syncode;

			set
			{
				ResultUser.Syncode = value;
				RaisePropertyChanged();
			}
		}

		public AsyncCommand LoginCommand { get; }

		private async Task DoLogin()
		{
			IsLoggingIn = true;
			Message = LoggingInMessage;
			var loginResult = await profileService.TryLogin(ResultUser);

			switch (loginResult)
			{
				case AccountResponse.Success:
					if (usersCache.ContainsUser(UserId))
					{
						Message = UsersExistsMessage;
					}
					else
					{
						await NavigationService.Close(this, ResultUser);
					}

					break;

				case AccountResponse.Banned:
					Message = BannedMessage;
					break;
				case AccountResponse.Failed:
					Message = WrongCredentialsMessage;
					break;
			}

			RaisePropertyChanged(nameof(Message));
			IsLoggingIn = false;
		}

		protected override void OnClose() => CloseCompletionSource?.TrySetResult(null);
	}
}
