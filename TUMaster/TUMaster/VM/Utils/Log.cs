﻿using System;
using System.IO;

namespace TUMaster.Utils
{
	public static class Log
	{
		public static void Message(string message)
		{
			#if DEBUG
				using (var file = new FileStream("Log.txt", FileMode.Append))
				{
					using (var writer = new StreamWriter(file))
					{
						writer.WriteLine(string.Format("Time: {0} Message:\n{1}", DateTime.Now, message));
					}
				}
			#endif
		}
	}
}
